//
//  WelcomePageViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 29/12/20.
//
import UIKit

protocol ControllerIndexDelegate {
    func getControllerIndex(index: Int)
}

class WelcomePageViewController: UIPageViewController {
    
    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList = [UIViewController]()
    var sb = UIStoryboard(name:"Main",bundle:nil)
    static var index_delegate: ControllerIndexDelegate? = nil
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.modalPresentationStyle = .custom
        
        self.delegate = self
        WelcomePageViewController.customDataSource = self
        viewControllerList = [sb.instantiateViewController(withIdentifier: "WelcomeViewController" ),
                    sb.instantiateViewController(withIdentifier: "FoodItemViewController" ),
                    sb.instantiateViewController(withIdentifier: "FoodItemTwoViewController" ),
                    sb.instantiateViewController(withIdentifier: "FoodItemThreeViewController" ),
                    sb.instantiateViewController(withIdentifier: "FoodItemFourViewController" )]
            if let firstViewController = viewControllerList.first {
                self.setViewControllers([firstViewController],direction: .forward,
                                        animated:false,completion:nil)
            }
            for view in self.view.subviews {
                if let subView = view as? UIScrollView {
                    subView.isScrollEnabled = false
                }
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    func setFirstController(direction:Int) {
        setViewControllers([viewControllerList[0]], direction: direction == 2 ? .reverse:.forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 0
        WelcomePageViewController.index_delegate?.getControllerIndex(index: 0)
    }
    
    func setSecondController(direction:Int) {
        setViewControllers([viewControllerList[1]], direction: direction == 2 ? .reverse:.forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 1
        currentMenuItem = 1
        WelcomePageViewController.index_delegate?.getControllerIndex(index: 1)
    }
    
    func setThirdController(direction:Int) {
        setViewControllers([viewControllerList[2]], direction: direction == 2 ? .reverse:.forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 2
        currentMenuItem = 2
        WelcomePageViewController.index_delegate?.getControllerIndex(index: 2)
    }
    
    func setFOurthController(direction:Int) {
        setViewControllers([viewControllerList[3]], direction: direction == 2 ? .reverse:.forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 3
        currentMenuItem = 3
        WelcomePageViewController.index_delegate?.getControllerIndex(index: 3)
    }
    
    func setFifthController(direction:Int) {
        setViewControllers([viewControllerList[4]], direction: direction == 2 ? .reverse:.forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 4
        currentMenuItem = 4
        WelcomePageViewController.index_delegate?.getControllerIndex(index: 4)
    }
    
 
    //Assigning controller manually
    func nextController(index: Int, direction: UIPageViewController.NavigationDirection) {
        setViewControllers([viewControllerList[index]], direction: direction, animated: true, completion: nil)
        WelcomePageViewController.index_delegate?.getControllerIndex(index: index)
    }
    
    //Getting current controller index
    func currentControllerIndex(VC: UIViewController) {
        if let viewControllerIndex = viewControllerList.index(of: VC) {
            WelcomePageViewController.index_delegate?.getControllerIndex(index: viewControllerIndex)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension WelcomePageViewController : UIPageViewControllerDataSource {

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        currentControllerIndex(VC: viewController)
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        WelcomePageViewController.index_delegate?.getControllerIndex(index: nextIndex)
        return viewControllerList[nextIndex]
    }
}

extension WelcomePageViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
        if let page = self.pageControl?.currentPage{
            WelcomePageViewController.index_delegate?.getControllerIndex(index: page)
        }
    }
}

