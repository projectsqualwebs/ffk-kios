//
//  Objects.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 18/05/21.
//

import Foundation
import Realm
import RealmSwift

//class GetCartObject: Object,Decodable {
//    @objc dynamic var id: Int = 0
//    @objc dynamic var cart_item_id: Int = 0
//    @objc dynamic var cart_list_id: Int = 0
//    @objc dynamic var item_id: Int = 0
//    @objc dynamic var item_count: Int = 0
//    @objc dynamic var menu_id: Int = 0
//    @objc dynamic var menu_price: Int = 0
//    @objc dynamic var modifier_id: Int = 0
//    @objc dynamic var modifier_item_id: Int = 0
//    @objc dynamic var modifier_item_count: Int = 0
//    @objc dynamic var modifier_item_price: Int = 0
//    @objc dynamic var item_name: String = ""
//    @objc dynamic var item_price: Double = 0
//    @objc dynamic var item_image: String = ""
//    @objc dynamic var tax_rate: Double = 0
//    @objc dynamic var is_common:Int = 0
//    @objc dynamic var item_description: String = ""
//    var details = List<SubCategoryObject>()
//
//    override class func primaryKey() -> String? {
//            return "id"
//    }
//
//    private enum CodingKeys: String, CodingKey {
//          case id
//          case cart_item_id
//          case cart_list_id
//          case item_id
//          case item_count
//          case menu_id
//          case menu_price
//          case modifier_id
//          case modifier_item_id
//          case modifier_item_count
//          case modifier_item_price
//          case item_name
//          case item_price
//          case item_image
//          case tax_rate
//          case is_common
//           case item_description
//          // Set JSON Object Key
//          case details = "details"
//
//      }
//
//    public required convenience init(from decoder: Decoder) throws {
//        self.init()
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        self.id = try container.decode(Int.self, forKey: .id)
//        self.cart_item_id = try container.decode(Int.self, forKey: .cart_item_id)
//        self.cart_list_id = try container.decode(Int.self, forKey: .cart_list_id)
//        self.item_count = try container.decode(Int.self, forKey: .item_count)
//        self.menu_id = try container.decode(Int.self, forKey: .menu_id)
//        self.menu_price = try container.decode(Int.self, forKey: .menu_price)
//        self.modifier_id = try container.decode(Int.self, forKey: .modifier_id)
//        self.modifier_item_id = try container.decode(Int.self, forKey: .modifier_item_id)
//        self.modifier_item_count = try container.decode(Int.self, forKey: .modifier_item_count)
//        self.modifier_item_price = try container.decode(Int.self, forKey: .modifier_item_price)
//        self.item_name = try container.decode(String.self, forKey: .item_name)
//        self.item_price = try container.decode(Double.self, forKey: .item_price)
//        self.item_image = try container.decode(String.self, forKey: .item_image)
//        self.tax_rate = try container.decode(Double.self, forKey: .tax_rate)
//        self.is_common = try container.decode(Int.self, forKey: .is_common)
//        self.item_description = try container.decode(String.self, forKey: .item_description)
//        let subcategory = try container.decodeIfPresent([SubCategoryObject].self, forKey: .details) ?? [SubCategoryObject()]
//        details.append(objectsIn: subcategory)
//    }
//
//}

func incrementID(val:Int) -> Int {
    return Int(Date().timeIntervalSince1970)
}

class SubCategoryObject: Object,Decodable {
    @objc dynamic var random_id: Int = 0
    @objc dynamic var id:Int = 0
    @objc dynamic var cart_item_id:Int = 0
    @objc dynamic var item_id: Int = 0
    @objc dynamic var side_menu_id:Int = 0
    @objc dynamic var modifier_group_id: Int = 0
    @objc dynamic var category_id:Int = 0
    @objc dynamic var item_name:String = ""
    @objc dynamic var item_price:Double = 0
    @objc dynamic var item_image:String = ""
    @objc dynamic var thumbnail:String = ""
    @objc dynamic var tax_rate:Double = 0
    @objc dynamic var item_description:String = ""
    @objc dynamic var ipad_image: String = ""
    @objc dynamic var ipad_image_single: String = ""
    @objc dynamic var its_own:Int = 0
    @objc dynamic var isSelected:Int = 0
    @objc dynamic var itemCount:Int = 0
    @objc dynamic var count: Int = 0
    @objc dynamic var is_checked: Int = 0
    @objc dynamic var item_count: Int = 0
    
    override class func primaryKey() -> String? {
            return "random_id"
    }
    
    private enum CodingKeys: String, CodingKey {
        case random_id
          case id
          case cart_item_id
          case item_id
          case side_menu_id
          case modifier_group_id
          case category_id
          case item_name
          case item_price
          case item_image
          case thumbnail
          case tax_rate
          case item_description
          case ipad_image
          case ipad_image_single
          case its_own
          case isSelected
          case item_count
          case itemCount
      }
     
    public required convenience init(from decoder: Decoder) throws {
            self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.random_id = try container.decode(Int.self, forKey: .random_id)
        self.id = try container.decode(Int.self, forKey: .id)
        self.cart_item_id = try container.decode(Int.self, forKey: .cart_item_id)
        self.item_id = try container.decode(Int.self, forKey: .item_id)
        self.side_menu_id = try container.decode(Int.self, forKey: .side_menu_id)
        self.modifier_group_id = try container.decode(Int.self, forKey: .modifier_group_id)
        self.category_id = try container.decode(Int.self, forKey: .category_id)
        self.item_name = try container.decode(String.self, forKey: .item_name)
        self.item_price = try container.decode(Double.self, forKey: .item_price)
        self.item_image = try container.decode(String.self, forKey: .item_image)
        self.thumbnail = try container.decode(String.self, forKey: .thumbnail)
        self.tax_rate = try container.decode(Double.self, forKey: .tax_rate)
        self.item_description = try container.decode(String.self, forKey: .item_description)
        self.ipad_image = try container.decode(String.self, forKey: .ipad_image)
        self.ipad_image_single = try container.decode(String.self, forKey: .ipad_image_single)
        self.its_own = try container.decode(Int.self, forKey: .its_own)
        self.isSelected = try container.decode(Int.self, forKey: .isSelected)
        self.itemCount = try container.decode(Int.self, forKey: .itemCount)
        self.item_count =   try container.decode(Int.self, forKey: .item_count)
        
    }
}



class MenuList: Object, Decodable {
    @objc dynamic var random_id: Int = 0
    @objc dynamic var id:Int = 0
    @objc dynamic var item_id:Int = 0
    @objc dynamic var category_id:Int = 0
    @objc dynamic var item_name:String = ""
    @objc dynamic var item_price:Double = 0
    @objc dynamic var item_image:String = ""
    @objc dynamic var ipad_image: String = ""
    @objc dynamic var ipad_image_single: String = ""
    @objc dynamic var modifiers: Int = 0
    @objc dynamic var thumbnail:String = ""
    @objc dynamic var is_common: Int = 0
    @objc dynamic var tax_rate:Double = 0
    @objc dynamic var item_description:String = ""
    @objc dynamic var reward_item: Int = 0
    @objc dynamic var item_count: Int = 0
    var selectedSubmenu = List<SubMenuResponsObject>()
    
    
    override class func primaryKey() -> String? {
            return "random_id"
    }
    
    private enum CodingKeys: String, CodingKey {
          case random_id
          case id
          case item_id
          case category_id
          case item_name
          case item_price
          case item_image
          case ipad_image
          case ipad_image_single
          case modifiers
          case thumbnail
          case is_common
          case tax_rate
          case item_description
          case reward_item
        case item_count
          case selectedSubmenu = "selectedSubmenu"

      }
     
    public required convenience init(from decoder: Decoder) throws {
            self.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
        self.random_id = try container.decode(Int.self, forKey: .random_id)
        self.id = try container.decode(Int.self, forKey: .id)
        self.item_id = try container.decode(Int.self, forKey: .item_id)
        self.category_id = try container.decode(Int.self, forKey: .category_id)
        self.item_name = try container.decode(String.self, forKey: .item_name)
        self.item_price = try container.decode(Double.self, forKey: .item_price)
        self.item_image = try container.decode(String.self, forKey: .item_image)
        self.ipad_image = try container.decode(String.self, forKey: .ipad_image)
        self.ipad_image_single = try container.decode(String.self, forKey: .ipad_image_single)
        self.modifiers = try container.decode(Int.self, forKey: .modifiers)
        self.thumbnail = try container.decode(String.self, forKey: .thumbnail)
        self.is_common = try container.decode(Int.self, forKey: .is_common)
        self.tax_rate = try container.decode(Double.self, forKey: .tax_rate)
        self.item_description = try container.decode(String.self, forKey: .item_description)
        self.reward_item = try container.decode(Int.self, forKey: .reward_item)
        self.item_count = try container.decode(Int.self, forKey: .item_count)
        let submenu = try container.decodeIfPresent([SubMenuResponsObject].self, forKey: .selectedSubmenu) ?? [SubMenuResponsObject()]
        selectedSubmenu.append(objectsIn: submenu)
    }
}

class SubMenuResponsObject: Object, Decodable {
    @objc dynamic var random_id: Int = 0
    @objc dynamic var id:Int = 0
    @objc dynamic var modifier_group_id:Int = 0
    @objc dynamic var item_id:Int = 0
    @objc dynamic var added_from: Int = 0
    @objc dynamic var item_exactly:Int = 0
    @objc dynamic var item_range_from: Int = 0
    @objc dynamic var item_count: Int = 0
    @objc dynamic var item_range_to: Int = 0
    @objc dynamic var restaurant_id: Int = 0
    @objc dynamic var item_maximum: Int = 0
    @objc dynamic var single_item_maximum: Int = 0
    @objc dynamic var modifier_group_name:String = ""
    @objc dynamic var is_rule:Int = 0
    @objc dynamic var sectionItemCount: Int = 0
    var meals = List<SubCategoryObject>()
    
    
    override class func primaryKey() -> String? {
            return "random_id"
    }
    
    private enum CodingKeys: String, CodingKey {
          case random_id
          case id
          case modifier_group_id
          case added_from
          case item_exactly
          case item_id
          case item_range_from
          case item_count
          case item_range_to
          case restaurant_id
          case item_maximum
          case single_item_maximum
          case modifier_group_name
          case is_rule
          case sectionItemCount
          case meals = "meals"

      }
     
    public required convenience init(from decoder: Decoder) throws {
            self.init()
            let container = try decoder.container(keyedBy: CodingKeys.self)
        self.random_id = try container.decode(Int.self, forKey: .random_id)
        self.id = try container.decode(Int.self, forKey: .id)
        self.modifier_group_id = try container.decode(Int.self, forKey: .modifier_group_id)
        self.added_from = try container.decode(Int.self, forKey: .added_from)
        self.item_exactly = try container.decode(Int.self, forKey: .item_exactly)
        self.item_id = try container.decode(Int.self, forKey: .item_id)
        self.item_range_from = try container.decode(Int.self, forKey: .item_range_from)
        self.item_count = try container.decode(Int.self, forKey: .item_count)
        self.item_range_to = try container.decode(Int.self, forKey: .item_range_to)
        self.restaurant_id = try container.decode(Int.self, forKey: .restaurant_id)
        self.item_maximum = try container.decode(Int.self, forKey: .item_maximum)
        self.single_item_maximum = try container.decode(Int.self, forKey: .single_item_maximum)
        self.modifier_group_name = try container.decode(String.self, forKey: .modifier_group_name)
        self.is_rule = try container.decode(Int.self, forKey: .is_rule)
        self.sectionItemCount = try container.decode(Int.self, forKey: .sectionItemCount)
        let meal = try container.decodeIfPresent([SubCategoryObject].self, forKey: .meals) ?? [SubCategoryObject()]
        meals.append(objectsIn: meal)
    }
    
}
