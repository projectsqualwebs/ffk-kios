//
//  DBHelper.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 18/05/21.
//

import Foundation
import RealmSwift
import Realm
import UIKit

class DBHelper {
    static var realm: Realm = try! Realm()
    static var shared = DBHelper()
    
     func getCartItem<T:Object>()->[T] {
        let realmResults = DBHelper.realm.objects(T.self)
         return Array(realmResults)

     }
    
    public func addItemToCart(mainMenuId: Int?,menuList: GetMenusList, subcategory:[SubMenuResponse])
    {
        var previousData:[MenuList] = self.getCartItem()
        for i in 0..<previousData.count {
            if(previousData[i].item_id == menuList.item_id){
                try! DBHelper.realm.write {
                    previousData[i].item_count += 1
                    DBHelper.realm.add(previousData,update: .all)
                }
                return
            }
        }
        var data =  MenuList()
        data.random_id = Int(Date().timeIntervalSince1970)
        data.id = mainMenuId ?? 0
        data.item_count = menuList.item_count ?? 1
        data.item_id = menuList.item_id ?? 0
        data.category_id = menuList.category_id ?? 0
        data.item_name = menuList.item_name ?? ""
        data.item_price = menuList.item_price ?? 0
        data.item_image = menuList.item_image ?? ""
        data.ipad_image = menuList.ipad_image ?? ""
        data.ipad_image_single = menuList.ipad_image_single ?? ""
        data.modifiers = menuList.modifiers ?? 0
        data.thumbnail = menuList.thumbnail ?? ""
        data.is_common = menuList.is_common ?? 0
        data.tax_rate = menuList.tax_rate ?? 0
        data.item_description = menuList.item_description ?? ""
        data.reward_item = menuList.reward_item ?? 0
        data.selectedSubmenu = getSubmenu(data: subcategory)
        try! DBHelper.realm.write {
            DBHelper.realm.add(data)
        }
    }
    
    func updateItem(data:[MenuList]){
            try! DBHelper.realm.write {
                DBHelper.realm.add(data, update: .all)
            }
    }
    
    func updateItemCount(randomId:Int, increment: Int){
        var previousData:[MenuList] = self.getCartItem()
        for i in 0..<previousData.count {
            if(previousData[i].random_id == randomId){
                try! DBHelper.realm.write {
                    previousData[i].item_count = increment
                    DBHelper.realm.add(previousData,update: .all)
                }
                return
            }
        }
    }
    
    
    
    func handleCartPrice()-> Double{
        var previousData:[MenuList] = self.getCartItem()
        var count = 0
        var amount = Double()
        for val in previousData{
            count += val.item_count
            amount = amount + (val.item_price*Double(val.item_count)) + (val.tax_rate*Double(val.item_count))
            for meal in val.selectedSubmenu{
                for i in meal.meals{
                    if(i.item_name.lowercased() == "smothered" || i.item_name.lowercased() == "baked"){
                        if(i.isSelected == 1 || i.is_checked == 1){
                            amount += (1*Double(val.item_count))
                        }
                      
                    }
                }
            }
        }
        Singleton.shared.cartItemCount = count
        NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        return Double(round(100*amount)/100)
    }
    
    func getSubmenu(data:[SubMenuResponse]) -> List<SubMenuResponsObject>{
        var submenu = List<SubMenuResponsObject>()
        for val in data {
            let menu = SubMenuResponsObject()
            menu.random_id = Int.random(in: 1..<99999999)
            menu.id = val.id ?? 0
            menu.modifier_group_id = val.modifier_group_id ?? 0
            menu.added_from = val.added_from ?? 0
            menu.item_exactly = val.item_exactly ?? 0
            menu.item_id = val.item_id ?? 0
            menu.item_range_from = val.item_range_from ?? 0
            menu.item_count = val.item_count ?? 0
            menu.item_range_to = val.item_range_to ?? 0
            menu.restaurant_id = val.restaurant_id ?? 0
            menu.item_maximum = val.item_maximum ?? 0
            menu.single_item_maximum = val.single_item_maximum ?? 0
            menu.modifier_group_name = val.modifier_group_name ?? ""
            menu.is_rule = val.is_rule ?? 0
            menu.sectionItemCount = val.sectionItemCount ?? 0
            menu.meals = self.getMealObject(data: val.meals)
            submenu.append(menu)
        }
        return submenu
    }
    
    func getMealObject(data:[SubmenuSubCategory]) -> List<SubCategoryObject> {
        var meals = List<SubCategoryObject>()
        for val in data {
            let meal = SubCategoryObject()
            meal.random_id = Int.random(in: 1..<99999999)
            meal.id = val.id ?? 0
            meal.cart_item_id = val.cart_item_id ?? 0
            meal.item_id = val.item_id ?? 0
            meal.side_menu_id = val.side_menu_id ?? 0
            meal.modifier_group_id = val.modifier_group_id ?? 0
            meal.category_id = val.category_id  ?? 0
            meal.item_name = val.item_name ?? ""
            meal.item_price = val.item_price ?? 0
            meal.item_image = val.item_image ?? ""
            meal.thumbnail = val.thumbnail ?? ""
            meal.tax_rate = val.tax_rate ?? 0
            meal.item_description = val.item_description ?? ""
            meal.ipad_image = val.ipad_image ?? ""
            meal.ipad_image_single = val.ipad_image_single ?? ""
            meal.its_own = val.its_own ?? 0
            meal.isSelected = val.isSelected ?? 0
            meal.itemCount = val.itemCount ?? 0
            meal.item_count =  val.item_count ?? 0
            meals.append(meal)
        }
        
        return meals
    }
    
    func handleEditCart(randomId:Int, increment: Int){
        
    }
    
    func emptyLocalStorage(){
        Singleton.shared.cartItemCount = 0
        try! DBHelper.realm.write {
            DBHelper.realm.deleteAll()
        }
    }
}

