//
//  ViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 23/12/20.
//

import UIKit
import RMPZoomTransitionAnimator_Swift
import Spring
import BubbleTransition
import RevealingSplashView
import AVFoundation
import Reachability
import SquareReaderSDK
import Alamofire
import FirebaseRemoteConfig


class ViewController: UIViewController,UIViewControllerTransitioningDelegate,UINavigationControllerDelegate, NavigateToOrder, ReachabilityObserverDelegate , HandleCart, StartFresh,ControllerIndexDelegate, ShowHideCategory {
    func showHideCategory(show: Bool) {
        self.categoryView.isHidden = show
        
        if(MENU_TYPE == K_LUNCH_ID){
            for val in 0..<Singleton.shared.lunchMenu.count{
                switch val {
                case 0:
                    self.firstLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 1:
                    self.secondLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 2:
                    self.thirdLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 3:
                    self.fourthLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                default:
                    break
                }
            }
        }else if(MENU_TYPE == K_BREAK_ID){
            for val in 0..<Singleton.shared.breakfastMenu.count{
                switch val {
                case 0:
                    self.firstLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    
                    break
                case 1:
                    self.secondLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                case 2:
                    self.thirdLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                case 3:
                    self.fourthLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                default:
                    break
                }
            }
        }
        self.showHideCategory()
        
    }
    
    func getControllerIndex(index: Int) {
        self.currentIndex = index
    }

    func startFreshOrder(playSound: Bool) {
        self.animateImageView()
        self.view.addSubview(self.revealingSplashView!)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(800)) {
            self.revealingSplashView?.heartAttack = true
            self.revealingSplashView?.startAnimation()
        }
        if(playSound){
            if self.audioPlayer.isPlaying {
                self.audioPlayer.pause()
            }
            self.audioPlayer.currentTime = 0
            self.audioPlayer.play()
        }
    }
    
    func handleCartItemCount() {
        self.myCartView.isHidden = false
        self.countView.isHidden = false
        self.rightView.isHidden = false
        self.leftView.isHidden = false
        self.exitButton.isHidden = false
        //   UserDefaults.standard.setValue(Singleton.shared.cartItemCount, forKey: UD_BAG_COUNT)
        if self.audioPlayer.isPlaying {
            self.audioPlayer.pause()
        }
        self.audioPlayer.currentTime = 0
        self.audioPlayer.play()
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve) {
            self.messageView.isHidden = false
            if(self.view.frame.width < 1550){
                self.messageView.frame.origin.x -= 300
                self.messageView.frame.size.width += 300
            }else {
                self.messageView.frame.origin.x -= 300
                self.messageView.frame.size.width += 300
            }
        } completion: { (val) in
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2) + .milliseconds(100)) {
                UIView.animate(withDuration: 0.2, delay: 0, options: .curveLinear) {
                    self.messageView.frame.origin.x += self.view.frame.width < 1550 ? 300:300
                    self.messageView.frame.size.width = 0
                    self.countMessageView.constant = 0
                    self.messageView.alpha = 0.5
                    //  self.myCartView.alpha =  0.8
                } completion: { (val) in
                    self.messageView.isHidden = true
                    self.countMessageView.constant = self.view.frame.width < 1550 ? 300:300
                    self.messageView.alpha = 1
                    //   self.myCartView.alpha = 1
                }
            }
        }
        self.cartCount.text = "\(Singleton.shared.cartItemCount)"
        
        NavigationController.shared.getMyCart { (response) in
            self.updateCount()
        }
    }
    
    
    func reachabilityChanged(_ isReachable: Bool) {
       // print("called")
    }
    
    
    func pushToOrder() {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        self.navigationController?.pushViewController(myVC, animated:true)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.duration = 0.3
        transition.startingPoint =  myCartView.center
        
        transition.bubbleColor = .white//myCartView.backgroundColor!
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.duration = 0.35
        transition.startingPoint = CGPoint(x: myCartView.center.x+30, y: myCartView.center.y - 35)
        transition.bubbleColor = .white
        //myCartView.backgroundColor!
        return transition
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveTransition
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var containerView: WelcomeView!
    @IBOutlet weak var breakfastArrow: UIImageView!
    @IBOutlet weak var cokeArrow: UIImageView!
    @IBOutlet weak var dessertArrow: UIImageView!
    @IBOutlet weak var foodPlateArrow: UIImageView!
    
    @IBOutlet weak var breakfastView: SpringView!
    @IBOutlet weak var plateView: SpringView!
    @IBOutlet weak var dessertView: SpringView!
    @IBOutlet weak var beverageView: SpringView!
    
    @IBOutlet weak var breakfastButton: RippleButton!
    @IBOutlet weak var foodPlateButton: RippleButton!
    @IBOutlet weak var dessertButton: RippleButton!
    @IBOutlet weak var cokeButton: RippleButton!
    
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var thirdImage: UIImageView!
    @IBOutlet weak var thirdLabel: UILabel!
    @IBOutlet weak var fourthImage: UIImageView!
    @IBOutlet weak var fourthLabel: UILabel!
    
    @IBOutlet weak var rightView: View!
    @IBOutlet weak var leftView: View!
    @IBOutlet weak var leftSpringView: SpringView!
    @IBOutlet weak var rightSpringView: SpringView!
    
    
    @IBOutlet weak var myCartView: View!
    @IBOutlet weak var cartCount: UILabel!
    @IBOutlet weak var countView: View!
    @IBOutlet weak var countMessageView: NSLayoutConstraint!
    @IBOutlet weak var messageView: View!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var startnewOrderView: View!
    @IBOutlet weak var categoryView: UIView!
    @IBOutlet weak var categoryStackView: UIStackView!
    @IBOutlet weak var rightButton: DampingButton!
    @IBOutlet weak var leftButton: DampingButton!
    
    
    var currentIndex = Int()
    let transition = BubbleTransition()
    let interactiveTransition = BubbleInteractiveTransition()
    
    var revealingSplashView : RevealingSplashView?
    var timer: Timer?
    var audioPlayer = AVAudioPlayer()
    let reachability = try! Reachability()
    var remoteConfig: RemoteConfig!

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleBaseUrl()
        self.leftAction(self.leftButton)
        self.rightAction(self.rightButton)
        EmailSubscribeViewController.startFreshDelegate = self
        
        // FoodItemViewController.swipeDelegate = self
        self.animateImageView()
        self.apiCalling()
        let sound = Bundle.main.path(forResource: "notifications_20", ofType: "mp3")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!), fileTypeHint: "mp3")
        }catch{
            print(error)
        }
        self.definesPresentationContext = true
        self.leftSpringView.layer.cornerRadius = 40
        self.rightSpringView.layer.cornerRadius = 40
        self.handleNotificationObserver()
        RealtimeFirebase.shared.addMenuChangeObserver { val in
            K_IS_MENU_ITEM_CHANGED = true
        }
        RealtimeFirebase.shared.checkRestaurantStatus()
        ItemDetailViewController.cartDelegate = self
        WelcomeViewController.categoryViewDelegate = self
        // self.handleNetworkRechability()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        try? addReachabilityObserver()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.updateCount()
        
        self.requestMicrophonePermission()
        
        //  self.handleUserInactivity()
        
        NavigationController.shared.getMyCart { (response) in
            self.updateCount()
        }
    }
    
    func handleBaseUrl(){
        remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        remoteConfig.setDefaults(fromPlist: "firebaseConfigInfo")
        
        var expirationDuration = 3600
        if remoteConfig.configSettings.minimumFetchInterval == 0 {
            expirationDuration = 0
        }
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                self.remoteConfig.activate { changed, error in
                }
            } else {
            }
//            if let url = self.remoteConfig["kiosk_base_url_ios"].stringValue as? String {
//                U_BASE = url
//            }
            
            if let image_url = self.remoteConfig["kiosk_image_base_url"].stringValue as? String {
                U_IMAGE_BASE = image_url
            }
            
            if let square_image_url = self.remoteConfig["kiosk_image_base_url2"].stringValue as? String {
                U_IMAGE_BASE_2 = square_image_url
            }
            
        }
    }
    
    func handleNotificationObserver(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_REMOVE_ARROW), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_START_FRESH_ORDER), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_MY_CART_EMPTY_DATA), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeArrow(_:)), name: NSNotification.Name(N_REMOVE_ARROW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageKithchenOffline(_:)), name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateCount), name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.functionCalled(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.startNewOrder), name: NSNotification.Name(N_START_FRESH_ORDER), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleMyCartEmpty)
                                               , name: NSNotification.Name(N_MY_CART_EMPTY_DATA), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showHideCategoryView(_:)), name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetViewOnMenuChange(_:)), name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetTimer(_:)), name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    func handleNetworkRechability(){
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func resetViewOnMenuChange(_ notif: Notification){
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
        self.backction(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1) + .milliseconds(500)) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ResetViewController") as! ResetViewController
            myVC.navigationDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: false, completion: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetViewOnMenuChange(_:)), name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
    }
    
    @objc func showHideCategoryView(_ notif: Notification){
        if((notif.userInfo?["show"] as? Bool) == true){
            self.categoryStackView.animShow()
        }else {
            self.categoryStackView.animHide()
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        if(reachability.isReachable){
            //            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NoNetworkViewController
            //            myVC.modalPresentationStyle = .overFullScreen
            //            self.present(myVC, animated: false, completion: nil)
        }else {
            
        }
    }
    
    
    func animateImageView(){
        let image = UIImageView()
        image.image = UIImage(named: "welcome")
        self.addOverlay(on: image)
        revealingSplashView =  RevealingSplashView(iconImage: UIImage(named: "apple-icon")!, iconInitialSize: CGSize(width: 800, height: 400), backgroundImage: image.image!)
    }
    
    func addOverlay(on view: UIView) {
        let overlay: UIView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        overlay.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        view.addSubview(overlay)
    }
    
    @objc func handleMyCartEmpty(){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.heading = "No items in cart, explore menu"
        self.present(myVC, animated: false, completion: nil)
    }
    
    @objc func startNewOrder(){
        //  NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_START_FRESH_ORDER), object: nil)
        self.animateImageView()
        self.view.addSubview(self.revealingSplashView!)
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600)) {
            self.revealingSplashView?.heartAttack = true
            self.revealingSplashView?.startAnimation()
        }
        
        if self.audioPlayer.isPlaying {
            self.audioPlayer.pause()
        }
        self.audioPlayer.currentTime = 0
        self.audioPlayer.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.startNewOrder), name: NSNotification.Name(N_START_FRESH_ORDER), object: nil)
    }
    
    
    func resizeImage(){
        guard let currentCGImage = self.fourthImage.image?.cgImage else { return }
        let currentCIImage = CIImage(cgImage: currentCGImage)
        
        let filter = CIFilter(name: "CIPixellate")
        filter?.setValue(currentCIImage, forKey: kCIInputImageKey)
        filter?.setValue(12, forKey: kCIInputScaleKey)
        guard let outputImage = filter?.outputImage else { return }
        
        let context = CIContext()
        
        if let cgimg = context.createCGImage(outputImage, from: outputImage.extent) {
            let processedImage = UIImage(cgImage: cgimg)
            
            self.fourthImage.image = processedImage
            //  print(processedImage.size)
        }
    }
    
    @objc func functionCalled(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
        if let val = notif.userInfo?["msg"] as? String{
            if let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as? NotAvailableViewController{
                myVC.heading = val
                myVC.modalPresentationStyle = .overFullScreen
                self.present(myVC, animated: false, completion: nil)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.functionCalled(_:)), name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil)
    }
    
    
    @objc func updateCount(){
        if(Singleton.shared.cartItemCount == 0 || currentIndex == 0){
            self.myCartView.isHidden = true
            self.countView.isHidden = true
            self.messageView.isHidden = true
            timer?.invalidate()
        }else {
            self.myCartView.isHidden = false
            self.countView.isHidden = false
            self.cartCount.text = "\(Singleton.shared.cartItemCount ?? 0)"
        }
        // if(Singleton.shared.cartItemCount >= 1){
        self.handleUserInactivity()
        //}
    }
    
    func handleUserInactivity(){
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_INTERVAL, target: self, selector: #selector(self.hideControls), userInfo: nil, repeats: true)
    }
    
    @objc func resetTimer(_ notif: Notification) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_INTERVAL, target: self, selector: #selector(self.hideControls), userInfo: nil, repeats: true)
    }
    
    @objc func hideControls() {
        let topContrller = UIApplication.topViewController()
        if(MENU_TYPE == 0){
            timer?.invalidate()
            self.currentIndex = 0
            return
        }
        if(isMyCartOpen){
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil, userInfo: ["close":false])
            return
        }
        if (topContrller is ViewController && (Singleton.shared.cartItemCount != 0)) {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeoutViewController") as! TimeoutViewController
            myVC.navigationDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }else if (topContrller is ViewController && (Singleton.shared.cartItemCount == 0) && self.currentIndex != 0) {
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeoutViewController") as! TimeoutViewController
            myVC.navigationDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }else if (topContrller is ViewController && (Singleton.shared.cartItemCount == 0)) {
            print("worked")
        }else {
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "TimeoutViewController") as! TimeoutViewController
            myVC.navigationDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }
    }
    
    @objc func removeArrow(_ notif: NSNotification){
        if let direction = notif.userInfo?["direction"] as? String{
            if(direction == "left"){
                self.leftOperation()
            }else if(direction == "right"){
                self.rightOperation()
            }
            return
        }
        if((notif.userInfo?["showIndicator"] as? Bool) == true){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(315)) {
                self.rightView.isHidden = false
                self.leftView.isHidden = false
                self.exitButton.isHidden = false
            }
            return
        }else if((notif.userInfo?["showIndicator"] as? Bool) == false){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(400)) {
                self.rightView.isHidden = true
                self.leftView.isHidden = true
                self.exitButton.isHidden = true
                if((notif.userInfo?["isExit"] as? Bool) == true){
                    
                    //   self.navigationController?.dismiss(animated: false, completion: nil)
                    self.exitButton.isHidden = true
                    self.breakfastArrow.isHidden = true
                    self.cokeArrow.isHidden = true
                    self.foodPlateArrow.isHidden = true
                    self.dessertArrow.isHidden = true
                    self.myCartView.isHidden = true
                    self.countView.isHidden = true
                    self.messageView.isHidden = true
                }
            }
            return
        }
        
        if((notif.userInfo?["playSound"] as? Bool) == true){
            self.handleCartItemCount()
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_REMOVE_ARROW), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeArrow(_:)), name: NSNotification.Name(N_REMOVE_ARROW), object: nil)
    }
    
    func animateView(image: UIImageView){
        self.breakfastArrow.isHidden = true
        self.cokeArrow.isHidden = true
        self.foodPlateArrow.isHidden = true
        self.dessertArrow.isHidden = true
        image.isHidden = false
    }
    
    func apiCalling(){
        // ActivityIndicator.show(view: self.view)
        if(Singleton.shared.mainMenuData.count  == 0){
            revealingSplashView?.heartAttack = true
            self.view.addSubview(revealingSplashView!)
            NavigationController.shared.getPreferredRestaurant()
            NotificationCenter.default.addObserver(self, selector: #selector(self.handleMenu(_:)), name: NSNotification.Name(N_MENU_DATA), object: nil)
        }else {
            self.updateCount()
        }
    }
    
    @objc func manageKithchenOffline(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil)
        if let changeMenu = notif.userInfo?["changeMenu"] as? Bool {
            if(changeMenu == false){
                MENU_TYPE = 0
                Singleton.shared.breakfastMenu = []
                Singleton.shared.lunchMenu = []
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
            }else {
                if(MENU_TYPE != 0){
                 NavigationController.shared.getMenu()
                    K_IS_MENU_ITEM_CHANGED = false
                    return
                }
            }
        }
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        self.backction(self)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1) + .milliseconds(500)) {
            if(K_SHOW_OFFLINE_POPUP){
                K_SHOW_OFFLINE_POPUP = false
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ManagingOfflineViewController") as! ManagingOfflineViewController
            myVC.navigationDelegate = self
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: false, completion: nil)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.manageKithchenOffline(_:)), name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil)
    }
    
    @objc func handleMenu(_ notif: NSNotification){
        //Starts animation
        revealingSplashView?.startAnimation(){
            
        }
        
        if((notif.userInfo?["hide"] as? Bool) == true){
            self.dismiss(animated: false, completion: nil)
            return
        }
        self.dismiss(animated: false, completion: nil)
        self.authorizeReaderSDKIfNeeded()
        if(MENU_TYPE == K_LUNCH_ID){
            for val in 0..<Singleton.shared.lunchMenu.count{
                switch val {
                case 0:
                    self.firstLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 1:
                    self.secondLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 2:
                    self.thirdLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                case 3:
                    self.fourthLabel.text = Singleton.shared.lunchMenu[val].category_name
                    break
                default:
                    break
                }
            }
        }else if(MENU_TYPE == K_BREAK_ID){
            for val in 0..<Singleton.shared.breakfastMenu.count{
                switch val {
                case 0:
                    self.firstLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    
                    break
                case 1:
                    self.secondLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                case 2:
                    self.thirdLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                case 3:
                    self.fourthLabel.text = Singleton.shared.breakfastMenu[val].category_name
                    break
                default:
                    break
                }
            }
        }
        
        showHideCategory()
    }
    
   func showHideCategory() {
        if(MENU_TYPE == K_BREAK_ID || MENU_TYPE == K_LUNCH_ID){
            self.categoryView.isHidden = false
            self.categoryStackView.animShow()
        }else {
            self.categoryStackView.animHide()
            self.categoryView.isHidden = true
        }
    }
    
    func showAlert(){
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        if(Singleton.shared.mainMenuData.count > 0){
            myVC.heading = Singleton.shared.mainMenuData[0].menu_resume_time_message ?? ""
            self.navigationController?.present(myVC, animated: false, completion: nil)
        }
    }
    
    //MARK: IBActions
    @IBAction func showcategoryAction(_ sender: Any) {
        if(MENU_TYPE == K_BREAK_ID || MENU_TYPE == K_LUNCH_ID){
            self.categoryView.isHidden = false
            self.categoryStackView.animShow()
        }else {
            self.categoryStackView.animHide()
            self.categoryView.isHidden = true
        }
    }
    
    @IBAction func backction(_ sender: Any) {
        let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
        controller.setFirstController(direction:2)
        self.rightView.isHidden = true
        self.leftView.isHidden = true
        self.exitButton.isHidden = true
        self.currentIndex = 0
        self.breakfastArrow.isHidden = true
        self.cokeArrow.isHidden = true
        self.foodPlateArrow.isHidden = true
        self.dessertArrow.isHidden = true
        self.myCartView.isHidden = true
        self.countView.isHidden = true
        self.messageView.isHidden = true
    }
    
    func rightOperation(){
        switch self.currentIndex {
        case 1:
            self.foodPlateAction(self)
            break
        case 2:
            self.dessertAction(self)
            break
        case 3:
            self.cokeAction(self)
            break
        case 4:
            self.breakfastAction(self)
            break
        default:
            break
        }
    }
    
    func leftOperation(){
        switch self.currentIndex {
        case 1:
            self.cokeAction(self)
            break
        case 2:
            self.breakfastAction(self)
            break
        case 3:
            self.foodPlateAction(self)
            break
        case 4:
            self.dessertAction(self)
            break
        default:
            break
        }
        
    }
    
    @IBAction func rightAction(_ sender: DampingButton) {
        sender.tapDownHandler = {
            self.rightSpringView.animate(self.rightSpringView, transform: CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9))
        }
        
        sender.tapUpHandler = {
            self.rightSpringView.animate(self.rightSpringView, transform: .identity)
            
        }
        
        if(Singleton.shared.breakfastMenu.count != 0 || Singleton.shared.lunchMenu.count != 0){
            if(MENU_TYPE == K_BREAK_ID){
                switch currentMenuItem {
                case 1:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil, userInfo: ["direction":"right"])
                    break
                case 2:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_TWO), object: nil, userInfo: ["direction":"right"])
                    break
                case 3:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_THREE), object: nil, userInfo: ["direction":"right"])
                    break
                case 4:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_FOUR), object: nil, userInfo: ["direction":"right"])
                    break
                default:
                    break
                }
                
                
            }else {
                switch currentMenuItem {
                case 1:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil, userInfo: ["direction":"right"])
                    break
                case 2:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_TWO), object: nil, userInfo: ["direction":"right"])
                    break
                case 3:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_THREE), object: nil, userInfo: ["direction":"right"])
                    break
                case 4:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_FOUR), object: nil, userInfo: ["direction":"right"])
                    break
                default:
                    break
                }
                
            }
        }
        
    }
    
    @IBAction func leftAction(_ sender: DampingButton) {
        sender.tapDownHandler = {
          self.leftSpringView.animate(self.leftSpringView, transform: CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9))
        }
        
        sender.tapUpHandler = {
          self.leftSpringView.animate(self.leftSpringView, transform: .identity)
        }
        
        if(Singleton.shared.breakfastMenu.count != 0 || Singleton.shared.lunchMenu.count != 0){
            if(MENU_TYPE == K_BREAK_ID){
                
                switch currentMenuItem {
                case 1:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil, userInfo: ["direction":"left"])
                    break
                case 2:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_TWO), object: nil, userInfo: ["direction":"left"])
                    break
                case 3:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_THREE), object: nil, userInfo: ["direction":"left"])
                    break
                case 4:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_FOUR), object: nil, userInfo: ["direction":"left"])
                    break
                default:
                    break
                }
                
            }else {
                switch currentMenuItem {
                case 1:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil, userInfo: ["direction":"left"])
                    break
                case 2:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_TWO), object: nil, userInfo: ["direction":"left"])
                    break
                case 3:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_THREE), object: nil, userInfo: ["direction":"left"])
                    break
                case 4:
                    NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_FOUR), object: nil, userInfo: ["direction":"left"])
                    break
                default:
                    break
                }
            }
        }
    }
    
    @IBAction func breakfastAction(_ sender: Any) {
        if ((MENU_TYPE == K_BREAK_ID && Singleton.shared.breakfastMenu.count > 0) || (MENU_TYPE == K_LUNCH_ID && Singleton.shared.lunchMenu.count > 0)){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                self.rightView.isHidden = false
                self.leftView.isHidden = false
                self.exitButton.isHidden = false
                self.updateCount()
            }
            self.animateView(image: self.breakfastArrow)
            NotificationCenter.default.post(name: NSNotification.Name(N_RESET_COLLECTION_INDEX_ONE), object: nil, userInfo: ["index":1])
            let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
            WelcomePageViewController.index_delegate = self
            controller.setSecondController(direction: (self.currentIndex == 1 || self.currentIndex == 0) ? 1:(self.currentIndex == 4 ? 1:2))
            self.currentIndex = 1
            self.breakfastView.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            UIView.animate(withDuration: 0.3) {
                self.breakfastView.transform = .identity
            }
        }else {
            self.showAlert()
        }
    }
    
    @IBAction func dessertAction(_ sender: Any) {
        if ((MENU_TYPE == K_BREAK_ID && Singleton.shared.breakfastMenu.count > 0) || (MENU_TYPE == K_LUNCH_ID && Singleton.shared.lunchMenu.count > 0)){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                self.rightView.isHidden = false
                self.leftView.isHidden = false
                self.exitButton.isHidden = false
                self.updateCount()
            }
            self.animateView(image: self.dessertArrow)
            NotificationCenter.default.post(name: NSNotification.Name(N_RESET_COLLECTION_INDEX_THREE), object: nil, userInfo: ["index":2])
            let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
            WelcomePageViewController.index_delegate = self
            controller.setFOurthController(direction: self.currentIndex == 4 ? 2:1)
            self.currentIndex = 3
            self.dessertView.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            UIView.animate(withDuration: 0.3) {
                self.dessertView.transform = .identity
            }
        }else {
            self.showAlert()
        }
    }
    
    @IBAction func foodPlateAction(_ sender: Any) {
        if((MENU_TYPE == K_BREAK_ID && Singleton.shared.breakfastMenu.count > 0) || (MENU_TYPE == K_LUNCH_ID && Singleton.shared.lunchMenu.count > 0)){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                self.rightView.isHidden = false
                self.leftView.isHidden = false
                self.exitButton.isHidden = false
                self.updateCount()
            }
            self.animateView(image: self.foodPlateArrow)
            NotificationCenter.default.post(name: NSNotification.Name(N_RESET_COLLECTION_INDEX_TWO), object: nil, userInfo: ["index":3])
            let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
            WelcomePageViewController.index_delegate = self
            controller.setThirdController(direction: self.currentIndex == 1 ? 1:2)
            self.currentIndex = 2
            self.plateView.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            UIView.animate(withDuration: 0.3) {
                self.plateView.transform = .identity
            }
            
        }else {
            self.showAlert()
        }
    }
    
    @IBAction func cokeAction(_ sender: Any) {
        if((MENU_TYPE == K_BREAK_ID && Singleton.shared.breakfastMenu.count > 0) || (MENU_TYPE == K_LUNCH_ID && Singleton.shared.lunchMenu.count > 0)){
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                self.rightView.isHidden = false
                self.leftView.isHidden = false
                self.exitButton.isHidden = false
                self.updateCount()
            }
            self.animateView(image: self.cokeArrow)
            NotificationCenter.default.post(name: NSNotification.Name(N_RESET_COLLECTION_INDEX_FOUR), object: nil, userInfo: ["index":4])
            let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
            WelcomePageViewController.index_delegate = self
            controller.setFifthController(direction: self.currentIndex == 1 ? 2:1)
            self.currentIndex = 4
            self.beverageView.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            UIView.animate(withDuration: 0.3) {
                self.beverageView.transform = .identity
            }
        }else {
            self.showAlert()
        }
    }
    
    @IBAction func myBagAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        myVC.transitioningDelegate = self
        myVC.modalPresentationStyle = .custom
        myVC.modalPresentationCapturesStatusBarAppearance = true
        myVC.interactiveTransition = interactiveTransition
        myVC.navigationDelegate = self
        
        if(MENU_TYPE == K_LUNCH_ID){
            myVC.mainMenuId = Singleton.shared.lunchMenu[0].menu_id ?? 0
        }else {
            myVC.mainMenuId = Singleton.shared.breakfastMenu[0].menu_id ?? 0
        }
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
}

extension ViewController {
    
    func requestMicrophonePermission() {
        AVAudioSession.sharedInstance().requestRecordPermission { authorized in
            if !authorized {
                print("Show UI directing the user to the iOS Settings app")
            }
        }
    }
    
    func retrieveAuthorizationCode(completion: @escaping (String) -> Void){
        let headers: HTTPHeaders = [
            .authorization(bearerToken: "EAAAEdsEkXAis7qr9i2Z51PiLRl3tQ_ss-onvBrgCa1D7ZErJ6FXF5MQVdhu7w6d"),
            .accept("application/json"),
            .init(name: "Square-Version", value: "2021-05-13")
        ]
        AF.request("https://connect.squareup.com/mobile/authorization-code", method: .post, parameters: [ "location_id":"L41Q7BKZ0HJZJ"], encoding: JSONEncoding.default, headers: headers, interceptor: nil).responseString {
            (response) in
            if let data = SessionManager.shared.convertDataToObject(response: response.data, SquarePaymentResponse.self){
                if((data.message ?? "") != ""){
                    self.showMessage(view: self, message: data.message ?? "")
                }else {
                    completion(data.authorization_code ?? "")
                }
            }
        }
    }
    
    func authorizeReaderSDKIfNeeded() {
        if SQRDReaderSDK.shared.isAuthorized {
            UserDefaults.standard.setValue(true, forKey: UD_CARD_READER_CONNECTED)
            self.pairCardReaders()
        } else {
            UserDefaults.standard.removeObject(forKey: UD_CARD_READER_CONNECTED)
            self.retrieveAuthorizationCode { (response) in
                print("authorization id is:-",response)
                SQRDReaderSDK.shared.authorize(withCode: response) { val, error in
                    if let authError = error {
                        // Handle the error
                        self.showMessage(view: self, message: "Square Payment not Authorized")
                    } else {
                        self.pairCardReaders()
                    }
                }
            }
        }
    }
    
    func checkoutControllerDidCancel(_: SQRDCheckoutController) {
        self.showMessage(view: self, message: "Checkout cancelled")
    }
    
    // Failure delegate method
    func checkoutController(_: SQRDCheckoutController, didFailWith error: Error) {
        // Checkout controller errors are always of type SQRDCheckoutControllerError
        let checkoutControllerError = error as! SQRDCheckoutControllerError
        
        switch checkoutControllerError.code {
        case .sdkNotAuthorized:
            // Checkout failed because the SDK is not authorized
            // with a Square merchant account.
            self.showMessage(view: self, message: "Square Payment not authorized")
            break
            
        case .usageError:
            // Checkout failed due to a usage error. Inspect the userInfo
            // dictionary for additional information.
            if let debugMessage = checkoutControllerError.userInfo[SQRDErrorDebugMessageKey],
               let debugCode = checkoutControllerError.userInfo[SQRDErrorDebugCodeKey] {
                print(debugCode, debugMessage)
            }
            break
        }
    }
    
    
    func pairCardReaders() {
        if let authorized = UserDefaults.standard.value(forKey: UD_CARD_READER_CONNECTED) as? Bool {
            if(authorized){
                return
            }
        }
        let readerSettingsController = SQRDReaderSettingsController(
            delegate: self
        )
        
        self.dismiss(animated: false) {
            readerSettingsController.present(from: self)
        }
        
    }
    
    
    
    func readerSettingsController(
        _: SQRDReaderSettingsController,
        didFailToPresentWith error: Error
    ) {
        // Handle the error - this example prints the error to the console
        self.showMessage(view: self, message: "This device is not currently authorized to accept payments with Square.")
        print(error)
    }
}

extension ViewController: SQRDReaderSettingsControllerDelegate {
    func readerSettingsControllerDidPresent(_ readerSettingsController: SQRDReaderSettingsController) {
        print("The Reader Settings controller did present.")
    }
    
    
}


