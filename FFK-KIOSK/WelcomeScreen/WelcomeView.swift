//
//  WelcomeView.swift
//  FFK-KIOSK
//
//  Created by qw on 23/12/20.
//

import UIKit

class WelcomeView: UIView {
    //MARK: IBOUtlets
   
    @IBOutlet var contentview: UIView!
    @IBOutlet weak var animateView: UIView!
    

    override init(frame: CGRect) {
         super.init(frame: frame)
         commonInit()
     }
     
     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
         commonInit()
     }
     
     func commonInit() {
        Bundle.main.loadNibNamed("WelcomeView", owner: self, options: nil)
        contentview.fixInView(self)
        self.performAnimation()
     }
    
    
    func performAnimation(){
        UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping:0.5, initialSpringVelocity: 5, options: [.repeat]) {
            self.animateView.transform =
                CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
        } completion: { (val) in
            self.animateView.transform = CGAffineTransform.identity.scaledBy(x: 0.1, y: 0.1)
           
        }

//        UIView.animate(withDuration: 1, animations: { [self] in
//
//           }) { finished in
//               UIView.animate(withDuration: 3, animations: {
//
//               })
//           }
    }
}
