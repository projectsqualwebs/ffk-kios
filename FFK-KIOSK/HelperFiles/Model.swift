//
//  Model.swift
//  FFK-KIOSK
//
//  Created by qw on 09/01/21.
//

import Foundation


struct ErrorResponse: Codable{
    var message: String?
    var status: Int?
}

struct SuccessResponse: Codable{
    var message: String?
    var status: Int?
}

struct MenuData: Codable{
    var name: String?
    var image: String?
    var price: String?
}

struct GetLocation: Codable{
    var response: [LocationResponse]
    var message: String?
    var status: Int?
}

struct GetLocationDetail: Codable{
    var response: LocationResponse
    var message: String?
    var status: Int?
}

struct LocationResponse: Codable{
    var id: Int?
    var placeId: String?
    var name: String?
    var address: String?
    var latitude: String?
    var longitude: String?
    var isSelected: Bool?
    var price: Double?
    var image: String?
    var is_opened: Int?
    var tax_rate: Double?
    var description: String?
    var complete_meal_of: String?
    var contact_number: String?
    var distance: Double?
    //var restaurant_id: Int?
    var additional_info: String?
}

struct CardItems: Codable {
        var user_id:Int
        var cart_data:[CartData]
        var country:String
    
    
    
        init(user_id: Int,cart_data:[CartData],country: String) {
        self.user_id = user_id
        self.cart_data  = cart_data
        self.country = country
    }
}

struct CartData: Codable {
   var  main_menu_id:Int
    var side_menu: [SideMenu]
    
    init(main_menu_id: Int,side_menu:[SideMenu]) {
        self.main_menu_id = main_menu_id
        self.side_menu  = side_menu
    }
}

struct AddToCart: Codable{
    var modifier_id: String?
    var modifier_item_id: String?
    var modifier_item_count:String?
    var item_count: Int?
}

struct SideMenu: Codable {
    var id: Int
    var category_id: Int
    init(id: Int,category_id:Int) {
        self.id = id
        self.category_id  = category_id
    }
}

struct GetMenu: Codable{
    var response: [GetMenuResponse]
    var message: String?
    var status: Int?
}

struct GetMenuResponse:Codable{
    var menu_id:Int?
    var restaurant_id: Int?
    var menu_name:String?
    var menu_resume_time_message: String?
    var from:Int?
    var slot_start: Int?
    var to:Int?
}

struct GetMeals: Codable{
    var response: [GetMealsResponse]
    var message: String?
    var status: Int?
}

struct GetMealsResponse: Codable{
    var id:Int?
    var cart_item_id: Int?
    var cart_list_id: Int?
    var item_flag: Int?
    var item_id: Int?
    var menu_id: Int?
    var meal: OrderMeals?
}

struct OrderMeals: Codable{
    var item_id:Int?
    var modifiers:Int?
    var side_menu_id:Int?
    var item_name:String?
    var item_price:Double?
    var item_image:String?
    var thumbnail:String?
    var tax_rate:Double?
    var description:String?
    var isSelected: Int?
    var its_own:Int?
}

struct GetBreakLunchResonse: Codable{
    var response: [GetBreakfastLunch]
    var message: String?
    var status: Int?
}

struct GetBreakfastLunch: Codable{
    var menu_id:Int?
    var category_id:Int?
    var menu_type_id: Int?
    var side_menu_id: Int?
    var category_name:String?
    var menus:[GetMenusList]
}

struct GetMenusList: Codable
{
    var item_id:Int?
    var category_id:Int?
    var item_name:String?
    var item_price:Double?
    var item_image:String?
    var ipad_image: String?
    var ipad_image_single: String?
    var modifiers: Int?
    var thumbnail:String?
    var is_common: Int?
    var tax_rate:Double?
    var item_description:String?
    var reward_item: Int?
    var item_count: Int?
    //var its_own:Int?
   // var complete_meal_of: Int?
}

struct GetSubmenu: Codable {
    var response: [SubMenuResponse]
    var message: String?
    var status: Int?
}

struct SubMenuResponse: Codable {
    var id:Int?
    var welcome_bonus_item_id: Int?
    var modifier_group_id:Int?
    var item_id:Int?
    var added_from: Int?
    var item_exactly:Int?
    var item_range_from: Int?
    var item_count: Int?
    var item_range_to: Int?
    var restaurant_id: Int?
    var item_maximum: Int?
    var single_item_maximum: Int?
    var modifier_group_name:String?
    var is_rule:Int?
    var sectionItemCount: Int?
    var meals:[SubmenuSubCategory]
}

struct SubmenuSubCategory: Codable {
    var id:Int?
    var cart_item_id:Int?
    var item_id: Int?
    var side_menu_id:Int?
    var modifier_group_id: Int?
    var category_id:Int?
    var item_name:String?
    var item_price:Double?
    var item_image:String?
    var thumbnail:String?
    var tax_rate:Double?
    var item_description:String?
    var ipad_image: String?
    var ipad_image_single: String?
    var its_own:Int?
    var isSelected:Int?
    var itemCount:Int?
    var count: Int?
    var restaurant_id: Int?
    var is_checked: Int?
   // var modifier_item_count: Int?
    var item_count: Int?
}

struct AddToCartResponse: Codable {
    var response:String
    var message: String
    var status: Int?
}


struct GetCart: Codable {
    var response = MyCartData()
    var status = Int()
    var message = String()
}

struct MyCartData: Codable {
    var cart_data = [GetCartResponse]()
    var cart_details = CartDetailResponse()
}

struct CartDetailResponse: Codable {
    var cart_list_id: Int?
    var cart_id: String?
    var total_tax: String?
    var order_total: String?
    var total_amount: String?
    var is_birthday: Int?
    var is_reward_coupon: Int?
    var is_admin_reward: Int?
    var is_bonus: Int?
    var bonus_id: Int?
    var is_reward_added: Int?
    var discount_amount: String?
    var coupon_id: Int?
    var coupon_type: Int?
}

struct GetCartResponse: Codable {
    var id: Int?
    var cart_item_id: Int?
    var receiver_name: String?
    var cart_list_id: Int?
    var item_id: Int?
    var user_id: Int?
    var item_count: Int?
    var new_item_count: Int?
    var menu_id: Int?
    var menu_price: Int?
    var item_flag: Int?
    var bonus_id: Int?
    var cart_id: String?
    var modifier_id: Int?
    var modifier_item_id: Int?
    var modifier_item_count: Int?
    var modifier_item_price: Int?
    var item_name: String?
    var item_price: Double?
    var item_image: String?
    var tax_rate: Double?
    var is_common:Int?
    var item_description: String?
   // var meals: [SubmenuSubCategory]
    var details: [SubmenuSubCategory]
}

struct SearchUser: Codable {
    var response:SearchUserResponse?
    var message: String?
    var status: Int?
}

struct SearchUserResponse: Codable {
    var id: Int?
    var first_name: String?
    var last_name: String?
    var email: String?
    var mobile: String?
    var zip_code: String?
    var date_of_birth: String?
    var google_id: String?
    var facebook_id: String?
    var latitude: String?
    var longitude: String?
    var restaurant_preferences: Int?
    var is_account_deleted: Int?
    var customer_id: String?
    var apple_id: String?
}

struct GuestCheckoutResponse: Codable{
    var response : OrderResponse?
      var message: String?
}

struct OrderResponse: Codable{
    var order_id: Int?
    var menu_name: String?
    var reference_id: String?
    var restaurant_name: String?
    var restaurant_address: String?
    var restaurant_contact_number: String?
    var pickup_time: Int?
    var menu_id: Int?
    var total_tax: String?
    var order_total: String?
    var discount_amount: String?
    var total_amount: String?
    var status: Int?
    var address: String?
    var item_price: Double?
    var is_favorite: Int?
    var favorite_order_id: Int?
    var label_name: String?
    var favorite_label_id: Int?
    var total_rewards: Double?
    var reward_coupons_status: Int?
    var coupon_type: Int?
    var coupon_id: Int?
    var bonus_id: Int?
    var order_date: Int?
    var is_common: Int?
    var is_in_stock:Int?
    var order_details: [OrderDetail]?
    var feedback: FeedbackResponse?
}

struct FeedbackResponse: Codable {
    var feedback: Int?
    var order_id: Int?
    var review: String?
}

struct OrderDetail: Codable{
    var order_detail_id: Int?
    var order_id: Int?
    var restaurant_id: Int?
    var item_id: Int?
    var item_count: Int?
    var item_name: String?
    var item_price: Double?
    var tax_rate: Double?
    var item_image: String?
    var order_item: [OrderDetail]?
}

struct SquarePaymentResponse: Codable{
    var authorization_code:String?
    var message: String?
}

struct CheckoutObject: Codable {
    var item_id: Int?
    var item_count: Int?
    var modifiers = [AddToCart]()
}


struct OrderConfirmed: Codable {
    var response: OrderConfirmResponse?
    var message: String?
    var status: Int?
}

struct OrderConfirmResponse: Codable {
    var order_id: Int?
}


struct GetSliderImage: Codable {
    var response = [SliderImageResponse]()
    var message: String?
    var staths: Int?
}

struct SliderImageResponse: Codable {
    var id: Int?
    var path: String?
    var status: Int?
}
