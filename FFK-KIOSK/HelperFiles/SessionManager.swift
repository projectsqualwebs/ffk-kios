//
//  SessionManager.swift
//  FFK-KIOSK
//
//  Created by qw on 14/01/21.
//

import UIKit
import Alamofire


struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
    }
}

class SessionManager: NSObject {

    static var shared = SessionManager()

    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        if(Connectivity.isConnectedToInternet){
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")

            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode != 400){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }else if(statusCode == 200){
                   object = self.convertDataToObject(response: dataResponse.data, T.self)
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                   
                  //  NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                    //self.showAlert(msg: errorObject?.message)
                }else if statusCode == 400{

                        if(errorObject?.message != "" || errorObject?.message != nil){
                      
                        }
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
                    self.showAlert(msg: errorObject?.message)
                    
                } else {
//                    if(errorObject?.message != "" || errorObject?.message != nil){
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
//                    }
                    // NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                     // self.showAlert(msg: errorObject?.message)
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
               // ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                    }
                   //  NavigationController.shared.showAlertScreen(message: error ?? "")
                    //self.showAlert(msg: error)
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])
                } else {
                    //Showing error message on alert
                   // self.showAlert(msg: error)
                    NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": error ?? ""])

                }
                break
            }
        }
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": "The Internet connection appears to be offline. Please connect via network and restart app again."])
            }
            
        }
    }

    private func showAlert(msg: String?) {
        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title:"", message: msg, action1Name: "Ok", action2Name: nil)

    }


     func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }

    
    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (1==1){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + token!]
            }
            } else {
                return nil
            }
        }
    }
