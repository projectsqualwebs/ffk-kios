//
//  Singleton.swift
//  FFK-KIOSK
//
//  Created by qw on 18/01/21.
//

import UIKit
import LocalAuthentication
import CoreLocation

class Singleton {
    static let shared = Singleton()
    var cityName:String?
    var address:String?
    var seletedTime:String?
    var selectedRewardCoupenId: Int?
    var selectedLocation = getSelectedRestaurant()
    var restaurentLocation = getLocation()
    var mainMenuData = [GetMenuResponse]()
    var lunchMenu = [GetBreakfastLunch]()
    var breakfastMenu = [GetBreakfastLunch]()
    var breakfastMessage = ""
    var lunchMessage = ""
    var mealsData = [GetMealsResponse]()
    var currentLocation = CLLocationCoordinate2D()
    var cartItemCount = 0
    var cartData = MyCartData()
    var currentCollectionIndex = [0,0,0,0]
    var userDetail = SearchUserResponse()
    
    public static func getLocation() -> [LocationResponse]{
        if let info = UserDefaults.standard.data(forKey: UD_RESTAURANTS){
            let locData = info
            let loc = try! JSONDecoder().decode([LocationResponse].self, from: locData)
            return loc
        }else {
            return [LocationResponse]()
        }
    }
    
    public static func getSelectedRestaurant() -> LocationResponse{
        if let info = UserDefaults.standard.data(forKey: UD_SELECTED_RESTAURANT){
            let locData = info
            let loc = try! JSONDecoder().decode(LocationResponse.self, from: locData)
            return loc
        }else {
            return LocationResponse()
        }
    }
    


  
    func initialiseValues(){
        cityName = String()
        address = String()
        selectedRewardCoupenId = nil
        seletedTime = String()
        mealsData = []
        userDetail = SearchUserResponse()
     }
}

