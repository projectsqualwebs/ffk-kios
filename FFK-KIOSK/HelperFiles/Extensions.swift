//
//  Extensions.swift
//  FFK-KIOSK
//
//  Created by qw on 23/12/20.
//

import UIKit
import CoreLocation
import AVFoundation
import RealmSwift


extension UIViewController: ResetView {
    func showHideView(view: UIView){
        view.isHidden = false
        view.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
        print("one:-",view.frame.width)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut) {
            view.transform = CGAffineTransform(scaleX: 1, y: 1)
            print("two:-",view.frame.width)
        } completion: { (val) in
          //  self.itemAddedView.isHidden = true
           // self.itemAddedView.transform = .identity
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut) {
                view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                view.alpha  = 0
            } completion: { (val) in
                view.isHidden = true
                view.transform = CGAffineTransform(scaleX: 0, y: 0)
                view.alpha  = 1
            }
        }
    }
    
    func clearNavigationStack() {
            let controller = WelcomePageViewController.customDataSource as! WelcomePageViewController
            controller.setFirstController(direction: 1)
            NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["showIndicator":false,"isExit":true])
        if(K_IS_MENU_ITEM_CHANGED){
          NotificationCenter.default.post(name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil, userInfo: ["changeMenu":true])
        }
        navigationController?.viewControllers.removeAll(where: { (vc) -> Bool in
            if (vc.isKind(of: ViewController.self)) {
                return false
            } else {
                return true
            }
        })
    }
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        controller.navigationController?.view.layer.add(transition, forKey: kCATransition)
    }
    
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        //        if let popoverPresentationController = alertController.popoverPresentationController {
        //            popoverPresentationController.sourceView = self.view
        //            popoverPresentationController.sourceRect = alertController.view.bounds
        //        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    //NAVIGATE HOME
    func pushHome(controller: UIViewController) {
        let cartData:[MenuList] = DBHelper.shared.getCartItem()
        if(cartData.count == 0){
            if(self.navigationController == nil){
                self.dismiss(animated: true, completion: nil)
            }else {
                self.navigationController?.popViewController(animated: true)
            }
        }else {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ExitPopupViewController") as! ExitPopupViewController
        myVC.navigationDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        controller.present(myVC, animated: false, completion: nil)
        }
    }

    func calculateTimeDifference(date1: Int, date2: Int) -> Int{
        let d1 = Date(timeIntervalSince1970: TimeInterval(date1))
        let d2 = Date(timeIntervalSince1970: TimeInterval(date2))
        let diff = d2.timeIntervalSince(d1)
        return Int(diff)
    }
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(identifier: "America/Chicago")
            
        //dateFormatter.locale = TimeZone.current
        return dateFormatter.string(from: date)
    }
    
    func convertDateToTimestamp(_ date: String, to format: String) -> Int{
        let dfmatter = DateFormatter()
        dfmatter.dateFormat = format
         dfmatter.timeZone = TimeZone(identifier: "America/Chicago")
        let date = dfmatter.date(from: date)
        var dateStamp:TimeInterval?
        if let myDate = date {
            dateStamp = myDate.timeIntervalSince1970
            let dateSt:Int = Int(dateStamp!)
            return dateSt
        }else {
            return Int(Date().timeIntervalSince1970)
        }
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func changeColor(string:String,colorString: String,color: UIColor,field:UILabel){
        let main_string = string
        var string_to_color = colorString
        //colorString.font
        var range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        if let font = UIFont(name: "Raleway-SemiBold", size: 20) {
            let fontAttributes = [NSAttributedString.Key.font: font]
            let size = (string_to_color as NSString).size(withAttributes: fontAttributes)
        }
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        field.attributedText = attribute
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    func getTotalIndex(count:Int)->Int{
        if(count%4 == 0){
            return count/4
        }else {
            let a = Int(round(Double(Double(count)/4)))
            if(Double(count)/4 > Double(a)){
                return a+1
            }else {
                return a
            }
        }
    }
    
    func handleLeft(collectionView: UICollectionView, itemIndex: Int){
        
        collectionView.scrollToItem(at: IndexPath(row:4*(itemIndex), section: 0), at: .left, animated: true)
        print(4*(itemIndex))
        
    }
    
    
    func handleRight(collectionView: UICollectionView, itemIndex: Int){
        
        if(itemIndex == 1 || itemIndex == 2){
            collectionView.scrollToItem(at: IndexPath(row:0, section: 0), at: .right, animated: true)
                
        }else {
            collectionView.scrollToItem(at: IndexPath(row:4*(itemIndex-2), section: 0), at: .right, animated: true)
            print(4*(itemIndex-2))
        }
        
    }
    
    func showMessage(view: UIViewController,message: String){
      //  view.dismiss(animated: false, completion: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.heading = "\(message)"
        view.navigationController?.present(myVC, animated: false, completion: nil)
    }
}


extension String {
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    func appendString(data: Double) -> String { // changed input type of data
        let value = data
        let formatter = NumberFormatter()
        formatter.minimumFractionDigits = 2 // for float
        formatter.maximumFractionDigits = 2 // for float
        formatter.minimumIntegerDigits = 1
        formatter.paddingPosition = .afterPrefix
        formatter.paddingCharacter = "0"
        return formatter.string(from: NSNumber(floatLiteral: value))! // here double() is not required as data is already double
    }
}

extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }

}

extension UITableView {
    func fadeEdges(with modifier: CGFloat) {

        let visibleCells = self.visibleCells

        guard !visibleCells.isEmpty else { return }
        guard let topCell = visibleCells.first else { return }
        guard let bottomCell = visibleCells.last else { return }

        visibleCells.forEach {
            $0.contentView.alpha = 1
        }

        let cellHeight = topCell.frame.height - 1
        let tableViewTopPosition = self.frame.origin.y
        let tableViewBottomPosition = self.frame.maxY

        guard let topCellIndexpath = self.indexPath(for: topCell) else { return }
        let topCellPositionInTableView = self.rectForRow(at:topCellIndexpath)

        guard let bottomCellIndexpath = self.indexPath(for: bottomCell) else { return }
        let bottomCellPositionInTableView = self.rectForRow(at: bottomCellIndexpath)

        let topCellPosition = self.convert(topCellPositionInTableView, to: self.superview).origin.y
        let bottomCellPosition = self.convert(bottomCellPositionInTableView, to: self.superview).origin.y + cellHeight
        let topCellOpacity = (1.0 - ((tableViewTopPosition - topCellPosition) / cellHeight) * modifier)
        let bottomCellOpacity = (1.0 - ((bottomCellPosition - tableViewBottomPosition) / cellHeight) * modifier)

        topCell.contentView.alpha = topCellOpacity
        bottomCell.contentView.alpha = bottomCellOpacity
    }
}


extension UIView
{
    func fixInView(_ container: UIView!) -> Void{
        self.translatesAutoresizingMaskIntoConstraints = false;
        self.frame = container.frame;
        container.addSubview(self);
        NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0).isActive = true
    }
    
    func bounceAnimationView(view: UIView){
        view.alpha = 1
        view.isHidden = false
        view.transform = CGAffineTransform(scaleX: 0.8, y: 1.2)
               
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [],  animations: {
           view.transform = .identity
        })
    }
    
    func pulsa() {
    let pulse = CASpringAnimation(keyPath: "transform.scale")
    pulse.duration = 0.4
    pulse.fromValue = 0.98
    pulse.toValue = 1.0
    pulse.autoreverses = true
    pulse.repeatCount = .infinity
    pulse.initialVelocity = 0.5
    pulse.damping = 1.0
    layer.add(pulse, forKey: nil)
    }
    
    func animShow(){
        if(self.isHidden == false){
            return
        }

//        self.isHidden = false
      //  self.alpha = 0
        if(UIApplication.shared.applicationState != .background) {
        self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.transition(with: self, duration: 0.25
                          , options: [.transitionCrossDissolve], animations: {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.curveLinear],
                           animations: {
                            self.center.y -= self.bounds.height
                            self.transform = .identity
                            self.layoutIfNeeded()
            }, completion: nil)
                            
         }) { (val) in
        }
        }
        self.isHidden = false
        self.transform = .identity
    }
    
    func animHide(){
        if(self.isHidden == true){
            return
        }
        if(UIApplication.shared.applicationState != .background) {
        self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.transition(with: self, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            UIView.animate(withDuration: 0.25, delay: 0, options: [.curveLinear],
                           animations: {
                            self.center.y += self.bounds.height
                            self.layoutIfNeeded()
                            self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            }, completion: nil)
            
        }) { (val) in
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }
        }
        self.isHidden = true
        self.transform = .identity
    }
    
    func animate(_ view: UIView, transform: CGAffineTransform) {
        UIView.animate(withDuration: 0.25,
                       delay: 0,
                       usingSpringWithDamping: 0.55,
                       initialSpringVelocity: 3,
                       options: [.curveEaseInOut],
                       animations: {
                        view.transform = transform
            }, completion: nil)
    }
    
}

extension UIImageView{
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.4
        pulse.fromValue = 0.98
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = .infinity
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
     //  pulse.repeatDuration = 1.0
        layer.add(pulse, forKey: nil)
    }
}

class DampingButton: UIButton {

    var tapDownHandler: (()-> Void)?
    
    var tapUpHandler: (()-> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    init() {
        super.init(frame: .zero)
        commonInit()
    }

    private func commonInit() {
        self.addTarget(self, action: #selector(self.touchDownHandler), for: [.touchDown, .touchDragEnter])
        self.addTarget(self, action: #selector(self.touchUpHandler), for: [.touchDragExit, .touchCancel, .touchUpInside, .touchUpOutside])
    }

    @objc func touchDownHandler() {
        self.animate(self, transform: CGAffineTransform.identity.scaledBy(x: 0.97, y: 0.97))

        if let handler = self.tapDownHandler {
            handler()
        }
    }
    
    @objc func touchUpHandler() {
        self.animate(self, transform: .identity)

        if let handler = self.tapUpHandler {
            handler()
        }
    }
}

class RapidFireButton: UIButton {

    private var rapidFireTimer: Timer?
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    init() {
        super.init(frame: .zero)
        commonInit()
    }

    private func commonInit() {
        addTarget(self, action: #selector(touchDownHandler), for: .touchDown)
    }

    @objc private func touchDownHandler() {
        rapidFireTimer?.invalidate()
        rapidFireTimer = nil
        rapidFireTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [unowned self] (timer) in
            self.tapHandler(self)
        })
    }


    var tapHandler: (RapidFireButton) -> Void = { button in

    }
}

extension UIApplication{
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let navigationController = controller as? UINavigationController {
                return topViewController(controller: navigationController.visibleViewController)
            }
            else if let tabController = controller as? UITabBarController {
                if let selected = tabController.selectedViewController {
                    return topViewController(controller: selected)
                }
            }
            else if let presented = controller?.presentedViewController {
                return topViewController(controller: presented)
            }
            return controller
    }
}
