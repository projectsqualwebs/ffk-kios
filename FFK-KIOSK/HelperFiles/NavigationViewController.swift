//
//  NavigationViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 26/01/21.
//

import UIKit
import RMPZoomTransitionAnimator_Swift


class NavigationViewController: UINavigationController,UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self

    }

    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationController.Operation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard let sourceTransition = fromVC as? RMPZoomTransitionAnimating,
              let destinationTransition = toVC as? RMPZoomTransitionable else {
                return nil
        }

        let animator = RMPZoomTransitionAnimator()
        animator.goingForward = (operation == .push)
        animator.sourceTransition = sourceTransition
        animator.destinationTransition = destinationTransition
        return animator;
    }

}
