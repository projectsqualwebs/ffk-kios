//
//  RealtimeFirebase.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 06/09/21.
//

import Foundation
import Firebase
import FirebaseDatabase

class RealtimeFirebase{
    static var shared = RealtimeFirebase()
    var isFirstTime = true
    
    public func checkRestaurantStatus(){
        ref.child("restaurant_2").child("settings").observe(.value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            K_IS_KIOSK_ONLINE = value?["is_kiosk_online"] as? String ?? "1"
            if(K_IS_KIOSK_ONLINE == "0"){
                K_SHOW_OFFLINE_POPUP = true
            }
        })
    }

    public func addMenuChangeObserver(completionHandler: @escaping (String) -> Void) {
    
        ref.child("restaurant_2").child("kioskUpdate").observe(.value, with: { (snapshot) in
            if(self.isFirstTime == false){
                self.isFirstTime = false
                completionHandler("test")
            }else {
                self.isFirstTime = false
            }
        })
    }
}
