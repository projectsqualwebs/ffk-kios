//
//  NavigationController.swift
//  FFK-KIOSK
//
//  Created by qw on 01/01/21.
//

import UIKit
import CoreLocation

class NavigationController: UIViewController, CLLocationManagerDelegate {
    //MARK: IBOutlets
    
    
    static let shared = NavigationController()
    lazy var locationManager = CLLocationManager()
    var timer: Timer?
    var isFirsTime = false
    //Demo timings
            var bStart = Calendar.current.date(bySettingHour:0, minute:1, second: 0, of: Date()) ?? Date()
            var bEnd = Calendar.current.date(bySettingHour:0, minute:10, second: 0, of: Date()) ?? Date()
            var lStart = Calendar.current.date(bySettingHour:0, minute:11, second: 0, of: Date()) ?? Date()
            var lEnd = Calendar.current.date(bySettingHour:  0, minute:20, second: 0, of: Date()) ?? Date()
            let dayEnd = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) ?? Date()
            let dayStart = Calendar.current.date(bySettingHour: 0, minute: 16, second: 00, of: Date()) ?? Date()
            let sundayStart = Calendar.current.date(bySettingHour: 11, minute: 00, second: 00, of: Date()) ?? Date()
            let sundayEnd = Calendar.current.date(bySettingHour: 15, minute: 30, second: 00, of: Date()) ?? Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    func getPreferredRestaurant(){
        let location = Singleton.shared.selectedLocation
        let coordinate = self.getCurrentLocation()
        if(location.id == nil){
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_PREFERRED_RESTAURANT, method: .post, parameter: ["latitude":Singleton.shared.currentLocation.latitude,"longitude":Singleton.shared.currentLocation.longitude], objectClass: GetLocationDetail.self, requestCode: U_GET_PREFERRED_RESTAURANT, userToken: nil) { (response) in
                let selectedLoc = try! JSONEncoder().encode(response.response)
                Singleton.shared.selectedLocation = response.response
                UserDefaults.standard.set(selectedLoc, forKey: UD_SELECTED_RESTAURANT)
                self.getMenuType()
            }
        }else {
            self.getMenuType()
        }
    }
    
    func getCurrentLocation(){
        if(self.hasLocPermission()){
            if let loc = locationManager.location {
                Singleton.shared.currentLocation = loc.coordinate
            }
        }
    }
    
    func hasLocPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    func getLocations() {
        let locations = Singleton.shared.restaurentLocation
        if (locations.count > 0){
            Singleton.shared.selectedLocation = locations[0]
            NavigationController.shared.getMenuType()
        }else {
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_RESTAURENTS, method: .get, parameter: nil, objectClass: GetLocation.self, requestCode: U_GET_RESTAURENTS, userToken: nil) { (response) in
                Singleton.shared.restaurentLocation = response.response
                Singleton.shared.selectedLocation = response.response[0]
                let selectedLoc = try! JSONEncoder().encode(response.response)
                UserDefaults.standard.set(selectedLoc, forKey: UD_RESTAURANTS)
                self.getMenuType()
            }
        }
    }
    
    func emptyMyCart() {
        Singleton.shared.userDetail = SearchUserResponse()
        Singleton.shared.cartItemCount = 0
        DBHelper.shared.emptyLocalStorage()
    }
    
    @objc func getMenuType(){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU_TYPE +  "\(Singleton.shared.selectedLocation.id ?? 0)", method: .get, parameter: nil, objectClass: GetMenu.self, requestCode: U_GET_MENU_TYPE, userToken: nil) { (dataResponse) in
            if(dataResponse.response.count > 1){
                Singleton.shared.mainMenuData = dataResponse.response
                K_BREAK_ID = dataResponse.response[0].menu_id ?? 0
                K_LUNCH_ID = dataResponse.response[1].menu_id ?? 0
                let components = Calendar.current.dateComponents([.weekday], from: Date())
                self.isFirsTime = true
                self.checkMenuTiming()
                self.timer?.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: K_MAIN_TIMER_INTERVAL, target: self, selector: #selector(self.checkMenuTiming), userInfo: nil, repeats: true)
            }
        }
    }
    
    @objc func checkMenuTiming(){
        if(K_IS_KIOSK_ONLINE == "1"){
            //7-10am 11-6:45 pm
            //let date = NSDate()
            //Realm Timings
//            let calendar = Calendar.current
//            let bStart = Calendar.current.date(bySettingHour:6, minute: 58, second: 0, of: Date()) ?? Date()
//            let bEnd = Calendar.current.date(bySettingHour: 10, minute: 30, second: 0, of: Date()) ?? Date()
//            let lStart = Calendar.current.date(bySettingHour:10, minute:58, second: 0, of: Date()) ?? Date()
//            let lEnd = Calendar.current.date(bySettingHour: 18, minute:45, second: 0, of: Date()) ?? Date()
//            let dayEnd = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) ?? Date()
//            let dayStart = Calendar.current.date(bySettingHour: 0, minute: 2, second: 00, of: Date()) ?? Date()
//            let sundayStart = Calendar.current.date(bySettingHour: 10, minute: 55, second: 00, of: Date()) ?? Date()
//            let sundayEnd = Calendar.current.date(bySettingHour: 15, minute: 30, second: 00, of: Date()) ?? Date()
            let components = Calendar.current.dateComponents([.weekday], from: Date())
            
            if(components.weekday == 1){
                if((self.sundayStart.timeIntervalSince1970 >= Date().timeIntervalSince1970) && (self.sundayEnd.timeIntervalSince1970 > Date().timeIntervalSince1970)){
                    if(MENU_TYPE != 0 ){
                        MENU_TYPE = 0
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil,userInfo: ["hide":true])
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }
                }else if((self.sundayStart.timeIntervalSince1970 >= Date().timeIntervalSince1970) || (self.sundayEnd.timeIntervalSince1970 >= Date().timeIntervalSince1970)){
                    MENU_TYPE = K_LUNCH_ID
                    if(Singleton.shared.lunchMenu.count == 0 || MENU_TYPE != K_LUNCH_ID){
                        self.getMenu()
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_MENU_DATA), object:nil))
                        if(MENU_TYPE != 0 && self.isFirsTime == false){
                            self.emptyMyCart()
                            Singleton.shared.cartItemCount = 0
                            DBHelper.shared.emptyLocalStorage()
                            self.clearNavigationStack()
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_SWIPE_CARD), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ITEM_DETAIL_SCREEN), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil)
                        }
                    }
                }else if(self.dayEnd.timeIntervalSince1970 >= Date().timeIntervalSince1970){
                    if(MENU_TYPE != 0 || self.isFirsTime){
                        NotificationCenter.default.post(name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
                        self.isFirsTime = false
                        MENU_TYPE = 0
                        Singleton.shared.breakfastMenu = []
                        Singleton.shared.lunchMenu = []
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }
                    MENU_TYPE = 0
                }else if(self.dayStart.timeIntervalSince1970 >= Date().timeIntervalSince1970){
                    MENU_TYPE = 0
                    self.getMenuType()
                }
            }else {
                if((self.bStart.timeIntervalSince1970 >= Date().timeIntervalSince1970) && (self.bEnd.timeIntervalSince1970 > Date().timeIntervalSince1970)){
                    if(MENU_TYPE != 0 ){
                        MENU_TYPE = 0
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }else{
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil,userInfo: ["hide":true])
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }
                }else if((self.bStart.timeIntervalSince1970 >= Date().timeIntervalSince1970) || (self.bEnd.timeIntervalSince1970 > Date().timeIntervalSince1970)){
                    MENU_TYPE = K_BREAK_ID
                    if(Singleton.shared.breakfastMenu.count == 0 || MENU_TYPE != K_BREAK_ID){
                        self.getMenu()
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_MENU_DATA), object:nil))
                        if(MENU_TYPE != 0 && self.isFirsTime == false){
                            self.isFirsTime = false
                            self.emptyMyCart()
                            Singleton.shared.cartItemCount = 0
                            DBHelper.shared.emptyLocalStorage()
                            self.clearNavigationStack()
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_SWIPE_CARD), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ITEM_DETAIL_SCREEN), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil)
                        }
                    }
                }else if((self.bEnd.timeIntervalSince1970 <= Date().timeIntervalSince1970) && (self.lStart.timeIntervalSince1970 > Date().timeIntervalSince1970)){
                    if(MENU_TYPE != 0 || self.isFirsTime){
                        self.isFirsTime = false
                        MENU_TYPE = 0
                        NotificationCenter.default.post(name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }
                    MENU_TYPE = 0
                }else if((self.lStart.timeIntervalSince1970 >= Date().timeIntervalSince1970) || (self.lEnd.timeIntervalSince1970 >= Date().timeIntervalSince1970)){
                    MENU_TYPE = K_LUNCH_ID
                    if(Singleton.shared.lunchMenu.count == 0 || MENU_TYPE != K_LUNCH_ID){
                        self.getMenu()
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_MENU_DATA), object:nil))
                    }
                }else if(self.dayEnd.timeIntervalSince1970 >= Date().timeIntervalSince1970){
                    if(MENU_TYPE != 0 || self.isFirsTime){
                        NotificationCenter.default.post(name: NSNotification.Name(N_RESET_ON_MENU_CHANGE), object: nil)
                        self.isFirsTime = false
                        //                            self.bStart = calendar.date(byAdding: .second, value: 10, to: Date())!
                        //                            self.bEnd = calendar.date(byAdding: .second, value: 600, to: Date())!
                        //                            self.lStart = calendar.date(byAdding: .second, value: 620, to: Date())!
                        //                            self.lEnd = calendar.date(byAdding: .second, value: 1200, to: Date())!
                        MENU_TYPE = 0
                        Singleton.shared.breakfastMenu = []
                        Singleton.shared.lunchMenu = []
                        NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                        NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                    }
                    MENU_TYPE = 0
                }else if(self.dayStart.timeIntervalSince1970 >= Date().timeIntervalSince1970){
                    MENU_TYPE = 0
                    Singleton.shared.breakfastMenu = []
                    Singleton.shared.lunchMenu = []
                    NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
                }
            }
        }else {
            if(MENU_TYPE != 0){
                self.isFirsTime = true
                NotificationCenter.default.post(name: NSNotification.Name(N_MANAGE_KITCHEN_OFFLINE), object: nil, userInfo: ["changeMenu":false])
            }
        }
        
    }
    
    func serverToLocal(date:String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let localDate = dateFormatter.date(from: date)
        
        return localDate
    }
    
    func getMenu() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU + "\(MENU_TYPE)" + "/\(Singleton.shared.selectedLocation.id ?? 1)"  , method: .get, parameter: nil, objectClass: GetBreakLunchResonse.self, requestCode: U_GET_MENU, userToken: nil) { (dataResponse) in
            if(MENU_TYPE == K_LUNCH_ID){
                Singleton.shared.lunchMenu = dataResponse.response
            }else if(MENU_TYPE == K_BREAK_ID) {
                Singleton.shared.breakfastMenu = dataResponse.response
            }
            Singleton.shared.lunchMessage = ""
            Singleton.shared.breakfastMessage = ""
            NotificationCenter.default.post(name: NSNotification.Name(N_MENU_DATA),object: nil)
            NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: N_KITCHEN_NOT_AVAILABLE)))
            NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil,userInfo: ["changeMenu":true])
            NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_TWO), object: nil,userInfo: ["changeMenu":true])
            NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_THREE), object: nil,userInfo: ["changeMenu":true])
            NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_FOUR), object: nil,userInfo: ["changeMenu":true])
        }
    }
    
    func getMyCart(completionHandler: @escaping (MyCartData) -> Void) {
        if(Singleton.shared.cartData.cart_data.count == 0){
            let currentCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String
            var baseUrl = String()
            if(currentCartId == nil || currentCartId == ""){
                return
            }else{
                baseUrl = U_BASE + U_GET_CART_DATA + (currentCartId ?? "")
            }
            SessionManager.shared.methodForApiCalling(url: baseUrl, method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_CART_DATA, userToken: nil) { (response) in
                Singleton.shared.cartData = response.response
                completionHandler(response.response)
            }
        }else {
            completionHandler(Singleton.shared.cartData)
        }
    }
}




