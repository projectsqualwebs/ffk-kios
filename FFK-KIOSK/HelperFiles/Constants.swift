//
//  Constants.swift
//  FFK-KIOSK
//
//  Created by qw on 23/12/20.

import UIKit
//MARK: COLOR CODE

let primaryColor = UIColor(red: 222/255, green: 45/255, blue: 36/255, alpha: 1) //#E30202//RED COLOR
let whiteColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.1) //#ffffff
let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.5) //#000000

//MARK: API CALLING URL
let U_BASE = "https://admin.farmersfreshkitchen.com/api/"
//var U_BASE = "https://v2api.farmersfreshkitchen.com/api/"
//var U_BASE = "https://beta-api.farmersfreshmeat.com/api/"
var U_IMAGE_BASE_2 = "https://v2api.farmersfreshkitchen.com/public/storage/"
var U_IMAGE_BASE =  "https://v2api.farmersfreshkitchen.com/public/storage/"

let U_APP_STORE_LINK = "https://itunes.apple.com/app/id=1486732896"
let U_GET_MENU = "get-menu/"
let U_GET_PREFERRED_RESTAURANT = "get-pref-restaurants"
let U_GET_SUB_CATEGORIES = "get-main-menu-categories/"
let U_GET_SUBCATEGORIES = "get-main-menu-modifiers/"
let U_ADD_TO_CART =  "kiosk/add-to-cart"
let U_PLACE_ORDER = "kiosk/place-order"
let U_UPDATE_ITEM_COUNT = "kiosk/update-quantity"
let U_GET_MENU_TYPE = "get-menu-type/"
let U_GET_CART_DATA = "get-my-cart?cart_id="
let U_GET_MENU_MEALS = "get-menu-type-meal/"
let U_REMOVE_CART = "remove-from-cart/"
let U_SELECT_PREFERENCE = "select-preference"
let U_CONFIRM_ORDER = "confirm-order"
let U_EMPTY_CART = "empty-cart"
let U_GET_RESTAURENTS = "get-restaurants"
let U_SEARCH_USER = "kiosk/user-search"
let U_GET_EDIT_CART_MODIFIER = "get-edit-cart-modifiers/"
let U_UPDATE_MY_CART = "update-my-cart"
let U_EMAIL_SUBSCRIBE = "email-subscribe"
let U_GET_SLIDER_IMAGE = U_BASE + "kiosk/slider-images?type="

//Square Payment
//LIVE
let K_SQUARE_APP_ID = "sq0idp-RvC4zckibDCNUU85Q-KAGQ"
let K_SQUARE_ACCESS_TOKEn = "EAAAEdsEkXAis7qr9i2Z51PiLRl3tQ_ss-onvBrgCa1D7ZErJ6FXF5MQVdhu7w6d"
let K_SQUARE_LOCATION_ID = "L41Q7BKZ0HJZJ"

let U_GUEST_CHECKOUT_BRAINTREE = "guest-checkout-v2"
let U_CONFIRM_ORDER_BRAINTREE = "confirm-order-v2"
let U_ADD_CARD_BRAINTREE = "create-customer-paymethod"


var K_LUNCH_ID = Int()
var K_BREAK_ID = Int()
var MENU_TYPE = Int()


//User Defaults
let UD_USER_DETAIl = "userDetail"
let UD_TOKEN = "user_token"
let UD_SELECTED_RESTAURANT = "selected_restaurant"
let UD_RESTAURANTS = "ffk_restaurants"
let UD_ADD_TO_CART = "addtocart"
let UD_BAG_COUNT = "bag_item"
let UD_EMPTY_CART = "UD_EMPTY_CART"
let UD_CARD_READER_CONNECTED =  "UD_CARD_READER_CONNECTED"

//Notifications
let N_REMOVE_ARROW = "remove_arrow"
var N_MENU_DATA = "reload_menu_data"
let N_UPDATE_BAG_COUNT = "N_UPDATE_BAG_COUNT"
let N_COLLECTION_INDEX_ONE = "N_COLLECTION_INDEX_ONE"
let N_COLLECTION_INDEX_TWO = "N_COLLECTION_INDEX_TWO"
let N_COLLECTION_INDEX_THREE = "N_COLLECTION_INDEX_THREE"
let N_COLLECTION_INDEX_FOUR = "N_COLLECTION_INDEX_FOUR"
let N_MANAGE_KITCHEN_OFFLINE = "N_MANAGE_KITCHEN_OFFLINE"

let N_RESET_COLLECTION_INDEX_ONE = "N_RESET_COLLECTION_INDEX_ONE"
let N_RESET_COLLECTION_INDEX_TWO = "N_RESET_COLLECTION_INDEX_TWO"
let N_RESET_COLLECTION_INDEX_THREE = "N_RESET_COLLECTION_INDEX_THREE"
let N_RESET_COLLECTION_INDEX_FOUR = "N_RESET_COLLECTION_INDEX_FOUR"

let N_SHOW_ERROR_MESSAGE = "N_SHOW_ERROR_MESSAGE"
let N_DISMISS_USER_DETAIL_VIEW = "N_DISMISS_USER_DETAIL_VIEW"
let N_DISMISS_SWIPE_CARD = "N_DISMISS_SWIPE_CARD"
let N_DISMISS_ORDER_TOTAL_VIEW = "N_DISMISS_ORDER_TOTAL_VIEW"
let N_START_FRESH_ORDER = "N_START_FRESH_ORDER"
let N_MY_CART_EMPTY_DATA = "N_MY_CART_EMPTY_DATA"
let N_KITCHEN_NOT_AVAILABLE = "N_KITCHEN_NOT_AVAILABLE"
let N_SHOW_HIDE_CATEGORY_STACK = "N_SHOW_HIDE_CATEGORY_STACK"
let N_RESET_ON_MENU_CHANGE = "N_RESET_ON_MENU_CHANGE"
let N_HANDLE_USER_INACTIVITY = "N_HANDLE_USER_INACTIVITY"
let N_DISMISS_ITEM_DETAIL_SCREEN = "N_DISMISS_ITEM_DETAIL_SCREEN"
let N_DISMISS_MYCART_SCREEN = "N_DISMISS_MYCART_SCREEN"

let N_HANDLE_FOREGROUND_BACKGROUD = "N_HANDLE_FOREGROUND_BACKGROUD"

//Check for cash
// app name
// bundle id
//Need to make changes on line 217-220 & 20-27(uncomment for test mode)  & comment 117-124
//Check for KIOSK live condition
//Check for remote config url
//Handle base url from firebae config for url
//Navigation controller timings
//Handle base url from firebae config for url

let K_ACTIVITY_POPUP_AUTODISMISS = 10 //Real: 30, Beta: 5
let K_ACTIVITY_POPUP_INTERVAL = 30  //Real: 90, Beta: 30
//This time indicates that when
let K_MAIN_TIMER_INTERVAL = 30.0  //Real: 60, Beata: 10
var K_IS_KIOSK_ONLINE = "1"
var K_IS_MENU_ITEM_CHANGED = false
var K_SHOW_OFFLINE_POPUP = false
