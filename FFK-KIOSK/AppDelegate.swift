//
//  AppDelegate.swift
//  FFK-KIOSK
//
//  Created by qw on 23/12/20.
//

import UIKit
import IQKeyboardManager
import SquareReaderSDK
import Firebase
import FirebaseDatabase

var ref: DatabaseReference!


@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DBHelper.shared.emptyLocalStorage()
        IQKeyboardManager.shared().isEnabled = true
        IQKeyboardManager.shared().isEnableAutoToolbar = false
        FirebaseApp.configure()
        ref = Database.database().reference()
        

        SQRDReaderSDK.initialize(applicationLaunchOptions: launchOptions)

        
        if let cart = UserDefaults.standard.value(forKey: UD_EMPTY_CART) as? String{
            if(cart == "true"){
                NavigationController.shared.emptyMyCart()
            }
        }
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
        UserDefaults.standard.setValue("true", forKey: UD_EMPTY_CART)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        UserDefaults.standard.setValue("true", forKey: UD_EMPTY_CART)
        exit(0)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        print("called")
    }
}

