//
//  PaymentOptionViewController.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 23/09/22.
//

import UIKit

protocol PaymentOption {
    func handlePaymentOption(type: Int)
}

class PaymentOptionViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var mainView: UIView!
    //  @IBOutlet weak var neverMindButton: CustomButton!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var yesButton: CustomButton!
    @IBOutlet weak var nevermindButton: CustomButton!
    
    
    var navigationDelegate: ResetView? = nil
    var cartItemDelegate: ClearCarItem? = nil
    var timer = Timer()
    var paymentDelegate: PaymentOption? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.mainView.alpha = 0
//            self.headingLabel.text = "Still thinking?"
//            self.subHeadingLabel.text = "Need more time or should we start a new order?"
//            self.yesButton.setTitle("Continue ordering", for: .normal)
//            self.nevermindButton.setTitle("Cancel order and clear cart", for: .normal)
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.popupView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        self.mainView.alpha = 0.5
       // self.neverMindButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 8.0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.popupView.isHidden = false
                        self?.popupView.transform = .identity
                        self?.mainView.alpha = 1
                //        self?.neverMindButton.isHidden = false
               //         self?.neverMindButton.transform = .identity
                        
                       },
                       completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        timer =  Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_AUTODISMISS, target: self, selector: #selector(resetView), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    @objc func resetView(){
        self.performExit()
    }
    
    func performNevermind(){
        self.mainView.alpha = 0.8
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut) {
            self.popupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.popupView.alpha  = 0
            self.mainView.alpha = 0
            
        } completion: { (val) in
            self.paymentDelegate?.handlePaymentOption(type: 2)
        }
    }
    
    func performExit(){
        self.navigationDelegate?.clearNavigationStack()
        self.mainView.alpha = 0.8
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut) {

            self.popupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.11)
            self.popupView.alpha  = 0
            self.mainView.alpha = 0
        } completion: { (val) in
            self.popupView.isHidden = true
            self.popupView.transform = .identity
            self.popupView.alpha  = 1
            self.mainView.alpha = 0
            self.paymentDelegate?.handlePaymentOption(type: 1)
        }
    }
    
    //MARK: IBActions
    @IBAction func exitAction(_ sender: Any) {
        self.performNevermind()
    }
    
    @IBAction func nevermindAction(_ sender: Any) {
        self.performExit()
    }
    
    @IBAction func crossAction(_ sender: Any) {
        self.paymentDelegate?.handlePaymentOption(type: 3)
    }
}
