//
//  ResetViewController.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 09/08/21.
//

import UIKit


class ResetViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var yesButton: CustomButton!
    
    
    var navigationDelegate: ResetView? = nil
    var cartItemDelegate: ClearCarItem? = nil
    var timer = Timer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.alpha = 0
        let bEnd = Calendar.current.date(bySettingHour: 10, minute: 30, second: 0, of: Date()) ?? Date()
        let lStart = Calendar.current.date(bySettingHour:11, minute:00, second: 0, of: Date()) ?? Date()
        if((bEnd.timeIntervalSince1970 <= Date().timeIntervalSince1970) && (lStart.timeIntervalSince1970 > Date().timeIntervalSince1970)){
            self.headingLabel.text = "Oops! Kiosk is offline!"
            self.subHeadingLabel.text = "Kiosk is currently closed and will open for lunch by 11AM."
        }else if(K_ACTIVITY_POPUP_AUTODISMISS == 10.0){
            self.headingLabel.text = "Oops! Kiosk is offline!"
            self.subHeadingLabel.text = "Kiosk is currently closed and will open for lunch by 11AM."
        }else{
            self.headingLabel.text = "Oops! Kiosk is offline!"
            self.subHeadingLabel.text = "Kiosk is closed for the day."
        }
        
        self.yesButton.setTitle("Understood, thanks.", for: .normal)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.popupView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        self.mainView.alpha = 0.5
        // self.neverMindButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 8.0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.popupView.isHidden = false
                        self?.popupView.transform = .identity
                        self?.mainView.alpha = 1
                        //        self?.neverMindButton.isHidden = false
                        //         self?.neverMindButton.transform = .identity
                        
                       },
                       completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        timer.invalidate()
        timer =  Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_AUTODISMISS, target: self, selector: #selector(resetView), userInfo: nil, repeats: false)
    }
    
    @objc func resetView(){
        self.exitAction(self)
    }
    
    //MARK: IBActions
    @IBAction func exitAction(_ sender: Any) {
        
        self.navigationDelegate?.clearNavigationStack()
        self.mainView.alpha = 0.8
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut) {
            
            self.popupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.11)
            self.popupView.alpha  = 0
            self.mainView.alpha = 0
            Singleton.shared.cartItemCount = 0
            NavigationController.shared.emptyMyCart()
            
        } completion: { (val) in
            self.popupView.isHidden = true
            self.popupView.transform = .identity
            self.popupView.alpha  = 1
            self.mainView.alpha = 0
            Singleton.shared.userDetail = SearchUserResponse()
            self.cartItemDelegate?.emptyCartData()
            Singleton.shared.cartItemCount = 0
            DBHelper.shared.emptyLocalStorage()
            self.clearNavigationStack()
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_SWIPE_CARD), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ITEM_DETAIL_SCREEN), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil)
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
}
