//
//  FoodItemViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 27/12/20.
//

import UIKit
//import RMPZoomTransitionAnimator_Swift
import PINRemoteImage
import Hero



var viewWidth = CGFloat()
var viewHeight = CGFloat()
var minX = CGFloat()
var minY = CGFloat()
var currentMenuItem = 1

class FoodItemViewController: UIViewController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate    {
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var indexpath = IndexPath()
    var currentIndex = Int()
    var width = Int()
    var height = Int()
    var menuData = [GetMenusList]()
    var isObserverAdded = false
    // static var swipeDelegate: SwipeGesture? = nil
    var itemIndex = 1
    var performCollectionAnimation = false
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        
        width = Int((collectionView.bounds.size.width/2)-5)
        height = Int((collectionView.bounds.size.height/2)-5)
        collectionView.layer.cornerRadius = 10
        //  WelcomePageViewController.index_delegate = self
        
        self.initData()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCollectionIndex), name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetCollectionIndex), name: NSNotification.Name(N_RESET_COLLECTION_INDEX_ONE), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.collectionView.reloadData()
    }
    
    func initData(){
        if(MENU_TYPE == K_LUNCH_ID){
            if(Singleton.shared.lunchMenu.count > 0){
                self.menuData = Singleton.shared.lunchMenu[0].menus
            }
        }else if(MENU_TYPE == K_BREAK_ID) {
            if(Singleton.shared.breakfastMenu.count > 0){
                self.menuData = Singleton.shared.breakfastMenu[0].menus
            }
        }
    }
    
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
            switch swipeGesture.direction {
            case .right:
                // FoodItemViewController.swipeDelegate?.handleSwipeGeture(direction: 2)
                NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil,userInfo: ["direction":"left"])
                break
            case .left:
                // FoodItemViewController.swipeDelegate?.handleSwipeGeture(direction: 1)
                NotificationCenter.default.post(name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil,userInfo: ["direction":"right"])
                break
            default:
                break
            }
        }
    }
    
    @objc func resetCollectionIndex(_ notif: NSNotification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_RESET_COLLECTION_INDEX_ONE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.resetCollectionIndex(_:)), name: NSNotification.Name(N_RESET_COLLECTION_INDEX_ONE), object: nil)
        self.itemIndex = 1
        collectionView?.contentInsetAdjustmentBehavior = .always
        collectionView.scrollToItem(at: IndexPath(row:0, section: 0), at: .right, animated: false)
    }
    
    @objc func handleCollectionIndex(_ notif: NSNotification){
        self.performCollectionAnimation = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCollectionIndex), name: NSNotification.Name(N_COLLECTION_INDEX_ONE), object: nil)
        
        if let changeMenu = notif.userInfo?["changeMenu"] as? Bool{
            // self.navigationController?.popViewController(animated: true)
            self.initData()
            return
        }
        
        if let direction = notif.userInfo?["direction"] as? String{
            if(direction == "left"){
                let count = self.menuData.count
                let a = getTotalIndex(count: count)
                if(itemIndex <= a && (itemIndex*4) <= count){
                    if(itemIndex == 1){
                        currentMenuItem = 4
                        NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["direction":"left"])
                    }else{
                        self.handleRight(collectionView: self.collectionView, itemIndex: self.itemIndex)
                        if(itemIndex > 1){
                            itemIndex -= 1
                        }
                    }
                }else {
                    if(itemIndex > 1){
                        self.handleRight(collectionView: self.collectionView, itemIndex: self.itemIndex)
                        if(itemIndex > 1){
                            itemIndex -= 1
                        }
                    }else {
                        currentMenuItem = 4
                        NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["direction":"left"])
                    }
                }
            }else {
                let count = self.menuData.count
                let a = self.getTotalIndex(count: count)
                if(itemIndex <= a && (itemIndex*4) <= count){
                    if(itemIndex == a){
                        currentMenuItem = 2
                        NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["direction":"right"])
                    }else {
                        self.handleLeft(collectionView: self.collectionView, itemIndex: self.itemIndex)
                        itemIndex += 1
                    }
                }else {
                    currentMenuItem = 2
                    NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["direction":"right"])
                }
            }
        }
    }
    
    //MARK: IBActions
    
}

extension FoodItemViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FoodItemCell", for: indexPath) as! FoodItemCell
        if(self.menuData.count > indexPath.row){
            let val = self.menuData[indexPath.row]
            if(self.view.frame.width < 1550){
                 cell.foodName.font = UIFont(name: "GothamRounded-Bold", size: 30)
                cell.foodDesc.font = UIFont(name: "GothamRounded-Book", size: 22)
            }
            cell.foodName.text = val.item_name
            cell.foodImage.heroID = "heroId \(indexPath.row)"
            //cell.foodImage.sd_setImage(with: URL(string:U_IMAGE_BASE + (val.ipad_image  ?? val.item_image ?? "")), placeholderImage: nil)
            cell.foodImage.pin_updateWithProgress = true
            cell.foodImage.pin_setImage(from: URL(string:U_IMAGE_BASE + (val.ipad_image  ?? val.item_image ?? "")),placeholderImage: (UIImage(named: "background_image")))
            let imageView = UIImageView()
            imageView.pin_setImage(from: URL(string:U_IMAGE_BASE + (val.ipad_image_single  ?? val.item_image ?? "")),placeholderImage: (UIImage(named: "background_image")))
            let price = Double(round(100*(val.item_price ?? 0)))/100
            cell.foodPrice.text = "$" + "\(price)".appendString(data: price)
            cell.foodDesc.text = val.item_description
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.size.width/2)-5, height: (collectionView.bounds.size.height/2)-5
        )
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":false])
        DispatchQueue.main.async { [self] in
            self.indexpath = indexPath
            self.navigationController?.delegate = self
            //self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailViewController") as! ItemDetailViewController
            if(self.currentIndex < Singleton.shared.lunchMenu.count){
                if(MENU_TYPE == K_LUNCH_ID){
                    myVC.mainMenuId = Singleton.shared.lunchMenu[self.currentIndex].menu_id ?? 0
                }else {
                    myVC.mainMenuId = Singleton.shared.breakfastMenu[self.currentIndex].menu_id ?? 0
                }
            }
            myVC.data = self.menuData[self.indexpath.row]
            myVC.heroId =  "heroId \(indexPath.row)"
            //self.transitioningDelegate = self
            //            self.navigationController?.pushViewController(myVC, animated: true)
            self.present(myVC, animated: true)
        }
        
    }
    
//    func navigationController(_ navigationController: UINavigationController,
//                              animationControllerFor operation: UINavigationController.Operation,
//                              from fromVC: UIViewController,
//                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        var destination = toVC
//        var source = fromVC
//        _ = self.storyboard?.instantiateViewController(withIdentifier: "FoodItemViewController") as! FoodItemViewController
//        let desteVC = self.storyboard?.instantiateViewController(withIdentifier: "ItemDetailViewController") as! ItemDetailViewController
//        if(toVC is ViewController ){
//            source = fromVC
//            destination = self
//        }
//
//        if(fromVC is ViewController){
//            source = self
//            destination = toVC
//        }
//
//        guard let sourceTransition =  source as? RMPZoomTransitionAnimating,
//              let destinationTransition = destination as? RMPZoomTransitionable else {
//            return nil
//        }
//
//        let animator = RMPZoomTransitionAnimator()
//
//        animator.goingForward = (operation == .push)
//        animator.sourceTransition = sourceTransition
//        animator.destinationTransition = destinationTransition
//        return animator;
//    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if(self.performCollectionAnimation){
            
            cell.layer.transform = CATransform3DMakeScale(0.9,0.9,1)
            UIView.animate(withDuration: 0.3, animations: {
                cell.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        }
    }
    
}


// MARK: - RMPZoomTransitionAnimating
//extension FoodItemViewController: RMPZoomTransitionAnimating {
//    var transitionDestinationImageViewFrame: CGRect {
//        guard let selectedIndexPath = self.collectionView.indexPathsForSelectedItems?.first else {
//            return CGRect()
//        }
//        let cell = self.collectionView.cellForItem(at: selectedIndexPath) as! FoodItemCell
//        //  let cellFrameInSuperview = cell.foodImage.convert(cell.foodImage.frame, to: self.collectionView.superview)
//        let cellFrameInSuperview = cell.foodImage.convert(cell.foodImage.frame, to: UIView(frame: CGRect(x: minX, y: minY, width: CGFloat(viewWidth), height:CGFloat(viewHeight))))
//
//        self.collectionView.reloadData()
//        return cellFrameInSuperview
//    }
//
//    var transitionSourceImageView: UIImageView {
//        guard let selectedIndexPath = self.collectionView.indexPathsForSelectedItems?.first else {
//            return UIImageView()
//        }
//        let cell = self.collectionView.cellForItem(at: selectedIndexPath) as! FoodItemCell
//        // self.menuData[indexPath.row]
//        let imageView = UIImageView()
//        imageView.pin_setImage(from: URL(string:U_IMAGE_BASE + (self.menuData[selectedIndexPath.row].ipad_image_single  ?? self.menuData[selectedIndexPath.row].item_image ?? "")),placeholderImage: (UIImage(named: "background_image")))
//        //  cell.itemImage.pin_setImage(from: URL(string:(U_IMAGE_BASE) + (val.item_image ?? "")), placeholderImage:(UIImage(named: "background_image")))
//        imageView.contentMode = cell.foodImage.contentMode;
//        imageView.clipsToBounds = true
//        imageView.isUserInteractionEnabled = false
//        imageView.layer.cornerRadius = 10
//        minX = cell.foodImage.frame.minX
//        minY = cell.foodImage.frame.minY
//        viewWidth = self.collectionView.bounds.size.width - 5
//        viewHeight = self.collectionView.bounds.size.height - 5
//
//        imageView.frame = cell.foodImage.convert(cell.foodImage.frame, to: self.collectionView.superview)
//        self.performCollectionAnimation = true
//        return imageView;
//    }
//
//    var transitionSourceBackgroundColor: UIColor? {
//        return .clear //self.collectionView.backgroundColor
//    }
//
//}


class FoodItemCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var foodImage: ImageView!
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var foodPrice: UILabel!
    @IBOutlet weak var foodDesc: UILabel!
}

