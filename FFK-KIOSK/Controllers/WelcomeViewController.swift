//
//  WelcomeViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 26/12/20.
//

import UIKit
import Spring
import Reachability
import PINRemoteImage

protocol ShowHideCategory {
    func showHideCategory(show:Bool)
}

class WelcomeViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var animateView: SpringView!
    @IBOutlet weak var bounceView: SpringView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var messageLabel: DesignableUILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    
    var images = [SliderImageResponse]()
    var timer = Timer()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    var index = 0
    let animationDuration: TimeInterval = 0.5
    let switchingInterval: TimeInterval = 4
    var transition = CATransition()
    let reachability = try! Reachability()
    var isFirstTime = true
    static var categoryViewDelegate:ShowHideCategory? = nil
    
    
    override func viewDidLayoutSubviews() {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        self.initializeView()
        self.imageView.pin_updateWithProgress = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(goToNextPage))

      //  self.setImage()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleKitchenAvailability), name: NSNotification.Name(N_KITCHEN_NOT_AVAILABLE), object: nil)
        RealtimeFirebase.shared.addMenuChangeObserver { val in
            self.images = []
            self.setImage()
        }
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: self.switchingInterval, target: self, selector: #selector(self.animateImageView), userInfo: nil, repeats: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.handleNetworkRechability()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.initializeView()
    }
    
    @objc func  handleKitchenAvailability(){
        if(Singleton.shared.breakfastMenu.count == 0 && Singleton.shared.lunchMenu.count == 0 || MENU_TYPE == 0){
            self.animateView.layer.removeAllAnimations()
            self.messageLabel.text = "Kiosk is currently closed"
            self.arrowImage.isHidden = true
            self.animateView.hideLoading()
            WelcomeViewController.categoryViewDelegate?.showHideCategory(show: true)
        }else {
            self.initializeView()
            self.messageLabel.text = "Tap a category to start your order"
            self.arrowImage.isHidden = false
            self.images = []
            self.setImage()
            WelcomeViewController.categoryViewDelegate?.showHideCategory(show: false)
        }
    }
    
    func handleNetworkRechability(){
        // Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        if(isFirstTime){
            self.isFirstTime = false
        }else {
            if(reachability.isReachable){
                self.animateView.layer.removeAllAnimations()
                self.messageLabel.text = "Kiosk is currently closed"
                self.arrowImage.isHidden = true
                self.animateView.hideLoading()
                WelcomeViewController.categoryViewDelegate?.showHideCategory(show: true)
            }else {
                self.initializeView()
                self.messageLabel.text = "Tap a category to start your order"
                self.arrowImage.isHidden = false
                WelcomeViewController.categoryViewDelegate?.showHideCategory(show: false)
            }
        }
    }
    
    func reachabilityChanged(_ isReachable: Bool) {
        print("called")
    }
    
    
    func initializeView(){
        self.animateView.autostart = true
        self.animateView.animation = "pop"
        self.animateView.delay = 1
        self.animateView.duration = 3
        self.animateView.damping = 0
        self.animateView.velocity = 0.01
        self.animateView.repeatCount = 999
        self.animateView.animate()
    }
    
    func setImage(){
        if (self.images.count > 0){
            if(self.images.count > index){
                imageView.layer.sublayers?.forEach { $0.removeAllAnimations() }
            imageView.pin_setImage(from: URL(string: U_IMAGE_BASE_2 + (self.images[index].path ?? ""))!)
                CATransaction.disableAnimations {
                    self.imageView.layer.removeAllAnimations()
                    CATransaction.flush()
                    animateImageView()
                    index += 1
                }
        }
        }else {
            index = 0
            SessionManager.shared.methodForApiCalling(url: U_GET_SLIDER_IMAGE + "\(MENU_TYPE == K_BREAK_ID ? "breakfast":"lunch")", method: .get, parameter: nil, objectClass: GetSliderImage.self, requestCode: U_GET_SLIDER_IMAGE, userToken: nil) { response in
                self.images = response.response
                if(self.images.count > self.index){
                self.imageView.pin_setImage(from: URL(string:U_IMAGE_BASE_2 + (self.images[self.index].path ?? ""))!)
                }
                self.imageView.layer.sublayers?.forEach { $0.removeAllAnimations() }
                CATransaction.disableAnimations {
                    self.imageView.layer.removeAllAnimations()
                    CATransaction.flush()
                    self.animateImageView()
                    self.index += 1
                }
            }
        }
    }
    
    @objc func goToNextPage() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let controller = mainStoryboard.instantiateViewController(withIdentifier: "AnimateCarouselViewController") as? AnimateCarouselViewController {
            present(controller, animated: true)
        }
    }
    
    @objc func animateImageView() {
        //CATransaction.begin()
        
        CATransaction.setAnimationDuration(animationDuration)
        transition.type = .fade
        transition.duration = 1
        //transition.subtype = CATransitionSubtype.fromRight
        
        /*
         transition.type = CATransitionType.fade
         transition.subtype = CATransitionSubtype.fromRight
         */
        
        imageView.layer.add(transition, forKey: kCATransition)
        if (images.count != 0 && images.count > index) {
            imageView.pin_setImage(from: URL(string: U_IMAGE_BASE_2 + (self.images[index].path ?? ""))!)
        }
        CATransaction.commit()
        index = index < images.count - 1 ? index + 1 : 0
    }
}

extension CATransaction {

    static func disableAnimations(_ completion: () -> Void) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        completion()
        CATransaction.commit()
    }

}
