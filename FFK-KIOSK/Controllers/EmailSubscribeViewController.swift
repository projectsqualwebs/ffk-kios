//
//  EmailSubscribeViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 08/01/21.
//

import UIKit
import Spring
import SkyFloatingLabelTextField
import NVActivityIndicatorView

protocol StartFresh{
    func startFreshOrder(playSound: Bool)
}
class EmailSubscribeViewController: UIViewController, UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == email){
            self.emailView.borderColor = .clear
        }
        return true
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var tickImage: SpringImageView!
    @IBOutlet weak var viewBottom: NSLayoutConstraint!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var emailView: View!
    @IBOutlet weak var doneButton: CustomButton!
    @IBOutlet weak var doneIndicatorView: NVActivityIndicatorView!
    @IBOutlet weak var subscribeLabel: UILabel!
    
    @IBOutlet weak var row1: UIView!
    @IBOutlet weak var row2: UIView!
    @IBOutlet weak var row3: UIView!
    @IBOutlet weak var row4: UIView!
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    var capsLockOn = false
    static var startFreshDelegate: StartFresh? = nil
    var timer: Timer?
    var orderId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.view.frame.width < 1550){
            self.subscribeLabel.font = UIFont(name: "GothamRounded-Book", size: 18)
        }
        self.email.becomeFirstResponder()
        self.doneButton.layer.cornerRadius = 10
        self.emailView.borderColor = .black
        changeCaps(containerView: row1)
        changeCaps(containerView: row2)
        changeCaps(containerView: row3)
        changeCaps(containerView: row4)
        self.email.delegate = self
        self.email.titleFormatter = {$0}
        self.email.placeholderFont = UIFont(name: "Poppins-Medium", size: 26)
        self.email.layer.cornerRadius = 10
        self.email.text = Singleton.shared.userDetail.email ?? ""
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    func navigateToDashboard(){
        Singleton.shared.cartItemCount = 0
        self.clearNavigationStack()
        Singleton.shared.userDetail = SearchUserResponse()
        NavigationController.shared.emptyMyCart()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
        DispatchQueue.main.async {
            EmailSubscribeViewController.startFreshDelegate?.startFreshOrder(playSound: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_AUTODISMISS, target: self, selector: #selector(self.hideControls), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    @objc func hideControls(){
        self.navigateToDashboard()
    }
    
    //MARK: IBActions
    @IBAction func doneAction(_ sender: Any) {
        if(self.email.text!.isEmpty){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.emailView.center.x - 10, y: self.emailView.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.emailView.center.x + 10, y: self.emailView.center.y))
            self.emailView.layer.add(animation, forKey: "position")
            self.emailView.borderColor = primaryColor
        }else if !(self.isValidEmail(emailStr: self.email.text ?? "")){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.emailView.center.x - 10, y: self.emailView.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.emailView.center.x + 10, y: self.emailView.center.y))
            self.emailView.layer.add(animation, forKey: "position")
            self.emailView.borderColor = primaryColor
        }else {
            self.doneButton.alpha = 0.9
            self.doneButton.isUserInteractionEnabled = false
            self.doneIndicatorView.startAnimating()
            self.doneButton.setTitleColor(.clear, for: .normal)
            var mobile = (Singleton.shared.userDetail.mobile ?? "").replacingOccurrences(of: " ", with: "")
            mobile = mobile.replacingOccurrences(of: "-", with: "")
            mobile = mobile.replacingOccurrences(of: "(", with: "")
            mobile = mobile.replacingOccurrences(of: ")", with: "")
            let param:[String:Any] = [
                "email":self.email.text ?? "",
                "mobile_number":mobile,
                "restaurant_id":Singleton.shared.selectedLocation.id ?? 0,
                "order_id":self.orderId ?? 0,
                "email_subscribed": self.tickImage.image == nil ? 0:1
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_EMAIL_SUBSCRIBE, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_EMAIL_SUBSCRIBE, userToken: nil) { (response) in
                self.doneButton.alpha = 1
                self.doneButton.isUserInteractionEnabled = true
                self.doneIndicatorView.stopAnimating()
                self.doneButton.setTitleColor(.white, for: .normal)
                self.navigateToDashboard()
            }
        }
    }
    
    @IBAction func skipAction(_ sender: Any) {
        self.navigateToDashboard()
    }
    
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func tickAction(_ sender: Any) {
        if(self.tickImage.image == nil){
            self.tickImage.image = #imageLiteral(resourceName: "tick_image")
            self.tickImage.duration = 0.1
            self.tickImage.damping = 20
            self.tickImage.velocity = 4
            self.tickImage.animation = "pop"
            self.tickImage.animate()
        }else {
            self.tickImage.image = nil
            self.tickImage.duration = 0.1
            self.tickImage.damping = 20
            self.tickImage.velocity = 4
            self.tickImage.animation = "pop"
            self.tickImage.animate()
        }
    }
    
    @IBAction func capsLockPressed(button: UIButton) {
        capsLockOn = !capsLockOn
        changeCaps(containerView: row1)
        changeCaps(containerView: row2)
        changeCaps(containerView: row3)
        changeCaps(containerView: row4)
    }
    
    @IBAction func keyPressed(button: UIButton) {
        var string = button.titleLabel!.text  ?? ""
        self.emailView.borderColor = .black
        if(capsLockOn){
            self.email.text = (self.email.text ?? "") + string.uppercased()
        }else{
            self.email.text = (self.email.text ?? "") + string.lowercased()
        }
        UIView.animate(withDuration: 0.2, animations: {
            button.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
            
        }, completion: {(_) -> Void in
            button.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        })
    }
    
    @IBAction func backSpacePressed(button: UIButton) {
        if(self.email.text!.count > 0){
            self.email.text?.removeLast()
        }
    }
    
    @IBAction func spacePressed(button: UIButton) {
      self.email.text = (self.email.text ?? "") + " "
    }
    
    func changeCaps(containerView: UIView) {
        for view in containerView.subviews {
            for buttona in view.subviews {
                for buttonb in buttona.subviews{
                    for buttonc in buttonb.subviews{
                    if let button = buttonc as? UILabel{
                        let buttonTitle = button.text
                        if capsLockOn {
                            let text = buttonTitle!.uppercased()
                            button.text = text
                        } else {
                            let text = buttonTitle!.lowercased()
                            button.text = text
                        }
                    }
                }
                }
            }
        }
    }
}
