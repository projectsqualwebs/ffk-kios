//
//  ItemDetailViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 29/12/20.
//

import UIKit
//import RMPZoomTransitionAnimator_Swift
import Spring
import CountableLabel
import NVActivityIndicatorView
import PINRemoteImage
import AVFoundation
import Hero

protocol HandleCart{
    func handleCartItemCount()
}


class ItemDetailViewController: UIViewController, UINavigationControllerDelegate {
//    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        let animator = RMPZoomTransitionAnimator()
//        animator.goingForward = (operation == .push)
//        animator.sourceTransition = fromVC as? RMPZoomTransitionAnimating
//        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "FoodItemViewController") as! FoodItemViewController
//        animator.destinationTransition = myVC
//        return animator
//    }
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainView: View!
    @IBOutlet weak var itemDescription: DesignableUILabel!
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var backButton: View!
    
    @IBOutlet weak var bounceView: UIView!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    
    
    
    @IBOutlet weak var addButton: RapidFireButton!
    @IBOutlet weak var customizeButton: RapidFireButton!
    
    @IBOutlet weak var itemCountLabel: CountableLabel!
    @IBOutlet weak var springView: SpringView!
    @IBOutlet weak var singleItemView: UIView!
    @IBOutlet weak var addButtonSpinnerView: NVActivityIndicatorView!
    @IBOutlet weak var customButtonSpinnerView: NVActivityIndicatorView!
    @IBOutlet weak var backGrayView: View!
    @IBOutlet weak var categoryDetails: UIStackView!
    
    
    
    static var cartDelegate:HandleCart? = nil
    var data = GetMenusList()
    var submenuData = [SubMenuResponse]()
    var jsonData:Any?
    var mainMenuId = Int()
    var isSingleItem = false
    var isDollarSelected = false
    var audioPlayer = AVAudioPlayer()
    var heroId: String = ""
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_DISMISS_ITEM_DETAIL_SCREEN), object: nil)
    }
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.imageView.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.data.ipad_image_single ?? data.item_image ?? "")))
        self.imageView.heroID = heroId
        self.hero.isEnabled = true
        if(self.view.frame.width < 1550){
            self.itemName.font = UIFont(name: "GothamRounded-Bold", size: 30)
            self.itemDescription.font = UIFont(name: "GothamRounded-Book", size: 22)
            self.headingLabel.font = UIFont(name: "GothamRounded-Bold", size: 32)
        }
        
        
        self.itemCountLabel.animationType = .none

        self.itemName.text = data.item_name
        if(data.modifiers == 0){
            self.singleItemView.isHidden = false
            let price = Double(round(100*(data.item_price ?? 0)))/100
            self.customizeButton.setTitle("Add $" + "\(price)".appendString(data: price), for: .normal)
            self.itemDescription.isHidden = false
        }else {
            self.singleItemView.isHidden = true
            self.customizeButton.setTitle("Customize", for: .normal)
            self.itemDescription.isHidden = false
            self.getSubmenuItems()
        }
        self.itemCountLabel.text = "1"
        self.headingLabel.text = "Customize " + (data.item_name ?? "")
        self.itemDescription.text = data.item_description
        
        let sound = Bundle.main.path(forResource: "notifications_20", ofType: "mp3")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!), fileTypeHint: "mp3")
        }catch{
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.addButton.layer.cornerRadius = 10
        self.customizeButton.layer.cornerRadius = 10
       
        NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["showIndicator":false])
        self.categoryDetails.animShow()
        self.customizeButton.animShow()
        self.addButton.setTitle( "Add $" + "\(round(100*(self.data.item_price ?? 0))/100)".appendString(data: (self.data.item_price ?? 0)), for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissView), name: NSNotification.Name(N_DISMISS_ITEM_DETAIL_SCREEN), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.2) {
            self.view.bringSubviewToFront(self.backButton)
            self.backGrayView.transform = CGAffineTransform(translationX: 10, y: 30)
            self.backButton.transform = CGAffineTransform(translationX: 10, y: 30)
           // self.view.bringSubviewToFront(self.backGrayView)
        }
    }
    
    @objc func dismissView(){
        self.dismiss(animated: true)
        //self.navigationController?.popViewController(animated: false)
        NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["showIndicator":false])
    }
    
    func getSubmenuItems(){
        self.customButtonSpinnerView.isHidden = false
        self.customButtonSpinnerView.startAnimating()
        self.customizeButton.setTitleColor(.clear, for: .normal)
        customizeButton.alpha = 0.9
        customizeButton.isUserInteractionEnabled = false
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SUBCATEGORIES + "\(data.item_id ?? 0)/\(Singleton.shared.selectedLocation.id ?? 0)", method: .get, parameter: nil, objectClass: GetSubmenu.self, requestCode: U_GET_SUBCATEGORIES, userToken: nil) { (response) in
            let data = response.response
            
            for val in data {
                var sectionCount = val
                sectionCount.sectionItemCount = 0
                for check in sectionCount.meals {
                    var tick = check
                    tick.isSelected = 0
                    tick.itemCount = 0
                }
                self.submenuData.append(sectionCount)
            }
            self.customButtonSpinnerView.stopAnimating()
            self.customizeButton.setTitleColor(.white, for: .normal)
            self.customizeButton.alpha = 1
            self.customizeButton.isUserInteractionEnabled = true
            self.menuTable.reloadData()
        }
    }
    
    func handleSelectedValue(){
        var selectedItems:SubMenuResponse?
        for val in self.submenuData{
            switch val.is_rule {
            case 1:
                if (val.sectionItemCount! != val.item_exactly!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                  //  myVC.heading = "Must select \(val.item_exactly ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"

                    self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            // let items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems?.meals.append(data)
                            //selectedItems = items
                        }
                    }
                }
                break
            case 2:
                if !(val.sectionItemCount! >= val.item_range_from! && val.sectionItemCount! <= val.item_range_to!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                   // myVC.heading = "Must select \(val.item_range_from ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"

                    self.navigationController?.present(myVC, animated: false, completion: nil)
                    
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            // let items = AddToCart(modifier_id: "\(val.modifier_group_id ?? 0)", modifier_item_id: "\(data.id ?? 0)", modifier_item_count: "\(data.itemCount ?? 0)")
                            //selectedItems.append(items)
                            //selectedItems = items
                            selectedItems?.meals.append(data)
                        }
                    }
                }
                break
            case 3:
                if (val.sectionItemCount! != val.item_maximum!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                  //  myVC.heading = "Must select \(val.item_maximum ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"

                    self.navigationController?.present(myVC, animated: false, completion: nil)
                    
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            
                            selectedItems?.meals.append(data)
                        }
                    }
                }
                break
            default:
                break
            }
        }
        
        if(selectedItems?.meals.count  != 0 ){
            DBHelper.shared.addItemToCart(mainMenuId: self.mainMenuId,menuList: self.data, subcategory: submenuData)
            Singleton.shared.cartItemCount = (Singleton.shared.cartItemCount) + (Int(self.itemCountLabel.text ?? "1")!)
            UserDefaults.standard.setValue( Singleton.shared.cartItemCount ?? 0, forKey: UD_BAG_COUNT)
            // Singleton.shared.cartData = MyCartData()
            //UserDefaults.standard.set(response.response, forKey: UD_ADD_TO_CART)
            
            self.bounceView.isHidden = true
            
            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":true])
            ItemDetailViewController.cartDelegate?.handleCartItemCount()
            //self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true)
        }
    }
    
    
    func cardHolderName() {
        
        var param = [String:Any]()
        if let id =  UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String {
            param = [
                "menu_id":self.mainMenuId,
                "menu": self.jsonData,
                "cart_id": id,
                "item_id":data.item_id,
                "item_count": self.itemCountLabel.text ?? ""
            ]
        }else {
            param = [
                "menu_id":self.mainMenuId,
                "menu": self.jsonData,
                "item_id":data.item_id,
                "item_count": self.itemCountLabel.text ?? ""
            ]
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_TO_CART, method: .post, parameter: param, objectClass: AddToCartResponse.self, requestCode: U_ADD_TO_CART, userToken: nil) { (response) in
            Singleton.shared.cartItemCount = (Singleton.shared.cartItemCount ?? 0) + (Int(self.itemCountLabel.text ?? "1")!)
            UserDefaults.standard.setValue( Singleton.shared.cartItemCount ?? 0, forKey: UD_BAG_COUNT)
            //Singleton.shared.cartData = MyCartData()
            UserDefaults.standard.set(response.response, forKey: UD_ADD_TO_CART)
            //  self.cartDelegate?.handleCartItemCount()
            
            
            self.bounceView.isHidden = true
            
            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":true])
            ItemDetailViewController.cartDelegate?.handleCartItemCount()
            //self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true)
        }
    }
    
    //MARK: IBActions
    @IBAction func minusAction(_ sender: Any) {
        var count = Int(self.itemCountLabel.text ?? "")!
        if(count > 1){
            count -= 1
        }
        if(count == 1){
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear) {
                self.springView.animation = "squeezeUp"
                self.springView.duration = 0.5
                self.springView.animate()
                self.springView.alpha = 0
            } completion: { (val) in
                self.springView.isHidden = true
                self.springView.alpha = 1
            }
            
        }
        self.itemCountLabel.animationType = .pushDown
        self.itemCountLabel.text = "\(count)"
        let amount = Double(count)*(data.item_price ?? 0)
        if(data.modifiers == 0){
            self.customizeButton.setTitle( "Add $" + "\(round(100*amount)/100)".appendString(data: amount), for: .normal)
        }else {
            self.addButton.setTitle( "Add $" + "\(round(100*amount)/100))".appendString(data: amount), for: .normal)
        }
    }
    
    @IBAction func plusAction(_ sender: Any) {
        var count = Int(self.itemCountLabel.text ?? "")!
        if(count == 1){
            self.springView.animation = "squeezeDown"
            self.springView.duration = 0.5
            self.springView.animate()
            self.springView.isHidden = false
        }
        count += 1
        self.itemCountLabel.animationType = .pushUp
        self.itemCountLabel.text = "\(count)"
        if self.audioPlayer.isPlaying {
            self.audioPlayer.pause()
        }
        self.audioPlayer.currentTime = 0
        self.audioPlayer.play()
        if(data.modifiers == 0){
            let number = Double(count)*(data.item_price ?? 0)
            self.customizeButton.setTitle( "Add $" + "\(round(100*number)/100)".appendString(data: number), for: .normal)
        }else {
            let number = Double(count)*(data.item_price ?? 0)
            self.addButton.setTitle( "Add $" + "\(round(100*number)/100)".appendString(data: number), for: .normal)
        }
    }
    
    @IBAction func addToBag(_ sender: Any) {
        self.handleSelectedValue()
        
    }
    
    
    @IBAction func didTapClose(_ sender: UIButton) {
        UIView.animate(withDuration: 0.27, animations: {
            self.bounceView.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.bounceView.alpha = 0.0
        }) { _ in
            
            self.bounceView.isHidden = true
            self.bounceView.alpha  = 1
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
        }
    }
    
    
    @IBAction func exitAction(_ sender: Any) {
        self.pushHome(controller: self)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.customizeButton.alpha = 0.8
        self.customizeButton.alpha = 0.8
        UIView.animate(withDuration: 0.24, delay: 0, options: .curveEaseOut) {
            self.customizeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.customizeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.customizeButton.alpha = 0
            self.customizeButton.alpha = 0
        } completion: { (val) in
            self.customizeButton.isHidden = true
            self.customizeButton.isHidden = true
            self.customizeButton.transform = .identity
            self.customizeButton.transform = .identity
            self.customizeButton.alpha = 1
            self.customizeButton.alpha = 1
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(20)) {
            NotificationCenter.default.post(name: NSNotification.Name(N_REMOVE_ARROW), object: nil,userInfo: ["showIndicator":true])
            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":true])
            //self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true)
        }
        
    }
    
    @IBAction func customizeAction(_ sender: Any) {
        if(data.modifiers == 0){
            
            var menudata = self.data
            menudata.item_count = Int(self.itemCountLabel.text ?? "1")!
            DBHelper.shared.addItemToCart(mainMenuId: self.mainMenuId,menuList: menudata, subcategory: [SubMenuResponse]())
            
            Singleton.shared.cartItemCount = (Singleton.shared.cartItemCount ?? 0) + Int(self.itemCountLabel.text ?? "1")!
            UserDefaults.standard.setValue( Singleton.shared.cartItemCount ?? 0, forKey: UD_BAG_COUNT)
            Singleton.shared.cartData = MyCartData()
            
            self.bounceView.isHidden = true
            (sender as? UIButton)?.alpha = 1
            (sender as? UIButton)?.isUserInteractionEnabled = true
            ItemDetailViewController.cartDelegate?.handleCartItemCount()
            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":true])
            //self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true)
        }else {
            NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
            self.bounceView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            UIView.animate(withDuration: 0.4,
                           delay: 0,
                           usingSpringWithDamping: 0.6,
                           initialSpringVelocity: 8.0,
                           options: .curveEaseInOut,
                           animations: { [weak self] in
                            self?.bounceView.isHidden = false
                            self?.bounceView.transform = .identity
                           },
                           completion: nil)
        }
    }
}

//extension ItemDetailViewController: RMPZoomTransitionAnimating {
//    var transitionSourceImageView: UIImageView {
//        let imageView = UIImageView(image: self.imageView.image)
//
//        imageView.contentMode = self.imageView.contentMode
//        imageView.clipsToBounds = true
//        imageView.isUserInteractionEnabled = false
//        imageView.frame = self.imageView.frame
//        imageView.layer.cornerRadius = 10
//        return imageView;
//
//    }
//
//    var transitionSourceBackgroundColor: UIColor? {
//        return view.backgroundColor
//    }
//
//    var transitionDestinationImageViewFrame: CGRect {
//        return CGRect(x: self.mainView.frame.minX+10, y: 70, width: self.imageView.frame.width, height: self.imageView.frame.height+25)
//    }
//}
//
//extension ItemDetailViewController: RMPZoomTransitionDelegate {
//    func zoomTransitionAnimator(animator: RMPZoomTransitionAnimator,
//                                didCompleteTransition didComplete: Bool,
//                                animatingSourceImageView imageView: UIImageView) {
//        //  self.imageView.sd_setImage(with: URL(string: U_IMAGE_BASE + (self.data.ipad_image_single ?? data.item_image ?? "")), placeholderImage:nil)
//        self.imageView.pin_setImage(from: URL(string: U_IMAGE_BASE + (self.data.ipad_image_single ?? data.item_image ?? "")))
//        
//    }
//}

//extension ItemDetailViewController: UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 3
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTableView") as! SelectionTableView
//        cell.headignLabel.text = self.section[indexPath.row]
//        cell.currentIndex = indexPath.row

//        return cell
//     }
//}

extension ItemDetailViewController : UITableViewDelegate,UITableViewDataSource, SelectionProtocol{
    
    func singleSelection(currentIndex: Int, collectionIndex: Int) {
        switch (self.submenuData[currentIndex].is_rule ?? 0) {
        case 1:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_exactly ?? 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    let name = self.submenuData[currentIndex].meals[collectionIndex]
                    if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected == false){
                        self.isDollarSelected = true
                        self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)+1))/100)".appendString(data: ((self.data.item_price ?? 0)+1)), for: .normal)
                    }else if(name.item_name?.lowercased() == "fried") {
                        self.isDollarSelected = false
                        self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)))/100)".appendString(data: ((self.data.item_price ?? 0))), for: .normal)
                    }
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section:0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                    }
                    UITableView.performWithoutAnimation {
                        self.menuTable.reloadRows(at:[IndexPath(row: currentIndex, section: 0)], with: .none)
                    }
                }else {
                    if(self.submenuData[currentIndex].item_exactly == 1 && self.submenuData.count == 2){
                        return
                    }
                    if(self.submenuData[currentIndex].item_exactly == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].is_checked = 0
                            meal[val].itemCount = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.menuTable.reloadRows(at:[IndexPath(row: currentIndex, section: 0)], with: .none)
                        let name = self.submenuData[currentIndex].meals[collectionIndex]
                        if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected == false){
                            self.isDollarSelected = true
                            self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)+1))/100)".appendString(data: (self.data.item_price ?? 0)+1), for: .normal)
                        }else if(name.item_name?.lowercased() == "fried"){
                            self.isDollarSelected = false
                            self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)))/100)".appendString(data: ((self.data.item_price ?? 0))), for: .normal)
                        }
                        return
                    }
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_exactly ?? 0) item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        if(self.submenuData[currentIndex].meals[0].isSelected == 1){
                            self.submenuData[currentIndex].meals[1].isSelected = 1
                            self.submenuData[currentIndex].meals[1].is_checked = 1
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                        }else {
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.submenuData[currentIndex].meals[0].isSelected = 1
                            self.submenuData[currentIndex].meals[0].is_checked = 1
                        }
                    }else {
                        if(self.submenuData[currentIndex].meals[1].isSelected == 1){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.submenuData[currentIndex].meals[0].isSelected = 1
                            self.submenuData[currentIndex].meals[0].is_checked = 1
                        }else {
                            self.submenuData[currentIndex].meals[1].isSelected = 1
                            self.submenuData[currentIndex].meals[1].is_checked = 1
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                        }
                    }
                    UITableView.performWithoutAnimation {
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }
                }else{
                    
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    let name = self.submenuData[currentIndex].meals[collectionIndex]
                    if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected){
                        self.isDollarSelected = false
                        self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)))/100)".appendString(data: ((self.data.item_price ?? 0))), for: .normal)
                    }else if(name.item_name?.lowercased() == "fried"){
                        self.isDollarSelected = false
                        self.addButton.setTitle( "Add $" + "\(round(100*((self.data.item_price ?? 0)))/100)".appendString(data: ((self.data.item_price ?? 0))), for: .normal)
                    }
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                UITableView.performWithoutAnimation {
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                }
            }
            break
        case 2:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if((((self.submenuData[currentIndex].sectionItemCount ?? 0) >= (self.submenuData[currentIndex].item_range_from ?? 0)) && ((self.submenuData[currentIndex].sectionItemCount ?? 0) < (self.submenuData[currentIndex].item_range_to ?? 0)) || self.submenuData[currentIndex].sectionItemCount == 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                    }
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    
                }else {
                    if(self.submenuData[currentIndex].item_range_to == 1 && self.submenuData.count == 2){
                        return
                    }
                    
                    if(self.submenuData[currentIndex].item_range_to == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].is_checked = 0
                            meal[val].itemCount = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        return
                    }
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_range_to ?? 0) item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        self.submenuData[currentIndex].meals[1].isSelected = 1
                        self.submenuData[currentIndex].meals[1].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }else {
                        self.submenuData[currentIndex].meals[0].isSelected = 1
                        self.submenuData[currentIndex].meals[0].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                }else{
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = ( self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                UITableView.performWithoutAnimation {
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                }
            }
            break
        case 3:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_maximum ?? 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                        
                    }
                    self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                }else {
                    if(self.submenuData[currentIndex].item_maximum == 1 && self.submenuData.count == 2){
                        return
                    }
                    
                    if(self.submenuData[currentIndex].item_maximum == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].itemCount = 0
                            meal[val].is_checked = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        return
                    }
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_maximum ?? 0) item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        self.submenuData[currentIndex].meals[1].isSelected = 1
                        self.submenuData[currentIndex].meals[1].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }else {
                        self.submenuData[currentIndex].meals[0].isSelected = 1
                        self.submenuData[currentIndex].meals[0].isSelected = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                }else{
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = ( self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                UITableView.performWithoutAnimation {
                    self.menuTable.reloadRows(at: [(NSIndexPath(row:currentIndex, section: 0) as IndexPath)], with: .none)
                }
            }
            
            break
        default:
            break
        }
    }
    
    func plusFunction(currentIndex: Int, collectionIndex: Int) {
        
        switch (self.submenuData[currentIndex].is_rule ?? 0) {
        case 1:
            if((self.submenuData[currentIndex].sectionItemCount ?? 0) < (self.submenuData[currentIndex].item_exactly ?? 0)){
                if(((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!) && !(((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) == 0) && ((self.submenuData[currentIndex].sectionItemCount ?? 0) == (self.submenuData[currentIndex].item_exactly ?? 0)))){
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                    
                    //if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 1){
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    // }
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                }else {
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                    
                }
                
            }else {
                
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_exactly ?? 0) item(s)"
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }
            break
        case 2:
            if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_range_to!) || (self.submenuData[currentIndex].sectionItemCount == 0) || (((self.submenuData[currentIndex].sectionItemCount ?? 0) == self.submenuData[currentIndex].item_range_to!) && ((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!) && ((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0))){
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!){
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                }else {
                    
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }else {
                if((self.submenuData[currentIndex].single_item_maximum ?? 0 ) == self.submenuData[currentIndex].meals[collectionIndex].itemCount){
                    
                    self.manageCartButton()
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].single_item_maximum ?? 0) item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                    
                }else {
                   
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_range_to ?? 0) item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }
            break
        case 3:
            if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < (self.submenuData[currentIndex].item_maximum ?? 0)){
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!){
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                }else {
                   
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more item(s)"
                    self.navigationController?.present(myVC, animated: false, completion: nil)
                }
            }else {
             
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                myVC.modalPresentationStyle = .overFullScreen
                myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_maximum!) item(s)"
                self.navigationController?.present(myVC, animated: false, completion: nil)
                
            }
            break
        default:
            break
        }
        
    }
    
    func minusFunction(currentIndex: Int, collectionIndex: Int) {
        switch (self.submenuData[currentIndex].is_rule!) {
        case 1:
            if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                
                self.submenuData[currentIndex].meals[collectionIndex].itemCount = ( (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1)
                if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                }
                if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                
                //  cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
            }
            break
        case 2:
            if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1
                
                if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                }
                if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                
                //  cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
                
            }
            break
        case 3:
            if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1
                if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                    self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                }
                if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                
                //   cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
            }
            break
        default:
            break
        }
        //        if(cell.lblNumberofItems.text == "0"){
        //            cell.backVIewCount.backgroundColor = backgroundColor
        //        }else {
        //            cell.backVIewCount.backgroundColor = primaryColor
        //        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.submenuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionData = self.submenuData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTableView") as! SelectionTableView
        if(self.view.frame.width < 1550){
            self.itemName.font = UIFont(name: "GothamRounded-Bold", size: 22)
        }
        cell.currentIndex = indexPath.row
        cell.selectionDelegate = self
        cell.submenuData = self.submenuData[indexPath.row].meals
        cell.sectionData = self.submenuData
        let operation = (sectionData.meals.count/2) + (sectionData.meals.count%2)
        cell.collectionViewHeight.constant = CGFloat(operation*85) + 20
        //        if((operation*85) == 0){
        //            cell.collectionViewHeight.constant = CGFloat(85) + 20
        //        }else{
        //            cell.collectionViewHeight.constant = CGFloat((sectionData.meals.count/2)*85) + 20
        //        }
        cell.collectionView.reloadData()
        let attrs1 = [NSAttributedString.Key.font :  UIFont(name: "GothamRounded-Book", size: 22), NSAttributedString.Key.foregroundColor : UIColor.gray]
        
        let attrs2 = [NSAttributedString.Key.font :  UIFont(name: "GothamRounded-Medium", size: 25), NSAttributedString.Key.foregroundColor : UIColor.black]
        
        
        if(sectionData.is_rule == 1){
            if((sectionData.item_exactly ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
                
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (select \(sectionData.item_exactly ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
            }
        }else if(sectionData.is_rule == 2){
            if((sectionData.item_range_to ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (choose upto \(sectionData.item_range_to ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
            }
        }else{
            if((sectionData.item_maximum ?? 0) == 1){
                let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
            }else {
                let attributedString1 = NSMutableAttributedString(string:" (select \(sectionData.single_item_maximum ?? 0))", attributes:attrs1)
                
                let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                
                attributedString2.append(attributedString1)
                cell.headignLabel.attributedText = attributedString2
            }
        }
        return cell
    }
    
    func manageCartButton(){
        var selectedItems = [AddToCart]()
        for val in self.submenuData{
            switch val.is_rule {
            case 1:
                if (val.sectionItemCount! != val.item_exactly!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    //  self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            //selectedItems = items
                        }
                    }
                }
                break
            case 2:
                if !(val.sectionItemCount! >= val.item_range_from! && val.sectionItemCount! <= val.item_range_to!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    // self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id ?? 0)", modifier_item_id: "\(data.id ?? 0)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            
                        }
                    }
                }
                break
            case 3:
                if (val.sectionItemCount! != (val.item_maximum ?? 0)){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    //  self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id!)", modifier_item_id: "\(data.id!)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                            
                        }
                    }
                }
                break
            default:
                break
            }
        }
        
    }
    
}

protocol SelectionProtocol{
    func singleSelection(currentIndex:Int,collectionIndex: Int)
    func plusFunction(currentIndex:Int,collectionIndex: Int)
    func minusFunction(currentIndex:Int,collectionIndex: Int)
}


class SelectionTableView: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK: IBOutlets
    @IBOutlet weak var headignLabel: DesignableUILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }
    
    var submenuData = [SubmenuSubCategory]()
    var sectionData = [SubMenuResponse]()
    var selectionDelegate: SelectionProtocol? = nil
    var currentIndex = Int()
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.submenuData.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectionCell", for: indexPath) as! SelectionCell
        cell.itemCount.textColor = .white
        var val = self.submenuData[indexPath.row]
        if(UIScreen.main.bounds.size.width < 1550){
            cell.itemName.font = UIFont(name: "GothamRounded-Bold", size: 22)
        }
        cell.tickImage.layer.cornerRadius = 27.5
        if(val.item_name?.lowercased() == "smothered" || val.item_name?.lowercased() == "baked"){
            let stringText = (val.item_name ?? "")
            let attributedText = NSMutableAttributedString(string: stringText, attributes: [NSAttributedString.Key.font: UIFont(name: "GothamRounded-Bold", size: 32)])
            attributedText.append(NSAttributedString(string: " (+$1)", attributes: [NSAttributedString.Key.font:  UIFont(name: "GothamRounded-Book", size: 22), NSAttributedString.Key.foregroundColor: UIColor.black]))
            cell.itemName.attributedText = attributedText
        }else {
            cell.itemName.text = (val.item_name ?? "")
        }
        if ((sectionData[currentIndex].single_item_maximum ?? 0) == 1 || (sectionData[currentIndex].single_item_maximum ?? 0) == 0 ){
            cell.tickView.isHidden = false
            cell.incrementView.isHidden = true
            cell.incrementView.layer.cornerRadius = 27.5
        }else if(val.isSelected == 1) {
            cell.tickView.isHidden = true
            cell.incrementView.isHidden = false
            cell.incrementView.layer.cornerRadius = 27.5
        }else {
            cell.tickView.isHidden = false
            cell.incrementView.isHidden = true
        }
        cell.itemCount.text = "\(val.itemCount ?? 0)"
    
        if((val.isSelected == 1)){
            cell.tickImage.image = #imageLiteral(resourceName: "tick_image")
        }else{
            cell.tickImage.image = #imageLiteral(resourceName: "dry-clean")
        }
        cell.tickBtn = {
            
            if(cell.incrementView.isHidden)
            {
                cell.tickImage.duration = 0.25
                cell.tickImage.damping = 5
                cell.tickImage.velocity = 0.2
                cell.tickImage.animation = "pop"
                cell.tickImage.animateToNext{
                    self.selectionDelegate?.singleSelection(currentIndex: self.currentIndex, collectionIndex: indexPath.row)
                }
                
            }
        }
        
        cell.minusItem = {
            
            self.selectionDelegate?.minusFunction(currentIndex: self.currentIndex, collectionIndex: indexPath.row)
            
        }
        
        cell.plusItem = {
            self.selectionDelegate?.plusFunction(currentIndex: self.currentIndex, collectionIndex: indexPath.row)
            
        }
        cell.itemCount.textColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-15, height: 85)
    }
}


class SelectionCell: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var tickImage: SpringImageView!
    @IBOutlet weak var viewForMinus: View!
    @IBOutlet weak var viewForPlus: View!
    @IBOutlet weak var tickView: UIView!
    @IBOutlet weak var incrementView: UIView!
    @IBOutlet weak var itemCount: CountableLabel!
    
    
    
    var tickBtn:(()-> Void)? = nil
    var plusItem: (() -> Void)? = nil
    var minusItem: (() -> Void)? = nil
    
    //MARK: IBAction
    @IBAction func tickAction(_ sender: Any) {
        if let tickBtn = self.tickBtn {
            tickBtn()
        }
        
    }
    
    @IBAction func addItemsPressed(_ sender: Any)
    {
        if let plusItem = self.plusItem {
            plusItem()
        }
    }
    
    @IBAction func subtractItemsPressed(_ sender: Any)
    {
        if let minusItem = self.minusItem {
            minusItem()
        }
    }
    
}
