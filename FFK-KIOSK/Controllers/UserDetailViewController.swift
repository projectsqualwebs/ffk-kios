//
//  UserDetailViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 08/01/21.
//

import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView
import BubbleTransition
import NumericKeyboard


class UserDetailViewController: UIViewController,UIViewControllerTransitioningDelegate,UINavigationControllerDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .present
        transition.duration = 0.3
        transition.startingPoint = myBagButton.center
        transition.bubbleColor = .white//myCartView.backgroundColor!
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.transitionMode = .dismiss
        transition.duration = 0.35
        transition.startingPoint = CGPoint(x: myBagButton.center.x+50, y: myBagButton.center.y)
        transition.bubbleColor = .white
        //myCartView.backgroundColor!
        return transition
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactiveTransition
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var viewBottom: NSLayoutConstraint!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var proccedButtonView: NVActivityIndicatorView!
    @IBOutlet weak var phoneNumberView: View!
    @IBOutlet weak var nameView: View!
    
    @IBOutlet weak var row1: UIView!
    @IBOutlet weak var row2: UIView!
    @IBOutlet weak var row3: UIView!
    @IBOutlet weak var row4: UIView!
    
    @IBOutlet weak var myBagButton: UIButton!
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var numberpadView: UIView!
    @IBOutlet weak var wewilltextlabel: DesignableUILabel!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    var capsLockOn = true
    let transition = BubbleTransition()
    let interactiveTransition = BubbleInteractiveTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.view.frame.width < 1550){
            self.wewilltextlabel.font = UIFont(name: "GothamRounded-Medium", size: 30)
            self.firstName.font = UIFont(name: "GothamRounded-Medium", size: 30)
            self.phoneNumber.font = UIFont(name: "GothamRounded-Medium", size: 30)
            self.topViewHeightConstraint.constant = 300
            
        }
        self.phoneNumber.becomeFirstResponder()
        self.firstName.delegate = self
        self.proceedButton.layer.cornerRadius = 10
        self.firstName.titleFormatter = {$0}
        self.phoneNumber.titleFormatter = {$0}
        self.phoneNumber.layer.cornerRadius = 10
        self.firstName.layer.cornerRadius = 10
        self.firstName.placeholderFont = UIFont(name: "Poppins-Medium", size: 26)
        self.phoneNumber.placeholderFont = UIFont(name: "Poppins-Medium", size: 26)
        let amount = DBHelper.shared.handleCartPrice()
        self.proceedButton.setTitle("Tap to proceed. Your subtotal: $\(amount)", for: .normal)
        self.phoneNumberView.borderColor = .black
        NotificationCenter.default.addObserver(self, selector: #selector(self.popScreen), name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
        
        if((Singleton.shared.userDetail.first_name ?? "") != "" ){
            self.firstName.text = (Singleton.shared.userDetail.first_name ?? "") + " " +  (Singleton.shared.userDetail.last_name ?? "")
        }
        
        if((Singleton.shared.userDetail.mobile ?? "") != "" ){
            self.phoneNumber.text = Singleton.shared.userDetail.mobile?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
        }
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
       // NKInputView.with(self.phoneNumber, type: .numberPad, returnKeyType: .join)
        self.phoneNumber.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if((self.firstName.text ?? "") != "" ){
            let fullName = self.firstName.text ?? ""
            var components = fullName.components(separatedBy: " ")
            if components.count > 0 {
                let firstName = components.removeFirst()
                let lastName = components.joined(separator: " ")
                Singleton.shared.userDetail.first_name = firstName
                Singleton.shared.userDetail.last_name = lastName
            }
        }
        
        if((self.phoneNumber.text ?? "") != "" ){
            Singleton.shared.userDetail.mobile = self.phoneNumber.text ?? ""
        }
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
    }
    
    @objc func popScreen(_ notif: NSNotification) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        if(self.phoneNumber.text!.isEmpty){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.phoneNumberView.center.x - 10, y: self.phoneNumberView.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.phoneNumberView.center.x + 10, y: self.phoneNumberView.center.y))
            self.phoneNumberView.layer.add(animation, forKey: "position")
            self.phoneNumberView.borderColor = primaryColor
        }else if(self.phoneNumber.text!.count != 14){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.phoneNumberView.center.x - 10, y: self.phoneNumberView.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.phoneNumberView.center.x + 10, y: self.phoneNumberView.center.y))
            self.phoneNumberView.layer.add(animation, forKey: "position")
            self.phoneNumberView.borderColor = primaryColor
        }else if(self.firstName.text!.isEmpty || self.firstName.text == " "){
            let animation = CABasicAnimation(keyPath: "position")
            animation.duration = 0.07
            animation.repeatCount = 2
            animation.autoreverses = true
            animation.fromValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x - 10, y: self.nameView.center.y))
            animation.toValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x + 10, y: self.nameView.center.y))
            self.nameView.layer.add(animation, forKey: "position")
            self.nameView.borderColor = primaryColor
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderTotalViewController") as! OrderTotalViewController
            myVC.name = self.firstName.text ?? ""
            myVC.number = self.phoneNumber.text ?? ""
            let transition = CATransition()
            transition.duration = 0.4
            transition.timingFunction = CAMediaTimingFunction(name: .easeIn)
            transition.subtype = .fromBottom
            transition.type = CATransitionType.push
            
            //  self.navigationController?.view.layer.add(transition, forKey: nil)
            
            self.navigationController!.view.layer.add(transition, forKey: kCATransition)
            //  self.navigationController?.pushViewController(myVC, animated: false)
            
            
            self.navigationController?.pushViewController(myVC, animated: false)
        }
    }
    
    @IBAction func nextKeyboardPressed(button: UIButton) {
        // advanceToNextInputMode()
    }
    
    @IBAction func capsLockPressed(button: UIButton) {
        capsLockOn = !capsLockOn
        
        changeCaps(containerView: row1)
        changeCaps(containerView: row2)
        changeCaps(containerView: row3)
        changeCaps(containerView: row4)
    }
    
    @IBAction func keyPressed(button: UIButton) {
        var string = button.titleLabel!.text  ?? ""
        if(self.numberpadView.isHidden == false){
            let mobileString = (self.phoneNumber.text ?? "") + string
            if(mobileString.count > 14){
                return
            }else {
                self.phoneNumber.text = mobileString
                var text = self.phoneNumber.text
                // self.phoneNumber.insertText(string)
                text = text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
                self.phoneNumber.text = text
                self.phoneNumberView.borderColor = .black
                self.nameView.borderColor = .clear
                if((self.phoneNumber.text?.count ?? 0) >= 14){
                    self.keyboardView.isHidden = false
                    self.numberpadView.isHidden = true
                    self.nameView.borderColor = .black
                    self.phoneNumberView.borderColor = .clear
                    self.getUserDetail(text: (self.phoneNumber.text ?? ""))
                    // self.firstName.becomeFirstResponder()
                }
            }
        }else{
            if(self.firstName.text!.count <= 20){
                self.nameView.borderColor = .black
                if(capsLockOn){
                    self.firstName.text = (self.firstName.text ?? "") + string.uppercased()
                    // self.firstName.insertText(string.uppercased())
                }else {
                    self.firstName.text = (self.firstName.text ?? "") + string.lowercased()
                    // self.firstName.insertText(string.lowercased())
                }
                if(self.firstName.text?.count == 1){
                    self.nameView.borderColor = .black
                    if(capsLockOn){
                        self.capsLockOn = false
                        changeCaps(containerView: row1)
                        changeCaps(containerView: row2)
                        changeCaps(containerView: row3)
                        changeCaps(containerView: row4)
                    }
                    
                }
            }else {
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 2
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x - 10, y: self.nameView.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x + 10, y: self.nameView.center.y))
                self.nameView.layer.add(animation, forKey: "position")
                self.nameView.borderColor = primaryColor
            }
        }
        UIView.animate(withDuration: 0.2, animations: {
            button.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
            
        }, completion: {(_) -> Void in
            button.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            
        })
    }
    
    @IBAction func backSpacePressed(button: UIButton) {
        if(self.numberpadView.isHidden == false){
            if(self.phoneNumber.text!.count > 0){
                self.phoneNumber.text?.removeLast()
            }
        }else {
            if(self.firstName.text!.count == 1) {
                self.capsLockOn = false
                self.capsLockPressed(button: button)
            }
            if(self.firstName.text!.count > 0){
                self.firstName.text?.removeLast()
            }
        }
        //  self.firstName.deleteBackward()
    }
    
    @IBAction func spacePressed(button: UIButton) {
        if(self.numberpadView.isHidden == false){
            self.phoneNumber.text = (self.phoneNumber.text ?? "") + " "
        }else {
            if(self.firstName.text?.count != 0){
                self.firstName.text = (self.firstName.text ?? "") + " "
            }
        }
    }
    
    @IBAction func returnPressed(button: UIButton) {
        //  (textDocumentProxy as UIKeyInput).insertText("\n")
    }
    
    
    func changeCaps(containerView: UIView) {
        for view in containerView.subviews {
            for buttona in view.subviews {
                for buttonb in buttona.subviews{
                    for buttonc in buttonb.subviews{
                        if let button = buttonc as? UILabel{
                            let buttonTitle = button.text
                            if capsLockOn {
                                let text = buttonTitle!.uppercased()
                                button.text = text
                            } else {
                                let text = buttonTitle!.lowercased()
                                button.text = text
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: IBActions
    @IBAction func phoneNumberAction(_ sender: Any) {
        self.numberpadView.isHidden = false
        self.keyboardView.isHidden = true
        self.phoneNumberView.borderColor = .black
        self.nameView.borderColor = .clear
    }
    
    @IBAction func firstNameAction(_ sender: Any) {
        self.numberpadView.isHidden = true
        self.keyboardView.isHidden = false
        self.nameView.borderColor = .black
        self.phoneNumberView.borderColor = .clear
    }
    
    
    @IBAction func myBagAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        myVC.transitioningDelegate = self
        myVC.modalPresentationStyle = .custom
        myVC.modalPresentationCapturesStatusBarAppearance = true
        myVC.interactiveTransition = interactiveTransition
        
        //        if(MENU_TYPE == K_LUNCH_ID){
        //            myVC.mainMenuId = Singleton.shared.lunchMenu[0].menu_id ?? 0
        //        }else {
        //            myVC.mainMenuId = Singleton.shared.breakfastMenu[0].menu_id ?? 0
        //        }
        self.present(myVC, animated: true, completion: nil)
    }
}

extension UserDetailViewController: UITextFieldDelegate {
    func handleKeyboardText(text: String){
        var string = text
        if(self.numberpadView.isHidden == false){
            let mobileString = (self.phoneNumber.text ?? "") + string
            if(mobileString.count > 14){
                return
            }else {
                self.phoneNumber.text = mobileString
                var text = self.phoneNumber.text
                // self.phoneNumber.insertText(string)
                text = text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
                self.phoneNumber.text = text
                self.phoneNumberView.borderColor = .black
                self.nameView.borderColor = .clear
                if((self.phoneNumber.text?.count ?? 0) >= 14){
                    self.keyboardView.isHidden = false
                    self.numberpadView.isHidden = true
                    self.nameView.borderColor = .black
                    self.phoneNumberView.borderColor = .clear
                    self.getUserDetail(text: (self.phoneNumber.text ?? ""))
                    // self.firstName.becomeFirstResponder()
                }
            }
        }else{
            if(self.firstName.text!.count <= 20){
                self.nameView.borderColor = .black
                if(capsLockOn){
                    self.firstName.text = (self.firstName.text ?? "") + string.uppercased()
                    // self.firstName.insertText(string.uppercased())
                }else {
                    self.firstName.text = (self.firstName.text ?? "") + string.lowercased()
                    // self.firstName.insertText(string.lowercased())
                }
                if(self.firstName.text?.count == 1){
                    self.nameView.borderColor = .black
                    if(capsLockOn){
                        self.capsLockOn = false
                        changeCaps(containerView: row1)
                        changeCaps(containerView: row2)
                        changeCaps(containerView: row3)
                        changeCaps(containerView: row4)
                    }
                    
                }
            }else {
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 2
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x - 10, y: self.nameView.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x + 10, y: self.nameView.center.y))
                self.nameView.layer.add(animation, forKey: "position")
                self.nameView.borderColor = primaryColor
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(textField)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if(textField == phoneNumber){
            let count = textField.text?.count ?? 0
            if(count < 15){
                self.phoneNumberView.borderColor = UIColor.clear
                self.phoneNumber.text =  self.phoneNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
                if(count == 14){
                    self.getUserDetail(text: (self.phoneNumber.text ?? ""))
                }
            }else {
                self.phoneNumber.text = String(self.phoneNumber.text!.prefix(14))
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == phoneNumber){
            self.phoneNumber.text =  self.phoneNumber.text?.applyPatternOnNumbers(pattern: "(###) ###-####", replacmentCharacter: "#")
            let currentCharacterCount = self.phoneNumber.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength == 14){
                self.getUserDetail(text: (self.phoneNumber.text ?? "") + string)
            }else if(newLength > 14){
                 self.numberpadView.isHidden = true
                self.keyboardView.isHidden = false
            }
            self.phoneNumberView.borderColor = .clear
            return newLength <= 14
        }else {
            let currentCharacterCount = self.firstName.text?.count ?? 0
            if (range.length + range.location > currentCharacterCount){
                return false
            }
            let newLength = currentCharacterCount + string.count - range.length
            if(newLength <= 20){
                self.nameView.borderColor = .clear
            }else {
                let animation = CABasicAnimation(keyPath: "position")
                animation.duration = 0.07
                animation.repeatCount = 2
                animation.autoreverses = true
                animation.fromValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x - 10, y: self.nameView.center.y))
                animation.toValue = NSValue(cgPoint: CGPoint(x: self.nameView.center.x + 10, y: self.nameView.center.y))
                self.nameView.layer.add(animation, forKey: "position")
                self.nameView.borderColor = primaryColor
                return false
            }
            return  newLength <= 20
            
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == phoneNumber){
            self.phoneNumberView.borderColor = UIColor.clear
            self.keyboardView.isHidden = true
            self.numberpadView.isHidden = false
        }else if(textField == firstName){
            self.nameView.borderColor = UIColor.clear
            self.keyboardView.isHidden = false
            self.numberpadView.isHidden = true
        }
    }
    
    func getUserDetail(text:String) {
        self.proccedButtonView.startAnimating()
        self.proceedButton.setTitleColor(.clear, for: .normal)
        self.proceedButton.alpha = 0.9
        self.proceedButton.isUserInteractionEnabled = false
        var number = text.replacingOccurrences(of: "(", with: "")
        number = number.replacingOccurrences(of: ")", with: "")
        number = number.replacingOccurrences(of: "-", with: "")
        number = number.replacingOccurrences(of: " ", with: "")
        
        let param:[String:Any] = [
            "mobile": number
        ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SEARCH_USER, method: .post, parameter: param, objectClass: SearchUser.self, requestCode: U_SEARCH_USER, userToken: nil) { (response) in
            self.proccedButtonView.stopAnimating()
            self.proceedButton.setTitleColor(.white, for: .normal)
            self.proceedButton.alpha = 1
            self.proceedButton.isUserInteractionEnabled = true
            if(response.response != nil){
                self.firstName.text = (response.response?.first_name ?? "") + " " + (response.response?.last_name ?? "")
                
                if let detail = response.response {
                    Singleton.shared.userDetail = detail
                }
            }else {
                self.firstName.text = ""
                Singleton.shared.userDetail = SearchUserResponse()
            }
        }
    }
}
