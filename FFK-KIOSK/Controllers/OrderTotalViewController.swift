//
//  OrderTotalViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 08/01/21.
//

import UIKit
import Spring
import RealmSwift
import SquareReaderSDK
import Alamofire
import AVFoundation
import NVActivityIndicatorView


class OrderTotalViewController: UIViewController, PaymentOption {
    func handlePaymentOption(type: Int) {
        self.dismiss(animated: true) {
            if(type == 1){
              self.authorizeReaderSDKIfNeeded()
            }else if(type == 2) {
                self.callCheckooutApi(id: "", amount: self.totalAmount, type: 2)
            }else {
                self.proceedButtonView.stopAnimating()
                self.proceedButton.setTitleColor(.white, for: .normal)
                self.proceedButton.alpha = 1
                self.proceedButton.isUserInteractionEnabled = true
            }
        }
        
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var orderTotal: ContentSizedTableView!
    @IBOutlet weak var tickImage: SpringImageView!
    
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var subtotal: UILabel!
    @IBOutlet weak var tax: UILabel!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var proceedButton: CustomButton!
    @IBOutlet weak var proceedButtonView: NVActivityIndicatorView!
    
    
    
    var name: String?
    var number: String?
    var cartData =  [MenuList]()
    var totalAmount = Double()
    var timer = Timer()
    var isAgreeToReceiveSms = true
    var blackView =  UIView()
    
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.view.frame.width < 1550){
            self.phoneNumber.font = UIFont(name: "GothamRounded-Medium", size: 24)
            self.username.font = UIFont(name: "GothamRounded-Medium", size: 24)
        }
        self.requestMicrophonePermission()
        self.proceedButton.layer.cornerRadius = 10
        self.cartData = DBHelper.shared.getCartItem()
        let amount = DBHelper.shared.handleCartPrice()
        self.subtotal.text = "$" + "\(amount)"
        let taxPercent = amount*0.0825
        self.tax.text = "$" + "\(Double(round(100*taxPercent)/100))"
        totalAmount = Double(round(100*(taxPercent + amount))/100)
        self.total.text = "$" + "\(self.totalAmount)"
        self.tickImage.layer.cornerRadius = 20
        self.username.text = self.name
        self.phoneNumber.text = self.number
        self.proceedButton.setTitle("Proceed to payment. Your total: $" + "\(self.totalAmount)", for: .normal)
        self.orderTotal.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(self.popScreen), name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    @objc func popScreen(_ notif: NSNotification) {
        NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        //self.pushHome(controller: self)
        let transition = CATransition()
        transition.duration = 0.3
        transition.timingFunction = CAMediaTimingFunction(name: .easeIn)
        transition.subtype = .fromTop
        transition.type = CATransitionType.push
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func paymentAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentOptionViewController") as! PaymentOptionViewController
        myVC.paymentDelegate = self
        self.present(myVC, animated: true)
        // timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.hideControls), userInfo: nil, repeats: false)
    }
    
    @objc func hideControls(){
        timer.invalidate()
        self.dismiss(animated: true, completion: nil)
        // let controller = sqr
        // checkoutControllerDidCancel(self)
    }
    
    @IBAction func tickAction(_ sender: Any) {
        if(self.tickImage.image == nil){
            self.tickImage.image = #imageLiteral(resourceName: "tick_image")
            self.tickImage.duration = 0.1
            self.tickImage.damping = 20
            self.tickImage.velocity = 4
            self.tickImage.animation = "pop"
            self.tickImage.animate()
            self.isAgreeToReceiveSms = true
        }else {
            self.tickImage.image = nil
            self.tickImage.duration = 0.1
            self.tickImage.damping = 20
            self.tickImage.velocity = 4
            self.tickImage.animation = "pop"
            self.tickImage.animate()
            self.isAgreeToReceiveSms = false
        }
    }
    
}

extension OrderTotalViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTable") as! MenuItemTable
        let val = self.cartData[indexPath.row]
        cell.itemName.text = "(\(val.item_count ?? 0)) "  + (val.item_name ?? "")
        let price = Double(round(100*(Double(val.item_count ?? 0)*(val.item_price ?? 0)))/100)
        cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
        for item in val.selectedSubmenu{
            for i in item.meals{
                if(i.isSelected == 1 || i.is_checked == 1){
                    if(i.item_name.lowercased() == "smothered" || i.item_name.lowercased() == "baked"){
                        let amount = ((val.item_price ?? 0)+1)
                        let price = Double(round(100*(Double(val.item_count ?? 0)*(amount)))/100)
                        cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
                    }
                }
            }
        }
        return cell
    }
    
    
}

extension OrderTotalViewController: SQRDCheckoutControllerDelegate {
    
    func retrieveAuthorizationCode(completion: @escaping (String) -> Void){
        let headers: HTTPHeaders = [
            .authorization(bearerToken: "EAAAEdsEkXAis7qr9i2Z51PiLRl3tQ_ss-onvBrgCa1D7ZErJ6FXF5MQVdhu7w6d"),
            .accept("application/json"),
            .init(name: "Square-Version", value: "2021-05-13")
        ]
        AF.request("https://connect.squareup.com/mobile/authorization-code", method: .post, parameters: [ "location_id":"L41Q7BKZ0HJZJ"], encoding: JSONEncoding.default, headers: headers, interceptor: nil).responseString {
            (response) in
            if let data = SessionManager.shared.convertDataToObject(response: response.data, SquarePaymentResponse.self){
                if((data.message ?? "") != ""){
                    self.showMessage(view: self, message: data.message ?? "")
                }else {
                    completion(data.authorization_code ?? "")
                }
            }
        }
    }
    
    func authorizeReaderSDKIfNeeded() {
        if SQRDReaderSDK.shared.isAuthorized {
            self.initializeCheckout()
        } else {
            self.retrieveAuthorizationCode { (response) in
                SQRDReaderSDK.shared.authorize(withCode: response) { _, error in
                    if let authError = error {
                        // Handle the error
                        self.showMessage(view: self, message: "Square Payment not authorized")
                        self.pairCardReaders()
                    } else {
                        self.initializeCheckout()
                    }
                }
            }
        }
    }
    
    func initializeCheckout() {
        let money = SQRDMoney(amount: Int(self.totalAmount*100))
        // Create checkout parameters
        let checkoutParameters = SQRDCheckoutParameters(amountMoney: money)
        checkoutParameters.note = "Tap card for payment"
        checkoutParameters.skipReceipt = true
        //checkoutParameters.additionalPaymentTypes = [.cash]
        // Create a checkout controller
        let checkoutController = SQRDCheckoutController(parameters: checkoutParameters, delegate: self)
        checkoutController.accessibilityViewIsModal = false
        
        // Present the Reader Settings controller from the `AppViewController` instance.
        
        if let presenter = parent {
            checkoutController.present(from: self)
        }
    }
    
    
    
    func checkoutControllerDidCancel(_: SQRDCheckoutController) {
        ActivityIndicator.hide()
        self.showMessage(view: self, message: "Checkout cancelled")
    }
    
    func checkoutController(_ checkoutController: SQRDCheckoutController, didFinishCheckoutWith result: SQRDCheckoutResult) {
      
        let label =  UILabel(frame: CGRect(x:  0, y: self.view.center.y, width: self.view.frame.width, height: 50))
        label.textAlignment = .center
        label.text = "We are processing your oder. Hang tight!"
        label.textColor = UIColor.white
        label.font = UIFont(name: "GothamRounded-Medium", size: 28)
        self.blackView.frame = self.view.frame
        self.blackView.addSubview(label)
        self.blackView.backgroundColor = .black
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.blackView)
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15)) {
            self.blackView.removeFromSuperview()
        }
        self.dismiss(animated: true, completion: nil)
        self.callCheckooutApi(id: result.transactionID ?? "", amount: Double(result.totalMoney.amount), type: 1)
        
    }
    
    func callCheckooutApi(id: String, amount: Double, type: Int){
        let cartItem:[MenuList] = DBHelper.shared.getCartItem()
        var data = [CheckoutObject]()
        var cartData = [Dictionary<String,Any>]()
        
        for val in cartItem {
            var modifierData = [AddToCart]()
            for item in val.selectedSubmenu{
                for modifier in item.meals{
                    if(modifier.isSelected == 1){
                        modifierData.append(AddToCart(modifier_id: "\(modifier.modifier_group_id ?? 0)", modifier_item_id: "\(modifier.id ?? 0)", modifier_item_count: "\(modifier.itemCount ?? 0)", item_count: modifier.itemCount))
                    }
                }
            }
            data.append(CheckoutObject(item_id: val.item_id, item_count: val.item_count, modifiers: modifierData))
        }
        
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try! jsonEncoder.encode(data)
            if let jsonArray = try JSONSerialization.jsonObject(with: jsonData, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                cartData = jsonArray
            } else {
                print("bad json")
            }
        } catch {
            print(error)
            
        }
        
        self.proceedButtonView.startAnimating()
        self.proceedButton.setTitleColor(.clear, for: .normal)
        self.proceedButton.alpha = 0.9
        self.proceedButton.isUserInteractionEnabled = false
        
        let dbAmount = DBHelper.shared.handleCartPrice()
        let taxAmout = (Double(round(100*(dbAmount*0.0825))/100))
        self.number = self.number?.replacingOccurrences(of: "(", with: "")
        self.number = self.number?.replacingOccurrences(of: ")", with: "")
        self.number = self.number?.replacingOccurrences(of: " ", with: "")
        self.number = self.number?.replacingOccurrences(of: "-", with: "")
        
        var param:[String:Any]
        if(type == 1){
            param = [
                "menu":cartData,
                "user_name":self.username.text ?? "",
                "mobile_number": self.number,
                "order_total":dbAmount,
                "total_tax":taxAmout,
                "total_amount": Double(Double(amount)/100),
                "menu_id": (cartItem.count > 0) ? cartItem[0].id:0,
                "restaurant_id": Singleton.shared.selectedLocation.id,
                "sms_subscribed":self.isAgreeToReceiveSms ? 1:0,
                "transaction":[
                    "transaction_id":id != "" ?  id:"lkfsdjflds",
                    "amount":Int(amount),
                ]
            ]
        }else {
            param = [
                "menu":cartData,
                "user_name":self.username.text ?? "",
                "mobile_number": self.number,
                "order_total":dbAmount,
                "total_tax":taxAmout,
                "total_amount":Double(Double(amount)),
                "menu_id": (cartItem.count > 0) ? cartItem[0].id:0,
                "restaurant_id": Singleton.shared.selectedLocation.id,
                "sms_subscribed":self.isAgreeToReceiveSms ? 1:0,
            
            ]
        }
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_PLACE_ORDER, method: .post, parameter: param, objectClass: OrderConfirmed.self, requestCode: U_PLACE_ORDER, userToken: nil) { (response) in
            self.proceedButtonView.stopAnimating()
            self.proceedButton.setTitleColor(.white, for: .normal)
            self.proceedButton.alpha = 1
            self.proceedButton.isUserInteractionEnabled = true
            
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScanCardViewController") as! ScanCardViewController
            
            myVC.orderId = response.response?.order_id ?? 0
            myVC.userName = self.username.text ?? ""
            self.navigationController?.pushViewController(myVC, animated: true)
            self.blackView.removeFromSuperview()
        }
    }
    
    
    // Failure delegate method
    func checkoutController(_ name: SQRDCheckoutController, didFailWith error: Error) {
        // Checkout controller errors are always of type SQRDCheckoutControllerError
        let checkoutControllerError = error as! SQRDCheckoutControllerError
        
        switch checkoutControllerError.code {
        case .sdkNotAuthorized:
            // Checkout failed because the SDK is not authorized
            // with a Square merchant account.
            self.showMessage(view: self, message: "Square Payment not authorized")
            break
        case .usageError:
            
            // Checkout failed due to a usage error. Inspect the userInfo
            // dictionary for additional information.
            
            if let debugMessage = checkoutControllerError.userInfo[SQRDErrorDebugMessageKey],
               let debugCode = checkoutControllerError.userInfo[SQRDErrorDebugCodeKey] {
                if let msg = debugMessage as? String {
                    self.showMessage(view: self, message:msg)
                }
                print(debugCode, debugMessage)
            }
            break
        }
    }
    
    
    func pairCardReaders() {
        if let authorized = UserDefaults.standard.value(forKey: UD_CARD_READER_CONNECTED) as? Bool {
            if(authorized){
                return
            }
        }
        let readerSettingsController = SQRDReaderSettingsController(
            delegate: self
        )
        readerSettingsController.present(from: self)
    }
    
    func readerSettingsController(
        _: SQRDReaderSettingsController,
        didFailToPresentWith error: Error
    ) {
        // Handle the error - this example prints the error to the console
        self.showMessage(view: self, message: "This device is not currently authorized to accept payments with Square.")
        print(error)
    }
}

extension OrderTotalViewController: SQRDReaderSettingsControllerDelegate {
    func readerSettingsControllerDidPresent(_ readerSettingsController: SQRDReaderSettingsController) {
        print("The Reader Settings controller did present.")
    }
    
    func requestMicrophonePermission() {
        AVAudioSession.sharedInstance().requestRecordPermission { authorized in
            if !authorized {
                print("Show UI directing the user to the iOS Settings app")
            }
        }
    }
    
    
}
