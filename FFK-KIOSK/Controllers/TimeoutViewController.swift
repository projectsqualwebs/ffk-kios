//
//  TimeoutViewController.swift
//  FFK-KIOSK
//
//  Created by Sagar Pandit on 09/08/21.
//

import UIKit


class TimeoutViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var mainView: UIView!
    //  @IBOutlet weak var neverMindButton: CustomButton!
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var subHeadingLabel: UILabel!
    @IBOutlet weak var yesButton: CustomButton!
    @IBOutlet weak var nevermindButton: CustomButton!
    
    
    var navigationDelegate: ResetView? = nil
    var cartItemDelegate: ClearCarItem? = nil
    var timer = Timer()

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.mainView.alpha = 0
            self.headingLabel.text = "Still thinking?"
            self.subHeadingLabel.text = "Need more time or should we start a new order?"
            self.yesButton.setTitle("Continue ordering", for: .normal)
            self.nevermindButton.setTitle("Cancel order and clear cart", for: .normal)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.popupView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        
        self.mainView.alpha = 0.5
       // self.neverMindButton.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 8.0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.popupView.isHidden = false
                        self?.popupView.transform = .identity
                        self?.mainView.alpha = 1
                //        self?.neverMindButton.isHidden = false
               //         self?.neverMindButton.transform = .identity
                        
                       },
                       completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        timer =  Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_AUTODISMISS, target: self, selector: #selector(resetView), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    @objc func resetView(){
        self.performExit()
    }
    
    func performNevermind(){
        self.mainView.alpha = 0.8
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut) {
            self.popupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            self.popupView.alpha  = 0
            self.mainView.alpha = 0
            
        } completion: { (val) in
            self.popupView.isHidden = true
            self.popupView.transform = .identity
            self.popupView.alpha  = 1
            self.mainView.alpha = 0
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func performExit(){
        self.navigationDelegate?.clearNavigationStack()
        self.mainView.alpha = 0.8
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut) {

            self.popupView.transform = CGAffineTransform(scaleX: 0.1, y: 0.11)
            self.popupView.alpha  = 0
            self.mainView.alpha = 0
            Singleton.shared.cartItemCount = 0
            UserDefaults.standard.removeObject(forKey: UD_BAG_COUNT)
            NavigationController.shared.emptyMyCart()
            
        } completion: { (val) in
            self.popupView.isHidden = true
            self.popupView.transform = .identity
            self.popupView.alpha  = 1
            self.mainView.alpha = 0
            Singleton.shared.userDetail = SearchUserResponse()
            Singleton.shared.cartItemCount = 0
            DBHelper.shared.emptyLocalStorage()
            self.clearNavigationStack()
            self.cartItemDelegate?.emptyCartData()
            NotificationCenter.default.post(name: NSNotification.Name(N_SHOW_HIDE_CATEGORY_STACK), object: nil, userInfo: ["show":true])
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_SWIPE_CARD), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: IBActions
    @IBAction func exitAction(_ sender: Any) {
      self.performNevermind()
    }
    
    @IBAction func nevermindAction(_ sender: Any) {
      self.performExit()
    }
    
}
