//
//  MyCartViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 02/01/21.
//

import UIKit
import CountableLabel
import BubbleTransition
import Spring
import NVActivityIndicatorView
import PINRemoteImage
import AVFoundation
import Alamofire
import RealmSwift

protocol NavigateToOrder {
    func pushToOrder()
}

var isMyCartOpen = false

class MyCartViewController: UIViewController,ClearCarItem {
    func emptyCartData(){
        try! DBHelper.realm.write {
            DBHelper.realm.delete(self.selectedDeleteItem)
        }
        if(self.cartData.cart_data.count > self.selectedDeleteIndex){
            self.cartTable.beginUpdates()
            self.cartData.cart_data.remove(at: self.selectedDeleteIndex)
            self.cartTable.deleteRows(at: [IndexPath(row: self.selectedDeleteIndex, section: 0)], with: .top)
            self.cartTable.endUpdates()
        }
        self.myCartData = DBHelper.shared.getCartItem()
    }
    
    //MARK: IBOutlets
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var mealTable: UITableView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var proceedButton: DampingButton!
    @IBOutlet weak var proceedIndicatorView: NVActivityIndicatorView!
    @IBOutlet weak var itemAddedView: View!
    
    @IBOutlet weak var editButtonSpinnerView: NVActivityIndicatorView!
    @IBOutlet weak var bounceView: UIView!
    @IBOutlet weak var menuTable: UITableView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var editButton: DampingButton!
    @IBOutlet weak var tableBackView: UIView!
    @IBOutlet weak var mealBackView: UIView!
    
    
    weak var interactiveTransition: BubbleInteractiveTransition?
    var navigationDelegate: NavigateToOrder? = nil
    var cartData = MyCartData()
    var mealsData = [GetMenusList]()
    var mainMenuId = Int()
    var jsonData:Any?
    var submenuData = [SubMenuResponse]()
    var selectedCartItemId = Int()
    var audioPlayer = AVAudioPlayer()
    var apiCallingTimer = Timer()
    var myCartData = [MenuList]()
    var selectedEditIndex = Int()
    var selectedDeleteIndex = Int()
    var selectedDeleteItem = MenuList()
    var isDollarSelected = false
    
    let currentCartId = UserDefaults.standard.value(forKey: UD_ADD_TO_CART) as? String
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.proceedButton.layer.cornerRadius = 10
        self.editButton.layer.cornerRadius = 10
        let sound = Bundle.main.path(forResource: "notifications_20", ofType: "mp3")
        do{
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: sound!), fileTypeHint: "mp3")
        }catch{
            print(error)
        }
        
        cartTable.estimatedRowHeight = 200
        cartTable.rowHeight = UITableView.automaticDimension
        mealTable.estimatedRowHeight = 200
        mealTable.rowHeight = UITableView.automaticDimension
        mealTable.tableFooterView = UIView()
        closeButton.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
        self.myCartData = DBHelper.shared.getCartItem()
        self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  "\(DBHelper.shared.handleCartPrice())", for: .normal)
        self.cartTable.reloadData()
        //  getCartData()
        
        
        // self.activeTabImage.image = UIImage.gif(asset: "active")
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(self.dismissView), name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isMyCartOpen = true
        getMenuMeals()
        UIApplication.shared.setStatusBarStyle(.lightContent, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.getCartData()
        isMyCartOpen = false
        super.viewWillDisappear(animated)
        UIApplication.shared.setStatusBarStyle(.default, animated: false)
        
        self.dismiss(animated: false, completion: nil)
        
        self.interactiveTransition?.finish()
    }
    
    @objc func dismissView(_ notif: Notification){
        if let close = notif.userInfo?["close"] as? Bool{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ExitPopupViewController") as! ExitPopupViewController
            // myVC.navigationDelegate = self
            // myVC.cartItemDelegate = self
            myVC.isTimeout = true
            myVC.modalPresentationStyle = .overFullScreen
            self.present(myVC, animated: false, completion: nil)
        }else {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_DISMISS_MYCART_SCREEN), object: nil)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func addTopAndBottomBorders(view: UIView) {
        let thickness: CGFloat = 2.0
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0, y: 0.0, width: view.frame.size.width, height: thickness)
        topBorder.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 0.8).cgColor
        view.layer.addSublayer(topBorder)
    }
    
    func getCartData() {
        NavigationController.shared.getMyCart { (response) in
            if(response.cart_data.count == 0){
                Singleton.shared.cartItemCount = 0
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                // let vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                // self.navigationController?.pushViewController(vc, animated: false)
                self.clearNavigationStack()
                NotificationCenter.default.post(name: NSNotification.Name(N_MY_CART_EMPTY_DATA), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_USER_DETAIL_VIEW), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_SWIPE_CARD), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(N_DISMISS_ORDER_TOTAL_VIEW), object: nil)
                return
            }
            if(response.cart_data.count > 0){
                UserDefaults.standard.setValue(response.cart_details.cart_id ?? "", forKey: UD_ADD_TO_CART)
            }
            self.cartData = response
            var count = 0
            for val in self.cartData.cart_data{
                count = count + (val.item_count ?? 0)
            }
            //   UserDefaults.standard.setValue(count ?? 0, forKey: UD_BAG_COUNT)
            Singleton.shared.cartItemCount = count ?? 0
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_BAG_COUNT), object: nil)
            //  Singleton.shared.cartData = response
            //     self.proceedButton.isUserInteractionEnabled = true
            //    self.proceedButton.alpha = 1
            //    self.proceedIndicatorView.stopAnimating()
            //    self.proceedButton.setTitleColor(.white, for: .normal)
            self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  (self.cartData.cart_details.total_amount ?? ""), for: .normal)
            self.cartTable.reloadData()
        }
    }
    
    func getMenuMeals(){
        //        if(currentCartId == nil || currentCartId == ""){
        //            return
        //        }
        //        if(Singleton.shared.mealsData.count == 0){
        //            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MENU_MEALS + "\(mainMenuId)?cart_id=\(currentCartId ?? "")", method: .get, parameter: nil, objectClass: GetMeals.self, requestCode: U_GET_MENU_MEALS, userToken: nil) { (response) in
        //                self.mealsData = response.response
        //                Singleton.shared.mealsData = response.response
        //                self.mealTable.reloadData()
        //            }
        //        }else{
        //            self.mealsData = Singleton.shared.mealsData
        //            self.mealTable.reloadData()
        //        }
        self.mealsData = []
        if(MENU_TYPE == K_LUNCH_ID){
            if(Singleton.shared.lunchMenu.count > 0){
                for val in Singleton.shared.lunchMenu[2].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
                for val in Singleton.shared.lunchMenu[1].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
                
                for val in Singleton.shared.lunchMenu[3].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
            }
        }else if(MENU_TYPE == K_BREAK_ID) {
            if(Singleton.shared.breakfastMenu.count > 0){
                for val in Singleton.shared.breakfastMenu[2].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
                
                for val in Singleton.shared.breakfastMenu[1].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
                
                for val in Singleton.shared.breakfastMenu[3].menus{
                    if(val.modifiers == 0){
                        self.mealsData.append(val)
                    }
                }
            }
        }
        self.mealTable.reloadData()
    }
    
    //MARK: IBActions
    @IBAction func proceedAction(_ sender: Any) {
        if((self.cartData.cart_details.total_amount ?? "") != "0.00"){
            self.interactiveTransition?.finish()
            self.navigationDelegate?.pushToOrder()
            self.dismiss(animated: true, completion: nil)
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
            myVC.modalPresentationStyle = .overFullScreen
            myVC.heading = "Please add item(s) to proceed."
            self.present(myVC, animated: false, completion: nil)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        //self.pushHome(controller: self)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func exitAction(_ sender: Any) {
        self.pushHome(controller: self)
    }
    
    @IBAction func closeAction(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        self.interactiveTransition?.finish()
        
        // NOTE: when using interactive gestures, if you want to dismiss with a button instead, you need to call finish on the interactive transition to avoid having the animation stuck
        
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        UIView.animate(withDuration: 0.27, animations: {
            self.bounceView.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.bounceView.alpha = 0.0
        }) { _ in
            self.bounceView.isHidden = true
            self.bounceView.alpha  = 1
            self.selectedCartItemId  = 0
            self.submenuData = []
            self.menuTable.reloadData()
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.handleSelectedValue()
//        DispatchQueue.main.async {
//            DBHelper.shared.handleCartPrice()
//            self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  "\(DBHelper.shared.handleCartPrice())", for: .normal)
//        }
    }
    
}

extension MyCartViewController: UITableViewDelegate, UITableViewDataSource {
    func handleSelectedValue(){
        var selectedItems = [AddToCart]()
        for val in self.submenuData{
            switch val.is_rule {
            case 1:
                if (val.sectionItemCount != val.item_exactly){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    //myVC.heading = "Must select \(val.item_exactly ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id)", modifier_item_id: "\(data.id)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            case 2:
                if !(val.sectionItemCount! >= val.item_range_from! && val.sectionItemCount! <= val.item_range_to!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                   // myVC.heading = "Must select \(val.item_range_from ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id)", modifier_item_id: "\(data.id)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            case 3:
                if (val.sectionItemCount != val.item_maximum){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                   // myVC.heading = "Must select \(val.item_maximum ?? 0) from \(val.modifier_group_name ?? "")"
                    myVC.heading = "Looks like you missed to select following: \(val.modifier_group_name ?? "")"
                    
                    self.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id)", modifier_item_id: "\(data.id)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            default:
                break
            }
        }
        
        if(selectedItems.count  != 0 ){
            do {
                var object = self.myCartData
                object[self.selectedEditIndex].selectedSubmenu = DBHelper.shared.getSubmenu(data: self.submenuData)
                DispatchQueue.main.async {
                    DBHelper.shared.updateItem(data: object)
                    self.myCartData = object
                    self.cartTable.reloadData()
                    self.didTapClose(self)
                }
                
            } catch { print(error) }
        }
    }
    
    
    func handleUpdateCart(val: MenuList){
        
        //        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_EDIT_CART_MODIFIER + "\(id)", method: .get, parameter: nil, objectClass: GetSubmenu.self, requestCode: U_GET_SUBCATEGORIES, userToken: nil) { (response) in
        //            self.submenuData = response.response
        //            self.selectedCartItemId = id
        //            for val in 0..<self.submenuData.count {
        //                self.submenuData[val].sectionItemCount = 0
        //                for check in 0..<self.submenuData[val].meals.count {
        //                    self.submenuData[val].meals[check].itemCount = self.submenuData[val].meals[check].count ?? 0
        //                    if(self.submenuData[val].meals[check].itemCount! > 0){
        //                        self.submenuData[val].sectionItemCount = (self.submenuData[val].sectionItemCount ?? 0) + 1
        //                        self.submenuData[val].meals[check].isSelected = 1
        //                    }else {
        //                        self.submenuData[val].meals[check].isSelected = 0
        //                    }
        //                }
        //            }
        self.bounceView.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        UIView.animate(withDuration: 0.4,
                       delay: 0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 8.0,
                       options: .curveEaseInOut,
                       animations: { [weak self] in
                        self?.bounceView.isHidden = false
                        self?.headingLabel.text = "Customize " + (val.item_name ?? "")
                        self?.bounceView.transform = .identity
                       },
                       completion: nil)
        
        self.submenuData = []
        for val in val.selectedSubmenu{
            var submenu = [SubmenuSubCategory]()
            for menu in val.meals{
                submenu.append(SubmenuSubCategory(id: menu.id, cart_item_id: menu.cart_item_id, item_id: menu.item_id, side_menu_id: menu.side_menu_id, modifier_group_id: menu.modifier_group_id, category_id: menu.category_id, item_name: menu.item_name, item_price: menu.item_price, item_image: menu.item_image, thumbnail: menu.thumbnail, tax_rate: menu.tax_rate, item_description: menu.item_description, ipad_image: menu.ipad_image, ipad_image_single: menu.ipad_image_single, its_own: menu.its_own, isSelected: menu.isSelected, itemCount: menu.itemCount, count: menu.count, restaurant_id: 0, is_checked: menu.isSelected, item_count: menu.item_count))
            }
            
            
            self.submenuData.append(SubMenuResponse(id: val.id, welcome_bonus_item_id: 0, modifier_group_id: val.modifier_group_id, item_id: val.item_id, added_from: val.added_from, item_exactly: val.item_exactly, item_range_from: val.item_range_from, item_count: val.item_count, item_range_to: val.item_range_to, restaurant_id: 0, item_maximum: val.item_maximum, single_item_maximum: val.single_item_maximum, modifier_group_name: val.modifier_group_name, is_rule: val.is_rule, sectionItemCount: val.sectionItemCount, meals: submenu))
        }
        
        self.menuTable.reloadData()
        //  }
        
    }
    
    func callUpdateCountApi(count:Int,cartId: Int){
        // self.proceedButton.alpha = 0.9
        //  self.proceedButton.isUserInteractionEnabled = false
        //   self.proceedIndicatorView.startAnimating()
        //   self.proceedButton.setTitleColor(.clear, for: .normal)
        DispatchQueue.global(qos: .background).async {
            let param:[String:Any] = [
                "cart_item_id": cartId,
                "item_count": count
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_UPDATE_ITEM_COUNT, method: .post, parameter: param, objectClass: SuccessResponse.self, requestCode: U_UPDATE_ITEM_COUNT, userToken: nil) { (response) in
                // Singleton.shared.cartData.cart_data = []
                // self.getCartData()
                //     self.proceedButton.alpha = 1
                //    self.proceedButton.isUserInteractionEnabled = true
                //      self.proceedIndicatorView.startAnimating()
                //      self.proceedButton.setTitleColor(.clear, for: .normal)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == cartTable){
            return self.myCartData.count
            //  return self.cartData.cart_data.count
        }else if(tableView == menuTable){
            return self.submenuData.count
        }else {
            return self.mealsData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == menuTable){
            let sectionData = self.submenuData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionTableView") as! SelectionTableView
            cell.currentIndex = indexPath.row
            cell.selectionDelegate = self
            cell.submenuData = self.submenuData[indexPath.row].meals
            cell.sectionData = self.submenuData
            let operation = (sectionData.meals.count/2) + (sectionData.meals.count%2)
            cell.collectionViewHeight.constant = CGFloat(operation*85) + 20
            cell.collectionView.reloadData()
            let attrs1 = [NSAttributedString.Key.font :  UIFont(name: "GothamRounded-Book", size: 22), NSAttributedString.Key.foregroundColor : UIColor.gray]
            
            let attrs2 = [NSAttributedString.Key.font :  UIFont(name: "GothamRounded-Medium", size: 25), NSAttributedString.Key.foregroundColor : UIColor.black]
            
            
            if(sectionData.is_rule == 1){
                if((sectionData.item_exactly ?? 0) == 1){
                    let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                    
                }else {
                    let attributedString1 = NSMutableAttributedString(string:" (select \(sectionData.item_exactly ?? 0))", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                }
            }else if(sectionData.is_rule == 2){
                if((sectionData.item_range_to ?? 0) == 1){
                    let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                }else {
                    let attributedString1 = NSMutableAttributedString(string:" (choose upto \(sectionData.item_range_to ?? 0))", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                }
            }else{
                if((sectionData.item_maximum ?? 0) == 1){
                    let attributedString1 = NSMutableAttributedString(string:" (required)", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                }else {
                    let attributedString1 = NSMutableAttributedString(string:" (select \(sectionData.single_item_maximum ?? 0))", attributes:attrs1)
                    
                    let attributedString2 = NSMutableAttributedString(string:sectionData.modifier_group_name ?? "", attributes:attrs2)
                    
                    attributedString2.append(attributedString1)
                    cell.headignLabel.attributedText = attributedString2
                }
            }
            
            return cell
        }else if(tableView == cartTable){
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTable") as! MenuItemTable
            let val = self.myCartData[indexPath.row]
            
            cell.itemCount.animationType = .none
            cell.itemCount.textColor = .white
            cell.springView.isHidden = true
            cell.itemCount.text = "\(val.item_count ?? 0)"
            if((val.item_count ?? 0) > 1){
                cell.springView.isHidden = false
            }else {
                cell.springView.isHidden = true
            }
            // cell.itemImage.sd_setImage(with: URL(string:(U_IMAGE_BASE) + (val.item_image ?? "")), placeholderImage: UIImage(named: "placeholder.png"))
            cell.itemImage.pin_setImage(from: URL(string:(U_IMAGE_BASE) + (val.item_image ?? "")))
            if(val.item_name.lowercased() == "smothered" || val.item_name.lowercased() == "baked"){
                let stringText = (val.item_name ?? "")
                let attributedText = NSMutableAttributedString(string: stringText, attributes: [NSAttributedString.Key.font: UIFont(name: "GothamRounded-Bold", size: 32)])
                attributedText.append(NSAttributedString(string: " (+$1)", attributes: [NSAttributedString.Key.font:  UIFont(name: "GothamRounded-Book", size: 22), NSAttributedString.Key.foregroundColor: UIColor.black]))
                cell.itemName.attributedText = attributedText
            }else {
                cell.itemName.text = (val.item_name ?? "")
            }
            let number = Double(val.item_count ?? 0)*(val.item_price ?? 0)
            let price = Double(round(100*number)/100)
            cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
            cell.itemDesc.text = ""
            for items in val.selectedSubmenu{
                for meal in items.meals{
                    if(meal.isSelected == 1){
                        cell.itemDesc.text =  (cell.itemDesc.text ?? "") + (((cell.itemDesc.text ?? "") != "") ? ", ":"") + ((meal.item_count ?? 0) > 1 ? ("\(meal.item_count ?? 0) x " + (meal.item_name ?? "")):(meal.item_name ?? ""))
                        if(meal.item_name.lowercased() == "smothered" || meal.item_name.lowercased() == "baked"){
                            let num = Double(val.item_count ?? 0)*((val.item_price ?? 0)+1)
                            let price = Double(round(100*(num))/100)
                            cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
                        }
                    }
                }
            }
            if(val.modifiers == 0){
                cell.editButton.isHidden = true
            }
            cell.itemImage.layer.cornerRadius = 5
            cell.removeBtn = {
                if(self.myCartData.count == 1){
                    self.selectedDeleteIndex = indexPath.row
                    self.selectedDeleteItem = val
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ExitPopupViewController") as! ExitPopupViewController
                    myVC.navigationDelegate = self
                    myVC.cartItemDelegate = self
                    myVC.modalPresentationStyle = .overFullScreen
                    self.present(myVC, animated: false, completion: nil)
                }else {
                    try! DBHelper.realm.write {
                        DBHelper.realm.delete(val)
                    }
                    if(self.cartData.cart_data.count > indexPath.row){
                        self.cartTable.beginUpdates()
                        self.cartData.cart_data.remove(at: indexPath.row)
                        self.cartTable.deleteRows(at: [indexPath], with: .top)
                        self.cartTable.endUpdates()
                    }
                    self.myCartData = DBHelper.shared.getCartItem()
                }
                if(self.myCartData.count == 0){
                   
                    return
                }
                UIView.performWithoutAnimation {
                    self.cartTable.reloadData()
                }
                self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  "\(DBHelper.shared.handleCartPrice())", for: .normal)
               
            }
            
            cell.minusBtn = {
                
                var count = Int(cell.itemCount.text ?? "")!
                if(count > 1){
                    count -= 1
                }
                if(count == 1){
                    UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear) {
                        cell.springView.animation = "squeezeUp"
                        cell.springView.duration = 0.5
                        cell.springView.animate()
                        cell.springView.alpha = 0
                    } completion: { (val) in
                        cell.springView.isHidden = true
                        cell.springView.alpha = 1
                    }
                    
                }
                cell.itemCount.animationType = .pushDown
                cell.itemCount.text = "\(count)"
                DBHelper.shared.updateItemCount(randomId: val.random_id, increment: count)
                let number = Double(count ?? 0)*(val.item_price ?? 0)
                let price = Double(round(100*number)/100)
                cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
                
                for items in val.selectedSubmenu{
                    for meal in items.meals{
                        if(meal.isSelected == 1){
                            if(meal.item_name.lowercased() == "smothered" || meal.item_name.lowercased() == "baked"){
                                let num = Double(count ?? 0)*((val.item_price ?? 0)+1)
                                let prc = Double(round(100*num)/100)
                                cell.itemPrice.text = "$" + "\(prc)".appendString(data: prc)
                                self.handleSelectedValue()
                            }
                        }
                    }
                }
                self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  "\(DBHelper.shared.handleCartPrice())", for: .normal)
            }
            
            cell.plusBtn = {
                var count = Int(cell.itemCount.text ?? "")!
                if(count == 1){
                    cell.springView.animation = "squeezeDown"
                    cell.springView.duration = 0.5
                    cell.springView.animate()
                    cell.springView.isHidden = false
                }
                count += 1
                cell.itemCount.animationType = .pushUp
                cell.itemCount.text = "\(count)"
                if self.audioPlayer.isPlaying {
                    self.audioPlayer.pause()
                }
                self.audioPlayer.currentTime = 0
                self.audioPlayer.play()
                DBHelper.shared.updateItemCount(randomId: val.random_id, increment: count)
                let number = Double(count ?? 0)*(val.item_price ?? 0)
                let price = Double(round(100*number)/100)
                cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
                for items in val.selectedSubmenu{
                    for meal in items.meals{
                        if(meal.isSelected == 1){
                            if(meal.item_name.lowercased() == "smothered" || meal.item_name.lowercased() == "baked"){
                                let num = Double(count ?? 0)*((val.item_price ?? 0)+1)
                                let prc = Double(round(100*num)/100)
                                cell.itemPrice.text = "$" + "\(prc)".appendString(data: prc)
                                self.handleSelectedValue()
                            }
                        }
                    }
                }
                self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  " \(DBHelper.shared.handleCartPrice())", for: .normal)
            }
            
            cell.rapidFireButton.tapHandler={(val) in
                //  self.itemAddedView.transform = CGAffineTransform(scaleX: 0, y: 0)
                self.showHideView(view:self.itemAddedView)
            }
            
            cell.editBtn={
                self.selectedEditIndex = indexPath.row
                self.handleUpdateCart(val:val)
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemTable") as! MenuItemTable
            let val = self.mealsData[indexPath.row]
            
            //  cell.itemImage.sd_setImage(with: URL(string:(U_IMAGE_BASE) + (val.meal?.item_image ?? "")), placeholderImage: UIImage(named: "placeholder.png"))
            
            cell.itemImage.pin_setImage(from: URL(string:(U_IMAGE_BASE) + (val.item_image ?? "")), placeholderImage:(UIImage(named: "background_image")))
            
            cell.itemName.text = val.item_name
            let price = Double(round(100*(val.item_price ?? 0)))/100
            cell.itemPrice.text = "$" + "\(price)".appendString(data: price)
            cell.itemDesc.text = val.item_description
            
            cell.rapidFireButton.tapHandler = { (item) in
                self.itemAddedView.transform = CGAffineTransform(scaleX: 0, y: 0)
                self.showHideView(view:self.itemAddedView)
            }
            
            cell.plusBtn = {
                DBHelper.shared.addItemToCart(mainMenuId: self.mainMenuId, menuList: val, subcategory: [SubMenuResponse]())
                self.myCartData = DBHelper.shared.getCartItem()
                self.cartTable.reloadData()
                self.proceedButton.setTitle("Tap to proceed. Your subtotal: $" +  "\(DBHelper.shared.handleCartPrice())", for: .normal)
                
                if self.audioPlayer.isPlaying {
                    self.audioPlayer.pause()
                }
                self.audioPlayer.currentTime = 0
                self.audioPlayer.play()
            }
            return cell
        }
    }
}

extension MyCartViewController : SelectionProtocol{
    func singleSelection(currentIndex: Int, collectionIndex: Int) {
        switch (self.submenuData[currentIndex].is_rule ?? 0) {
        case 1:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_exactly ?? 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    let name = self.submenuData[currentIndex].meals[collectionIndex]
//                    let totalAmount = Double(self.cartData.cart_details.total_amount ?? "0")!
//                    if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected == false){
//                        self.isDollarSelected = true
//
//                        self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)+1))/100)".appendString(data: ((totalAmount)+1)), for: .normal)
//                    }else if(name.item_name?.lowercased() == "fried") {
//                        self.isDollarSelected = false
//                        self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)))/100)".appendString(data: ((totalAmount))), for: .normal)
//                    }
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section:0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                    }
                    self.menuTable.reloadRows(at:[IndexPath(row: currentIndex, section: 0)], with: .none)
                }else {
                    if(self.submenuData[currentIndex].item_exactly == 1 && self.submenuData.count == 2){
                        return
                    }
                    if(self.submenuData[currentIndex].item_exactly == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].is_checked = 0
                            meal[val].itemCount = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.menuTable.reloadRows(at:[IndexPath(row: currentIndex, section: 0)], with: .none)
                        let name = self.submenuData[currentIndex].meals[collectionIndex]
//                        let totalAmount = Double(self.cartData.cart_details.total_amount ?? "0")!
//                        if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected == false){
//                            self.isDollarSelected = true
//
//                            self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)+1))/100)".appendString(data: ((totalAmount)+1)), for: .normal)
//                        }else if(name.item_name?.lowercased() == "fried") {
//                            self.isDollarSelected = false
//                            self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)))/100)".appendString(data: ((totalAmount))), for: .normal)
//                        }
                        
                        return
                    }
                    //                    cell.labelForError.text = "Can't add more than \(self.submenuData[currentIndex].item_exactly ?? 0) item(s)"
                    //                    cell.viewForError.alpha = 1
                    //                    ////cell.labelForError.numberOfLines = 2
                    //                    cell.labelForError.sizeToFit()
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    //                        cell.viewForError.alpha = 0
                    //                    }
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_exactly ?? 0) item(s)"
                    self.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        if(self.submenuData[currentIndex].meals[0].isSelected == 1){
                            self.submenuData[currentIndex].meals[1].isSelected = 1
                            self.submenuData[currentIndex].meals[1].is_checked = 1
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                        }else {
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.submenuData[currentIndex].meals[0].isSelected = 1
                            self.submenuData[currentIndex].meals[0].is_checked = 1
                        }
                    }else {
                        if(self.submenuData[currentIndex].meals[1].isSelected == 1){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.submenuData[currentIndex].meals[0].isSelected = 1
                            self.submenuData[currentIndex].meals[0].is_checked = 1
                        }else {
                            self.submenuData[currentIndex].meals[1].isSelected = 1
                            self.submenuData[currentIndex].meals[1].is_checked = 1
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                        }
                    }
                    
                    self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                }else{
                    
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    let name = self.submenuData[currentIndex].meals[collectionIndex]
//                    let totalAmount = Double(self.cartData.cart_details.total_amount ?? "0")!
//                    if((name.item_name?.lowercased() == "smothered" || name.item_name?.lowercased() == "baked") && self.isDollarSelected == false){
//                        self.isDollarSelected = true
//
//                        self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)+1))/100)".appendString(data: ((totalAmount)+1)), for: .normal)
//                    }else if(name.item_name?.lowercased() == "fried") {
//                        self.isDollarSelected = false
//                        self.proceedButton.setTitle( "Add $" + "\(round(100*((totalAmount)))/100)".appendString(data: ((totalAmount))), for: .normal)
//                    }
                    
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
            }
            break
        case 2:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if((((self.submenuData[currentIndex].sectionItemCount ?? 0) >= (self.submenuData[currentIndex].item_range_from ?? 0)) && ((self.submenuData[currentIndex].sectionItemCount ?? 0) < (self.submenuData[currentIndex].item_range_to ?? 0)) || self.submenuData[currentIndex].sectionItemCount == 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                    }
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    
                }else {
                    if(self.submenuData[currentIndex].item_range_to == 1 && self.submenuData.count == 2){
                        return
                    }
                    
                    if(self.submenuData[currentIndex].item_range_to == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].is_checked = 0
                            meal[val].itemCount = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        return
                    }
                    //                    cell.labelForError.text = "Can't add more than \(sectionData.item_range_to ?? 0) item(s)"
                    //                    cell.viewForError.alpha = 1
                    //                    ////cell.labelForError.numberOfLines = 2
                    //                    cell.labelForError.sizeToFit()
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    //                        cell.viewForError.alpha = 0
                    //                    }
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_range_to ?? 0) item(s)"
                    self.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        self.submenuData[currentIndex].meals[1].isSelected = 1
                        self.submenuData[currentIndex].meals[1].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }else {
                        self.submenuData[currentIndex].meals[0].isSelected = 1
                        self.submenuData[currentIndex].meals[0].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                }else{
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = ( self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
            }
            break
        case 3:
            if(self.submenuData[currentIndex].meals[collectionIndex].is_checked ?? 0 == 0){
                if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_maximum ?? 0) || self.submenuData[currentIndex].meals.count == 2){
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    if(self.submenuData[currentIndex].meals.count == 2){
                        self.submenuData[currentIndex].sectionItemCount = 1
                    }else {
                        self.submenuData[currentIndex].sectionItemCount = (self.submenuData[currentIndex].sectionItemCount ?? 0) + 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                    self.manageCartButton()
                    if(self.submenuData[currentIndex].meals.count == 2){
                        if(collectionIndex == 0){
                            self.submenuData[currentIndex].meals[1].isSelected = 0
                            self.submenuData[currentIndex].meals[1].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }else {
                            self.submenuData[currentIndex].meals[0].isSelected = 0
                            self.submenuData[currentIndex].meals[0].is_checked = 0
                            self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        }
                        
                    }
                    self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                }else {
                    if(self.submenuData[currentIndex].item_maximum == 1 && self.submenuData.count == 2){
                        return
                    }
                    
                    if(self.submenuData[currentIndex].item_maximum == 1 && self.submenuData[currentIndex].meals.count > 2){
                        var meal = self.submenuData[currentIndex].meals
                        for val in 0..<self.submenuData[currentIndex].meals.count{
                            meal[val].isSelected = 0
                            meal[val].itemCount = 0
                            meal[val].is_checked = 0
                        }
                        self.submenuData[currentIndex].meals = meal
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                        return
                    }
                   
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_maximum ?? 0) item(s)"
                    self.present(myVC, animated: false, completion: nil)
                }
            }else {
                if(self.submenuData[currentIndex].meals.count == 2){
                    if(collectionIndex == 0){
                        self.submenuData[currentIndex].meals[1].isSelected = 1
                        self.submenuData[currentIndex].meals[1].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }else {
                        self.submenuData[currentIndex].meals[0].isSelected = 1
                        self.submenuData[currentIndex].meals[0].is_checked = 1
                        self.menuTable.reloadRows(at: [(NSIndexPath(row: currentIndex, section: 0) as IndexPath)], with: .none)
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                }else{
                    self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    if (self.submenuData[currentIndex].sectionItemCount! > 0){
                        self.submenuData[currentIndex].sectionItemCount = ( self.submenuData[currentIndex].sectionItemCount ?? 0) - 1
                    }
                    self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = 0
                }
                self.manageCartButton()
                self.menuTable.reloadRows(at: [(NSIndexPath(row:currentIndex, section: 0) as IndexPath)], with: .none)
            }
            
            break
        default:
            break
        }
    }
    
    func plusFunction(currentIndex: Int, collectionIndex: Int) {
        try! DBHelper.realm.write {
            switch (self.submenuData[currentIndex].is_rule ?? 0) {
            case 1:
                if((self.submenuData[currentIndex].sectionItemCount ?? 0) < (self.submenuData[currentIndex].item_exactly ?? 0)){
                    if(((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!) && !(((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) == 0) && ((self.submenuData[currentIndex].sectionItemCount ?? 0) == (self.submenuData[currentIndex].item_exactly ?? 0)))){
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                        
                        //if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 1){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        // }
                        self.manageCartButton()
                        self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    }else {
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "Can't add more item(s)"
                        self.present(myVC, animated: false, completion: nil)
                        
                    }
                    
                }else {
                   
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_exactly ?? 0) item(s)"
                    self.present(myVC, animated: false, completion: nil)
                    //
                }
                break
            case 2:
                if(((self.submenuData[currentIndex].sectionItemCount ?? 0) < self.submenuData[currentIndex].item_range_to!) || (self.submenuData[currentIndex].sectionItemCount == 0) || (((self.submenuData[currentIndex].sectionItemCount ?? 0) == self.submenuData[currentIndex].item_range_to) && ((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!) && ((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0))){
                    if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!){
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                        
                        //if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 1){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        // }
                        self.manageCartButton()
                        self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    }else {
                        
                        //                    cell.labelForError.text = "Can't add more item(s)"
                        //                    cell.viewForError.alpha = 1
                        //                    ////cell.labelForError.numberOfLines = 2
                        //                    cell.labelForError.sizeToFit()
                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        //                        cell.viewForError.alpha = 0
                        //                    }
                        
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "Can't add more item(s)"
                        self.present(myVC, animated: false, completion: nil)
                    }
                }else {
                    if((self.submenuData[currentIndex].single_item_maximum ?? 0 ) == self.submenuData[currentIndex].meals[collectionIndex].itemCount){
                        
                        //  cell.labelForError.text = "Can't add more than \(self.submenuData[currentIndex].single_item_maximum ?? 0) item(s)"
                        //cell.viewForError.alpha = 1
                        ////cell.labelForError.numberOfLines = 2
                        //cell.labelForError.sizeToFit()
                        self.manageCartButton()
                        
                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        //                        cell.viewForError.alpha = 0
                        //                    }
                        
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "Can't add more than \(self.submenuData[currentIndex].single_item_maximum ?? 0) item(s)"
                        self.present(myVC, animated: false, completion: nil)
                        
                    }else {
                        
                        //                    cell.labelForError.text = "Can't add more than \(sectionData.item_range_to ?? 0) item(s)"
                        //                    cell.viewForError.alpha = 1
                        //                    ////cell.labelForError.numberOfLines = 2
                        //                    cell.labelForError.sizeToFit()
                        //                    UIView.transition(with: cell.viewForError, duration: 4, options: .curveEaseInOut, animations: {
                        //                        cell.viewForError.alpha = 0
                        //                    })
                        
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_range_to ?? 0) item(s)"
                        self.present(myVC, animated: false, completion: nil)
                    }
                }
                break
            case 3:
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < (self.submenuData[currentIndex].item_maximum ?? 0)){
                    if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) < self.submenuData[currentIndex].single_item_maximum!){
                        self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) + 1
                        
                        //if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 1){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! + 1
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 1
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 1
                        // }
                        self.manageCartButton()
                        //  cell.lblNumberofItems.text = "\(self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0)"
                        self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    }else {
                        
                        //                    cell.labelForError.text = "Can't add more item(s)"
                        //                    cell.viewForError.alpha = 1
                        //                    ////cell.labelForError.numberOfLines = 2
                        //                    cell.labelForError.sizeToFit()
                        //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        //                        cell.viewForError.alpha = 0
                        //                    }
                        
                        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                        myVC.modalPresentationStyle = .overFullScreen
                        myVC.heading = "Can't add more item(s)"
                        self.present(myVC, animated: false, completion: nil)
                    }
                }else {
                    //                cell.labelForError.text = "Can't add more than \(sectionData.item_maximum!) item(s)"
                    //                ////cell.labelForError.numberOfLines = 2
                    //                cell.labelForError.sizeToFit()
                    //                cell.viewForError.alpha = 1
                    //                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                    //                    cell.viewForError.alpha = 0
                    //                }
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Can't add more than \(self.submenuData[currentIndex].item_maximum) item(s)"
                    self.present(myVC, animated: false, completion: nil)
                    
                }
                break
            default:
                break
            }
            //        if(cell.lblNumberofItems.text == "0"){
            //            cell.backVIewCount.backgroundColor = backgroundColor
            //        }else {
            //            cell.backVIewCount.backgroundColor = primaryColor
            //        }
        }
    }
    
    func minusFunction(currentIndex: Int, collectionIndex: Int) {
        try! DBHelper.realm.write {
            switch (self.submenuData[currentIndex].is_rule) {
            case 1:
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                    
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = ( (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1)
                    if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                    }
                    if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                    }
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    
                    //  cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
                }
                break
            case 2:
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1
                    
                    if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                    }
                    if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                        
                    }
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    
                    //  cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
                    
                }
                break
            case 3:
                if((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) > 0){
                    self.submenuData[currentIndex].meals[collectionIndex].itemCount = (self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0) - 1
                    if((self.submenuData[currentIndex].sectionItemCount ?? 0) > 0){
                        self.submenuData[currentIndex].sectionItemCount = self.submenuData[currentIndex].sectionItemCount! - 1
                    }
                    if(self.submenuData[currentIndex].meals[collectionIndex].itemCount == 0){
                        self.submenuData[currentIndex].meals[collectionIndex].is_checked = 0
                        self.submenuData[currentIndex].meals[collectionIndex].isSelected = 0
                        
                    }
                    self.manageCartButton()
                    self.menuTable.reloadRows(at: [IndexPath(row: currentIndex, section: 0)], with: .none)
                    
                    //   cell.lblNumberofItems.text = "\((self.submenuData[currentIndex].meals[collectionIndex].itemCount ?? 0))"
                }
                break
            default:
                break
            }
            //        if(cell.lblNumberofItems.text == "0"){
            //            cell.backVIewCount.backgroundColor = backgroundColor
            //        }else {
            //            cell.backVIewCount.backgroundColor = primaryColor
            //        }
        }
    }
    
    
    func manageCartButton(){
        var selectedItems = [AddToCart]()
        for val in self.submenuData{
            switch val.is_rule {
            case 1:
                if (val.sectionItemCount != val.item_exactly){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    //  self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id)", modifier_item_id: "\(data.id)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            case 2:
                if !(val.sectionItemCount! >= val.item_range_from! && val.sectionItemCount! <= val.item_range_to!){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    // self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id ?? 0)", modifier_item_id: "\(data.id ?? 0)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            case 3:
                if (val.sectionItemCount != (val.item_maximum ?? 0)){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "NotAvailableViewController") as! NotAvailableViewController
                    myVC.modalPresentationStyle = .overFullScreen
                    myVC.heading = "Must select as per the instructions."
                    //  self.navigationController?.present(myVC, animated: false, completion: nil)
                    return
                }else {
                    
                    for data in val.meals{
                        if(data.isSelected == 1){
                            var items = AddToCart(modifier_id: "\(val.modifier_group_id)", modifier_item_id: "\(data.id)", modifier_item_count: "\(data.itemCount ?? 0)")
                            selectedItems.append(items)
                        }
                    }
                }
                break
            default:
                break
            }
        }
    }
}


class MenuItemTable: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var itemCount: CountableLabel!
    @IBOutlet weak var springView: SpringView!
    
    @IBOutlet weak var itemImage: ImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var rapidFireButton: RapidFireButton!
    @IBOutlet weak var rapidFIreButtonTwo: RapidFireButton!
    
    
    
    var plusBtn:(()-> Void)? = nil
    var minusBtn:(()-> Void)? = nil
    var editBtn:(()-> Void)? = nil
    var removeBtn:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func minusAction(_ sender: Any) {
        if let minusBtn = minusBtn{
            minusBtn()
        }
    }
    
    @IBAction func plusAction(_ sender: Any) {
        if let plusBtn = plusBtn{
            plusBtn()
        }
    }
    
    @IBAction func editAction(_ sender: Any) {
        if let editBtn = editBtn{
            editBtn()
        }
    }
    
    @IBAction func removeAction(_ sender: Any) {
        if let removeBtn = removeBtn{
            removeBtn()
        }
    }
}


