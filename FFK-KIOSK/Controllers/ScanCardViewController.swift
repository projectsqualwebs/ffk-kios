 //
//  ScanCardViewController.swift
//  FFK-KIOSK
//
//  Created by qw on 13/01/21.
//

import UIKit
import SwiftGifOrigin
import AVFoundation
import SquareReaderSDK

class ScanCardViewController: UIViewController {
   
    //MARK: IBOutlets
    @IBOutlet weak var greenTick: UIImageView!
    @IBOutlet weak var orderPlacedMessage: DesignableUILabel!
    @IBOutlet weak var eranedPoints: DesignableUILabel!
    @IBOutlet weak var bottomDescription: DesignableUILabel!
    @IBOutlet weak var scanEarnPointLabel: DesignableUILabel!
    @IBOutlet weak var doneButton: DampingButton!
    @IBOutlet weak var wewillTextLabel: DesignableUILabel!
    
    
    var userName = String()
    var timer: Timer?
    var orderId : Int?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.view.frame.width < 1550){
            self.eranedPoints.font = UIFont(name: "GothamRounded-Bold", size: 30)
            self.orderPlacedMessage.font = UIFont(name: "GothamRounded-Medium", size: 30)
            self.bottomDescription.font = UIFont(name: "GothamRounded-Book", size: 16)
            self.wewillTextLabel.font = UIFont(name: "GothamRounded-Book", size: 22)
        }
      
        self.doneButton.layer.cornerRadius = 10
        let amount = DBHelper.shared.handleCartPrice()
        let total = Int(amount.rounded()*10)
        if(Singleton.shared.userDetail.id != nil){
        self.eranedPoints.text = "You've Earned \(total) points"
            self.bottomDescription.text = "Scan QR code and earn points. You can use these points for future orders."
                self.scanEarnPointLabel.text = "Scan and earn point."
        }else {
            self.eranedPoints.text = "You can earn \(total) points for this order."
                self.bottomDescription.text = "Please use same mobile number to create account as this order to gain points."
                    self.scanEarnPointLabel.text = "Scan and download our app."
            
        }
        
        self.orderPlacedMessage.text = "Hey \(userName),\norder placed successfully!"
        self.greenTick.image = UIImage.gif(asset: "thank_you_icon")
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1500)) {
            self.greenTick.image = #imageLiteral(resourceName: "thank-you-icon.gif")
        }
        NotificationCenter.default.post(name: NSNotification.Name(N_HANDLE_USER_INACTIVITY), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        timer = Timer.scheduledTimer(timeInterval: K_ACTIVITY_POPUP_AUTODISMISS, target: self, selector: #selector(self.hideControls), userInfo: nil, repeats: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    @objc func hideControls(){
        Singleton.shared.cartItemCount = 0
        self.clearNavigationStack()
        NavigationController.shared.emptyMyCart()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        DispatchQueue.main.async {
            EmailSubscribeViewController.startFreshDelegate?.startFreshOrder(playSound: false)
        }
    }


    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func swipeAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EmailSubscribeViewController") as! EmailSubscribeViewController
        myVC.orderId = self.orderId
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        Singleton.shared.cartItemCount = 0
        self.clearNavigationStack()
        NavigationController.shared.emptyMyCart()
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        self.navigationController?.pushViewController(myVC, animated: true)
        DispatchQueue.main.async {
            EmailSubscribeViewController.startFreshDelegate?.startFreshOrder(playSound: false)
        }
    }
}

